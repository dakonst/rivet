# BEGIN PLOT /LHCF_2015_I1351909/d01-x01-y01
Title=pp$\to$n at $\sqrt{s}=7\,$TeV with $\eta>10.76$
XLabel=Energy [GeV]
YLabel=d$\sigma/$d$E$ [mb$/$GeV]
LogY=0
YMax=0.0005
# END PLOT

# BEGIN PLOT /LHCF_2015_I1351909/d01-x01-y02
Title=pp$\to$n at $\sqrt{s}=7\,$TeV with $8.99<\eta<9.22$
XLabel=Energy [GeV]
YLabel=d$\sigma/$d$E$ [mb$/$GeV]
LogY=0
# END PLOT

# BEGIN PLOT /LHCF_2015_I1351909/d01-x01-y03
Title=pp$\to$n at $\sqrt{s}=7\,$TeV with $8.81<\eta<8.99$
XLabel=Energy [GeV]
YLabel=d$\sigma/$d$E$ [mb$/$GeV]
LogY=0
# END PLOT

# ... add more histograms as you need them ...
