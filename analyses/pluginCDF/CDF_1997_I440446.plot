BEGIN PLOT /CDF_1997_I440446/d01-x01-y01
Title=Fraction of prompt $J/\psi$ from $\chi_c$ decays
YLabel=Fraction
XLabel=$\sqrt{s}$ [GeV]
END PLOT
BEGIN PLOT /CDF_1997_I440446/d02-x01-y01
Title=Prompt $J/\psi$ cross section excluding $\chi_c$ and $\psi(2S)$ decays ($|\eta|<0.6$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp\times\text{Br}(J/\psi\to\mu^+\mu^-)$ [nb/GeV]
END PLOT
BEGIN PLOT /CDF_1997_I440446/d03-x01-y01
Title=Prompt $J/\psi$ cross section from $\chi_c$ decays ($|\eta|<0.6$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp\times\text{Br}(J/\psi\to\mu^+\mu^-)$ [nb/GeV]
END PLOT
BEGIN PLOT /CDF_1997_I440446/d04-x01-y01
Title=Prompt $J/\psi$ cross section from $\psi(2S)$ decays ($|\eta|<0.6$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp\times\text{Br}(J/\psi\to\mu^+\mu^-)$ [nb/GeV]
END PLOT
