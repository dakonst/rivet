BEGIN PLOT /ZEUS_2007_I753991/d01-x01-y01
Title= $\bar{E_\mathrm{T}}$ for $x_{\gamma}^\mathrm{obs}>0.75$
XLabel=$\bar{E_\mathrm{T}}$
YLabel=$d\sigma / d\bar{E_\mathrm{T}}$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d02-x01-y01
Title= $\bar{E_\mathrm{T}}$ for $x_{\gamma}^\mathrm{obs}\leq 0.75$
XLabel=$\bar{E_\mathrm{T}}$
YLabel=$d\sigma / d\bar{E_\mathrm{T}}$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d03-x01-y01
Title= $E_\mathrm{T}^{\mathrm{jet1}}$ for $x_{\gamma}^{\mathrm{obs}}>0.75$
XLabel=$E_\mathrm{T}^{\mathrm{jet1}}$
YLabel=$d\sigma / dE_\mathrm{T}^{jet1}$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d04-x01-y01
Title= $E_\mathrm{T}^{\mathrm{jet1}}$ for $x_{\gamma}^{\mathrm{obs}}\leq 0.75$
XLabel=$E_\mathrm{T}^{\mathrm{jet1}}$
YLabel=$d\sigma / dE_\mathrm{T}$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d05-x01-y01
Title= $\bar{\eta}$ for $x_{\gamma}^{\mathrm{obs}}>0.75$
XLabel=$\bar{\eta}$
YLabel=$d\sigma / d\bar{\eta}$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d06-x01-y01
Title= $\bar{\eta}$ for $x_{\gamma}^{\mathrm{obs}}\leq 0.75$
XLabel=$\bar{\eta}$
YLabel=$d\sigma / d\bar{\eta}$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d07-x01-y01
Title= $x_p^{\mathrm{obs}}$ for $x_{\gamma}^{\mathrm{obs}}>0.75$
XLabel=$x_p^{\mathrm{obs}}$
YLabel=$d\sigma / dx_p^{\mathrm{obs}}$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d08-x01-y01
Title= $x_p^{\mathrm{obs}}$ for $x_{\gamma}^{\mathrm{obs}}\leq 0.75$
XLabel=$x_p^{\mathrm{obs}}$
YLabel=$d\sigma / dx_p^\mathrm{obs}$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d09-x01-y01
Title= $\Delta\phi$ for $x_{\gamma}^{\mathrm{obs}}> 0.75$
XLabel=$\Delta\phi$
YLabel=$d\sigma / d\Delta\phi$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d10-x01-y01
Title= $\Delta\phi$ for $x_{\gamma}^{\mathrm{obs}}\leq 0.75$
XLabel=$\Delta\phi$
YLabel=$d\sigma / d\Delta\phi$
END PLOT


BEGIN PLOT /ZEUS_2007_I753991/d11-x01-y01
Title= $x_p^{\mathrm{obs}}$ for High-$x_{\gamma}^{\mathrm{obs}}$ 1
XLabel=$x_p^{\mathrm{obs}}$
YLabel=$d\sigma / dx_p^{\mathrm{obs}}$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d12-x01-y01
Title= $x_p^{\mathrm{obs}}$ for High-$x_{\gamma}^{\mathrm{obs}}$ 2
XLabel=$x_p^{\mathrm{obs}}$
YLabel=$d\sigma / dx_p^{\mathrm{obs}}$
END PLOT

BEGIN PLOT /ZEUS_2007_I753991/d13-x01-y01
Title= $x_p^{\mathrm{obs}}$ for High-$x_{\gamma}^{\mathrm{obs}}$ 3
XLabel=$x_p^{\mathrm{obs}}$
YLabel=$d\sigma / dx_p^{\mathrm{obs}}$
END PLOT


BEGIN PLOT /ZEUS_2007_I753991/d14-x01-y01
Title= $x_p^{\mathrm{obs}}$ for High-$x_{\gamma}^{\mathrm{obs}}$ 4
XLabel=$x_p^{\mathrm{obs}}$
YLabel=$d\sigma / dx_p^{\mathrm{obs}}$
END PLOT


BEGIN PLOT /ZEUS_2007_I753991/d15-x01-y01
Title= $x_p^{\mathrm{obs}}$ for Low-$x_{\gamma}^{\mathrm{obs}}$ 1
XLabel=$x_p^{\mathrm{obs}}$
YLabel=$d\sigma / dx_p^{\mathrm{obs}}$
END PLOT


BEGIN PLOT /ZEUS_2007_I753991/d16-x01-y01
Title= $x_p^{\mathrm{obs}}$ for Low-$x_{\gamma}^{\mathrm{obs}}$ 2
XLabel=$x_p^{\mathrm{obs}}$
YLabel=$d\sigma / dx_p^{\mathrm{obs}}$
END PLOT


BEGIN PLOT /ZEUS_2007_I753991/d17-x01-y01
Title= $x_p^{\mathrm{obs}}$ for Low-$x_{\gamma}^{\mathrm{obs}}$ 3
XLabel=$x_p^{\mathrm{obs}}$
YLabel=$d\sigma / dx_p^{\mathrm{obs}}$
END PLOT


BEGIN PLOT /ZEUS_2007_I753991/d18-x01-y01
Title= $x_p^{\mathrm{obs}}$ for Low-$x_{\gamma}^{\mathrm{obs}}$ 4
XLabel=$x_p^{\mathrm{obs}}$
YLabel=$d\sigma / dx_p^{\mathrm{obs}}$
END PLOT


BEGIN PLOT /ZEUS_2007_I753991/d19-x01-y01
Title= $x_{\gamma}^{\mathrm{obs}}$
XLabel=$x_{\gamma}^{\mathrm{obs}}$
YLabel=$d\sigma / dx_{\gamma}^{\mathrm{obs}}$
END PLOT

