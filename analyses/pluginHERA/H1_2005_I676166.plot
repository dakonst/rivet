BEGIN PLOT /H1_2005_I676166/d01-x01-y01
Title=Photoproduction : Muon Pseudorapidity
XLabel=$\eta _{\mu}$
YLabel=d $\sigma$ / d $\eta _{\mu}$ [pb]
LogY=0
END PLOT

BEGIN PLOT /H1_2005_I676166/d02-x01-y01
Title=Photoproduction : Muon Transverse Momentum
XLabel=$p_t ^{\mu}$ [GeV]
YLabel=d $\sigma$ / d $p_t ^{\mu}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2005_I676166/d03-x01-y01
Title=Photoproduction : Leading Jet Transverse Momentum
XLabel=$p_t ^{jet _1}$  [GeV]
YLabel=d $\sigma$ / d $p_t ^{jet_1}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2005_I676166/d04-x01-y01
Title=Photoproduction : Fraction of Photon Energy
XLabel=$x_{\gamma} ^{obs}$
YLabel=d $\sigma$ / d $x_{\gamma} ^{obs}$ [pb/GeV]
LogY=0
END PLOT

BEGIN PLOT /H1_2005_I676166/d05-x01-y01
Title=Electroproduction : Photon Virtuality
LogX=1
XLabel=$Q^2$ [GeV$^2$]
YLabel=d $\sigma$ / d $Q^2$ [pb/GeV$^2$]
END PLOT

BEGIN PLOT /H1_2005_I676166/d06-x01-y01
Title=Electroproduction : Bjorken Scaling Variable
XLabel=$x$ 
YLabel=d $\sigma$ / d $log x$ [pb]
LogY=0
END PLOT

BEGIN PLOT /H1_2005_I676166/d07-x01-y01
Title=Electroproduction : Muon Pseudorapidity
XLabel=$\eta _{\mu}$
YLabel=d $\sigma$ / d $\eta _{\mu}$ [pb]
LogY=0
END PLOT

BEGIN PLOT /H1_2005_I676166/d08-x01-y01
Title=Electroproduction : Muon Transverse Momentum
LogX=1
XLabel=$p_t ^{\mu}$  [GeV]
YLabel=d $\sigma$ / d $p_t ^{\mu}$ [pb/GeV]
END PLOT

BEGIN PLOT /H1_2005_I676166/d09-x01-y01
Title=Electroproduction: Leading Jet Transverse Momentum
XLabel=$p_{t,jet} ^{Breit}$
YLabel=d $\sigma$ / d $p_{t,jet} ^{Breit}$ [pb/GeV]
END PLOT

# ... add more histograms as you need them ...
