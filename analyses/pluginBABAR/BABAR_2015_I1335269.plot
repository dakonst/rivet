BEGIN PLOT /BABAR_2015_I1335269/d01-x01-y01
Title=$D^-D^0$ mass distribution in $B^0\to D^-D^0K^+$
XLabel=$m_{D^-D^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^-D^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2015_I1335269/d01-x01-y02
Title=$D^-K^+$ mass distribution in $B^0\to D^-D^0K^+$
XLabel=$m_{D^-K^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^-K^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2015_I1335269/d01-x01-y03
Title=$D^0K^+$ mass distribution in $B^0\to D^-D^0K^+$
XLabel=$m_{D^0K^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^0K^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2015_I1335269/d02-x01-y01
Title=$\bar{D}^0D^0$ mass distribution in $B^+\to \bar{D}^0D^0K^+$
XLabel=$m_{\bar{D}^0D^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\bar{D}^0D^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2015_I1335269/d02-x01-y02
Title=$\bar{D}^0K^+$ mass distribution in $B^+\to \bar{D}^0D^0K^+$
XLabel=$m_{\bar{D}^0K^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\bar{D}^0K^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2015_I1335269/d02-x01-y03
Title=$D^0K^+$ mass distribution in $B^+\to \bar{D}^0D^0K^+$
XLabel=$m_{D^0K^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^0K^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
