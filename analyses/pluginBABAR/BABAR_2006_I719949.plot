BEGIN PLOT /BABAR_2006_I719949/d01-x01-y01
Title=Cross section for $e^+e^-\to\rho^0\rho^0$ ($|\cos\theta^*|<0.8$)
XLabel=$\sqrt{s}$
YLabel=$\sigma$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I719949/d01-x01-y02
Title=Cross section for $e^+e^-\to\rho^0\phi$ ($|\cos\theta^*|<0.8$)
XLabel=$\sqrt{s}$
YLabel=$\sigma$
LogY=0
END PLOT

BEGIN PLOT /BABAR_2006_I719949/d02-x01-y01
Title=Scattering angle in $e^+e^-\to\rho^0\rho^0$
XLabel=$|\cos\theta^*|$
YLabel=$1/\sigma\text{d}\sigma/\text{d}|\cos\theta^*|$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I719949/d02-x01-y02
Title=Scattering angle in $e^+e^-\to\rho^0\phi$
XLabel=$|\cos\theta^*|$
YLabel=$1/\sigma\text{d}\sigma/\text{d}|\cos\theta^*|$
LogY=0
END PLOT

BEGIN PLOT /BABAR_2006_I719949/d03-x01-y01
Title=$\rho^0$ helicity angle in $e^+e^-\to\rho^0\rho^0$
XLabel=$\cos\theta_h$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_h$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I719949/d03-x01-y02
Title=$\phi$ helicity angle in $e^+e^-\to\rho^0\phi$
XLabel=$\cos\theta_h$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_h$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I719949/d03-x01-y03
Title=$\rho^0$ helicity angle in $e^+e^-\to\rho^0\phi$
XLabel=$\cos\theta_h$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_h$
LogY=0
END PLOT
