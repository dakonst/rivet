BEGIN PLOT /BABAR_2007_I755548/d01
XLabel=$m_{p\bar{p}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{p\bar{p}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d01-x01-y01
Title=$p\bar{p}$ mass for $B^0\to p\bar{p}K^0_S$
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d01-x01-y02
Title=$p\bar{p}$ mass for $B^+\to p\bar{p}K^+$
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d01-x01-y03
Title=$p\bar{p}$ mass for $B^0\to p\bar{p}K^{*0}$
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d01-x01-y04
Title=$p\bar{p}$ mass for $B^+\to p\bar{p}K^{*+}$
END PLOT

BEGIN PLOT /BABAR_2007_I755548/d02-x01
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d02-x02
XLabel=$m_{\bar{p}h}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\bar{p}h}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2007_I755548/d02-x01-y01
Title=$pK^0_S$ mass for $B^0\to p\bar{p}K^0_S$ ($m_{pK^0_S}>m_{\bar{p}K^0_S}$)
XLabel=$m_{pK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{pK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d02-x02-y01
Title=$pK^0_S$ mass for $B^0\to p\bar{p}K^0_S$  ($m_{pK^0_S}<m_{\bar{p}K^0_S}$)
XLabel=$m_{\bar{p}K^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\bar{p}K^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d02-x01-y02
Title=$pK^+$ mass for $B^+\to p\bar{p}K^+$ ($m_{pK^+}>m_{\bar{p}K^+}$)
XLabel=$m_{pK^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{pK^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d02-x02-y02
Title=$pK^+$ mass for $B^+\to p\bar{p}K^+$ ($m_{pK^+}<m_{\bar{p}K^+}$)
XLabel=$m_{\bar{p}K^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\bar{p}K^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d02-x01-y03
Title=$\bar{p}K^{*0}$ mass for $B^0\to p\bar{p}K^{*0}$ ($m_{pK^{*0}}>m_{\bar{p}K^{*0}}$)
XLabel=$m_{pK^{*0}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{pK^{*0}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d02-x02-y03
Title=$\bar{p}K^{*0}$ mass for $B^0\to p\bar{p}K^{*0}$ ($m_{pK^{*0}}<m_{\bar{p}K^{*0}}$)
XLabel=$m_{\bar{p}K^{*0}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\bar{p}K^{*0}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d02-x01-y04
Title=$\bar{p}K^{*+}$ mass for $B^+\to p\bar{p}K^{*+}$ ($m_{pK^{*+}}>m_{\bar{p}K^{*+}}$)
XLabel=$m_{pK^{*+}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{pK^{*+}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2007_I755548/d02-x02-y04
Title=$\bar{p}K^{*+}$ mass for $B^+\to p\bar{p}K^{*+}$ ($m_{pK^{*+}}<m_{\bar{p}K^{*+}}$)
XLabel=$m_{\bar{p}K^{*+}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\bar{p}K^{*+}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
