BEGIN PLOT /BABAR_2014_I1254862
LogY=0
XLabel=$z_2$
END PLOT

BEGIN PLOT /BABAR_2014_I1254862/d01-x01-y01
Title =$A_{UL}$ for $\pi\pi$ production ($0.15<z_1<0.2$) (thrust)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x01-y02
Title =$A_{UL}$ for $\pi\pi$ production ($0.15<z_1<0.2$)  (hadron)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x01-y01
Title =$A_{UC}$ for $\pi\pi$ production ($0.15<z_1<0.2$)  (thrust)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x01-y02
Title =$A_{UC}$ for $\pi\pi$ production ($0.15<z_1<0.2$)  (hadron)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x02-y01
Title =$A_{UL}$ for $\pi\pi$ production ($0.2<z_1<0.3$) (thrust)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x02-y02
Title =$A_{UL}$ for $\pi\pi$ production ($0.2<z_1<0.3$)  (hadron)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x02-y01
Title =$A_{UC}$ for $\pi\pi$ production ($0.2<z_1<0.3$)  (thrust)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x02-y02
Title =$A_{UC}$ for $\pi\pi$ production ($0.2<z_1<0.3$)  (hadron)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x03-y01
Title =$A_{UL}$ for $\pi\pi$ production ($0.3<z_1<0.4$) (thrust)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x03-y02
Title =$A_{UL}$ for $\pi\pi$ production ($0.3<z_1<0.4$)  (hadron)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x03-y01
Title =$A_{UC}$ for $\pi\pi$ production ($0.3<z_1<0.4$)  (thrust)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x03-y02
Title =$A_{UC}$ for $\pi\pi$ production ($0.3<z_1<0.4$)  (hadron)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x04-y01
Title =$A_{UL}$ for $\pi\pi$ production ($0.4<z_1<0.5$) (thrust)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x04-y02
Title =$A_{UL}$ for $\pi\pi$ production ($0.4<z_1<0.5$)  (hadron)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x04-y01
Title =$A_{UC}$ for $\pi\pi$ production ($0.4<z_1<0.5$)  (thrust)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x04-y02
Title =$A_{UC}$ for $\pi\pi$ production ($0.4<z_1<0.5$)  (hadron)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x05-y01
Title =$A_{UL}$ for $\pi\pi$ production ($0.5<z_1<0.7$) (thrust)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x05-y02
Title =$A_{UL}$ for $\pi\pi$ production ($0.5<z_1<0.7$)  (hadron)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x05-y01
Title =$A_{UC}$ for $\pi\pi$ production ($0.5<z_1<0.7$)  (thrust)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x05-y02
Title =$A_{UC}$ for $\pi\pi$ production ($0.5<z_1<0.7$)  (hadron)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x06-y01
Title =$A_{UL}$ for $\pi\pi$ production ($0.7<z_1<0.9$) (thrust)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d01-x06-y02
Title =$A_{UL}$ for $\pi\pi$ production ($0.7<z_1<0.9$)  (hadron)
YLabel = $A_{UL}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x06-y01
Title =$A_{UC}$ for $\pi\pi$ production ($0.7<z_1<0.9$)  (thrust)
YLabel = $A_{UC}$ [$\%$]
END PLOT
BEGIN PLOT /BABAR_2014_I1254862/d02-x06-y02
Title =$A_{UC}$ for $\pi\pi$ production ($0.7<z_1<0.9$)  (hadron)
YLabel = $A_{UC}$ [$\%$]
END PLOT
