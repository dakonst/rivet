BEGIN PLOT /BABAR_2008_I789278/d01-x01-y01
Title=Longintudinal polarization in $B^+\to \phi K^+_1$
YLabel=$f_L$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I789278/d01-x01-y02
Title=Longintudinal polarization in $B^+\to \phi K^+_2$
YLabel=$f_L$
LogY=0
END PLOT
