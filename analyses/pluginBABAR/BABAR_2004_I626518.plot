BEGIN PLOT /BABAR_2004_I626518/d01-x01-y01
Title=Longintudinal polarization in $B^-\to D^{*0}\bar{K}^{*-}$
YLabel=$f_L$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2004_I626518/d02-x01-y01
Title=$D^0$ helicity angle in $B^-\to D^{*0}\bar{K}^{*-}$ ($D^{*0}\to\pi^0D^0$)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2004_I626518/d02-x02-y01
Title=Kaon helicity angle in $B^-\to D^{*0}\bar{K}^{*-}$ ($D^{*0}\to\pi^0D^0$)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2004_I626518/d02-x01-y02
Title=$D^0$ helicity angle in $B^-\to D^{*0}\bar{K}^{*-}$ ($D^{*0}\to\gamma D^0$)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2004_I626518/d02-x02-y02
Title=Kaon helicity angle in $B^-\to D^{*0}\bar{K}^{*-}$ ($D^{*0}\to\gamma D^0$)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
