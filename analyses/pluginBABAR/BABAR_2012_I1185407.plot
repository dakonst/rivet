BEGIN PLOT /BABAR_2012_I1185407/d01-x01-y01
Title=$3\pi^0$ mass distribution in $\tau^-\to 2\pi^-\pi^+3\pi^0\nu_\tau$
XLabel=$m_{3\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{3\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1185407/d01-x01-y02
Title=$\pi^+\pi^-\pi^0$ mass distribution in $\tau^-\to 2\pi^-\pi^+3\pi^0\nu_\tau$
XLabel=$m_{\pi^+\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1185407/d01-x01-y03
Title=$\pi^+2\pi^-3\pi^0$ mass distribution in $\tau^-\to 2\pi^-\pi^+3\pi^0\nu_\tau$
XLabel=$m_{\pi^+2\pi^-3\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+2\pi^-3\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2012_I1185407/d02-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\tau^-\to 3\pi^-2\pi^+\nu_\tau$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1185407/d02-x01-y02
Title=$2\pi^+2\pi^-$ mass distribution in $\tau^-\to 3\pi^-2\pi^+\nu_\tau$
XLabel=$m_{2\pi^+2\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{2\pi^+2\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1185407/d02-x01-y03
Title=$3\pi^+2\pi^-$ mass distribution in $\tau^-\to 3\pi^-2\pi^+\nu_\tau$
XLabel=$m_{3\pi^+2\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{3\pi^+2\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2012_I1185407/d03-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\tau^-\to 3\pi^-2\pi^+\pi^0\nu_\tau$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1185407/d03-x01-y02
Title=$\pi^+\pi^-\pi^0$ mass distribution in $\tau^-\to 3\pi^-2\pi^+\pi^0\nu_\tau$
XLabel=$m_{\pi^+\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1185407/d03-x01-y03
Title=$2\pi^+2\pi^-\pi^0$ mass distribution in $\tau^-\to 3\pi^-2\pi^+\pi^0\nu_\tau$
XLabel=$m_{2\pi^+2\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{2\pi^+2\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1185407/d03-x01-y04
Title=$2\pi^+3\pi^-\pi^0$ mass distribution in $\tau^-\to 3\pi^-2\pi^+\pi^0\nu_\tau$
XLabel=$m_{2\pi^+3\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{2\pi^+3\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
