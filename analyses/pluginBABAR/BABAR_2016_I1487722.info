Name: BABAR_2016_I1487722
Year: 2016
Summary: $\pi^+\pi^+\pi^-$ mass distribution in $B^0\to D^{*-} \pi^+\pi^+\pi^-$
Experiment: BABAR
Collider: PEP-II
InspireID: 1487722
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 94 (2016) 9, 091101
Description:
  'Measurement of mass distributions of the pions in the decay $B^0\to D^{*-} \pi^+\pi^+\pi^-$.
   The data were read from, the plots in the papers and efficiency from the paper applied.'
ValidationInfo:
  'Herwig 7 events using EvtGen for B decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BaBar:2016zti
BibTeX: '@article{BaBar:2016zti,
    author = "Lees, J. P. and others",
    collaboration = "BaBar",
    title = "{Measurement of the $B^0 -> D^*- \pi^+ \pi^- \pi^+$ branching fraction}",
    eprint = "1609.06802",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "SLAC-PUB-16822, BABAR-PUB-16-005, BABAR-PUB-16-015",
    doi = "10.1103/PhysRevD.94.091101",
    journal = "Phys. Rev. D",
    volume = "94",
    number = "9",
    pages = "091101",
    year = "2016"
}
'
