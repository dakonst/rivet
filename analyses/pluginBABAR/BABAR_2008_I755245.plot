BEGIN PLOT /BABAR_2008_I755245/d01-x01-y01
Title=$D_s^+K^-$ mass is $B^-\to D_s^+K^-\pi^-$
XLabel=$m_{D_s^+K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D_s^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I755245/d01-x01-y02
Title=$D_s^{*+}K^-$ mass is $B^-\to D_s^{*+}K^-\pi^-$
XLabel=$m_{D_s^{*+}K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D_s^{*+}K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
