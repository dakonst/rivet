BEGIN PLOT /BABAR_2004_I626730/d01-x01-y01
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$
XLabel=$m_{K^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2004_I626730/d01-x01-y02
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2004_I626730/d02-x01-y01
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$ (Region I)
XLabel=$m_{K^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2004_I626730/d02-x01-y02
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$ (Region V)
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2004_I626730/d02-x01-y03
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$ (Region II)
XLabel=$m_{K^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2004_I626730/d02-x02-y01
Title=$K^+\pi^-$ helictiy angle in $B^+\to K^+\pi^-\pi^+$ (Region I)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2004_I626730/d02-x02-y02
Title=$\pi^+\pi^-$ helicity angle in $B^+\to K^+\pi^-\pi^+$ (Region V)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2004_I626730/d02-x02-y03
Title=$K^+\pi^-$ helicity angle in $B^+\to K^+\pi^-\pi^+$ (Region II)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
