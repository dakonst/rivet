Name: BABAR_2011_I897848
Year: 2011
Summary: Mass distributions in $B^0\to K^+\pi^-\pi^0$ decays
Experiment: BABAR
Collider: PEP-II
InspireID: 897848
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 83 (2011) 112010
RunInfo: Any process producing B0 mesons, originally e+e- at Upsilon(4S)
Description:
  'Measurements of mass distributions in $B^0\to K^+\pi^-\pi^0$ decays. The data were read from the plots in the paper and the backgrounds given subtracted.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BaBar:2011vfx
BibTeX: '@article{BaBar:2011vfx,
    author = "Lees, J. P. and others",
    collaboration = "BaBar",
    title = "{Amplitude Analysis of $B^0\to K^+ \pi^- \pi^0$ and Evidence of Direct CP Violation in $B\to K^* \pi$ decays}",
    eprint = "1105.0125",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "SLAC-PUB-14452, BABAR-PUB-09-040",
    doi = "10.1103/PhysRevD.83.112010",
    journal = "Phys. Rev. D",
    volume = "83",
    pages = "112010",
    year = "2011"
}
'
