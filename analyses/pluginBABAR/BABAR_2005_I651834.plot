BEGIN PLOT /BABAR_2005_I651834/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $ \chi_{c1}(3872)\to J/\psi\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2005_I651834/d02-x01-y01
Title=$\pi^+\pi^-$ mass in $\psi(2S)\to J/\psi\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
