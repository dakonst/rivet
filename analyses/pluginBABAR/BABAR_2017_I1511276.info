Name: BABAR_2017_I1511276
Year: 2017
Summary: Cross section for $e^+e^-\to K_S^0K_L^0$ with $\pi^0$, $\eta$ and $\pi^0\pi^0$ between threshold and 4 GeV
Experiment: BABAR
Collider: PEP-II
InspireID: 1511276
Status: VALIDATED NOHEPDATA
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
- Phys.Rev. D95 (2017) no.5, 052001
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams:  [e+,e-]
Energies: [1.25, 1.35, 1.3500, 1.425, 1.45, 1.4500, 1.475, 1.525, 1.55, 1.5500, 1.575, 1.625,
           1.65, 1.6500, 1.675, 1.725, 1.75, 1.7500, 1.775, 1.825, 1.85, 1.8500, 1.875, 1.925,
           1.95, 1.9500, 1.975, 2.025, 2.05, 2.0500, 2.075, 2.125, 2.15, 2.1500, 2.175, 2.225,
           2.25, 2.2500, 2.275, 2.325, 2.35, 2.3500, 2.375, 2.425, 2.45, 2.4500, 2.475, 2.525,
           2.55, 2.5500, 2.575, 2.625, 2.65, 2.6500, 2.675, 2.725, 2.75, 2.7500, 2.775, 2.825,
           2.85, 2.8500, 2.875, 2.925, 2.9500, 2.975, 3.025, 3.0500, 3.075, 3.125, 3.1500, 3.175,
           3.225, 3.2500, 3.275, 3.325, 3.3500, 3.375, 3.425, 3.4500, 3.475, 3.525, 3.5500, 3.575,
           3.625, 3.6500, 3.675, 3.725, 3.7500, 3.775, 3.825, 3.8500, 3.875, 3.925, 3.9500, 3.975]
Options:
 - ENERGY=*
Description:
   'Cross section for $e^+e^-\to K_S^0K_L^0$ with $\pi^0$, $\eta$ and $\pi^0\pi^0$ between threshold and 4 GeV using radiative events.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords:
BibKey: TheBABAR:2017vgl
BibTeX: '@article{TheBABAR:2017vgl,
      author         = "Lees, J. P. and others",
      title          = "{Cross sections for the reactions $e^+ e^-\to K^0_S
                        K^0_L\pi^0$, $K^0_S K^0_L\eta$, and $K^0_S
                        K^0_L\pi^0\pi^0$ from events with initial-state
                        radiation}",
      collaboration  = "BaBar",
      journal        = "Phys. Rev.",
      volume         = "D95",
      year           = "2017",
      number         = "5",
      pages          = "052001",
      doi            = "10.1103/PhysRevD.95.052001",
      eprint         = "1701.08297",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "BABAR-PUB-16-004, SLAC-PUB-16918",
      SLACcitation   = "%%CITATION = ARXIV:1701.08297;%%"
}'
