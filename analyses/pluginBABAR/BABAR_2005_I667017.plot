BEGIN PLOT /BABAR_2005_I667017/d01-x01-y01
Title=Pion helicity angle for $B^0\to D^- K^{*+}$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
