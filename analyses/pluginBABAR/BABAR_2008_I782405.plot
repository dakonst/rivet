BEGIN PLOT /BABAR_2008_I782405/d01-x01-y01
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$
XLabel=$m_{K^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I782405/d01-x01-y02
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I782405/d01-x01-y03
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2008_I782405/d02-x01-y01
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I782405/d02-x01-y02
Title=$\pi^+\pi^-$ mass in $B^-\to K^-\pi^-\pi^+$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I782405/d02-x02-y01
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$ ($\cos\theta_H>0$)
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I782405/d02-x02-y02
Title=$\pi^+\pi^-$ mass in $B^-\to K^-\pi^-\pi^+$ ($\cos\theta_H>0$)
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I782405/d02-x03-y01
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^-\pi^+$ ($\cos\theta_H<0$)
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2008_I782405/d02-x03-y02
Title=$\pi^+\pi^-$ mass in $B^-\to K^-\pi^-\pi^+$ ($\cos\theta_H<0$)
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT