BEGIN PLOT /BABAR_2009_I801589/d01-x01-y01
Title=$K^0_S\pi^-$ mass in $B^-\to J/\psi K^0_S\pi^-$
XLabel=$m_{K^0_S\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d01-x01-y02
Title=$K^0_S\pi^-$ mass in $B^-\to \psi(2S) K^0_S\pi^-$
XLabel=$m_{K^0_S\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d01-x02-y01
Title=$K^+\pi^-$ mass in $B^0\to J/\psi K^+\pi^-$
XLabel=$m_{K^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d01-x02-y02
Title=$K^+\pi^-$ mass in $B^0\to \psi(2S) K^+\pi^-$
XLabel=$m_{K^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2009_I801589/d02-x01-y01
Title=$K\pi$ mass in $B\to J/\psi K\pi$
XLabel=$m_{K\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K\pi}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d02-x01-y02
Title=$K\pi$ mass in $B\to \psi(2S) K\pi$
XLabel=$m_{K\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K\pi}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BABAR_2009_I801589/d03-x01-y01
Title=$J/\psi\pi$ mass in $B\to J/\psi K\pi$ ($m_{K\pi}<0.795\,$GeV)
XLabel=$m_{J/\psi\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d03-x01-y02
Title=$J/\psi\pi$ mass in $B\to J/\psi K\pi$ ($0.795<m_{K\pi}<0.995\,$GeV)
XLabel=$m_{J/\psi\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d03-x01-y03
Title=$J/\psi\pi$ mass in $B\to J/\psi K\pi$ ($0.995<m_{K\pi}<1.332\,$GeV)
XLabel=$m_{J/\psi\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d03-x01-y04
Title=$J/\psi\pi$ mass in $B\to J/\psi K\pi$ ($1.332<m_{K\pi}<1.532\,$GeV)
XLabel=$m_{J/\psi\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d03-x01-y05
Title=$J/\psi\pi$ mass in $B\to J/\psi K\pi$ ($m_{K\pi}>1.532\,$GeV)
XLabel=$m_{J/\psi\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2009_I801589/d03-x02-y01
Title=$\psi(2S)\pi$ mass in $B\to \psi(2S) K\pi$ ($m_{K\pi}<0.795\,$GeV)
XLabel=$m_{\psi(2S)\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\psi(2S)\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d03-x02-y02
Title=$\psi(2S)\pi$ mass in $B\to \psi(2S) K\pi$ ($0.795<m_{K\pi}<0.995\,$GeV)
XLabel=$m_{\psi(2S)\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\psi(2S)\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d03-x02-y03
Title=$\psi(2S)\pi$ mass in $B\to \psi(2S) K\pi$ ($0.995<m_{K\pi}<1.332\,$GeV)
XLabel=$m_{\psi(2S)\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\psi(2S)\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d03-x02-y04
Title=$\psi(2S)\pi$ mass in $B\to \psi(2S) K\pi$ ($1.332<m_{K\pi}<1.532\,$GeV)
XLabel=$m_{\psi(2S)\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\psi(2S)\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d03-x02-y05
Title=$\psi(2S)\pi$ mass in $B\to \psi(2S) K\pi$ ($m_{K\pi}>1.532\,$GeV)
XLabel=$m_{\psi(2S)\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\psi(2S)\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2009_I801589/d04-x01-y01
Title=Helicity angle in  $B\to J/\psi K\pi$  ($K^*$ veto)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d04-x01-y02
Title=Helicity angle in  $B\to \psi(2S) K\pi$  ($K^*$ veto)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT

BEGIN PLOT /BABAR_2009_I801589/d05-x01-y01
Title=$J/\psi\pi$ mass in $B\to J/\psi K\pi$ (all events)
XLabel=$m_{J/\psi\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d05-x02-y01
Title=$\psi(2S)\pi$ mass in $B\to \psi(2S) K\pi$ (all events)
XLabel=$m_{\psi(2S)\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\psi(2S)\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d05-x01-y02
Title=$J/\psi\pi$ mass in $B\to J/\psi K\pi$ ($K^*$ region)
XLabel=$m_{J/\psi\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d05-x02-y02
Title=$\psi(2S)\pi$ mass in $B\to \psi(2S) K\pi$ ($K^*$ region)
XLabel=$m_{\psi(2S)\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\psi(2S)\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d05-x01-y03
Title=$J/\psi\pi$ mass in $B\to J/\psi K\pi$ ($K^*$ veto)
XLabel=$m_{J/\psi\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2009_I801589/d05-x02-y03
Title=$\psi(2S)\pi$ mass in $B\to \psi(2S) K\pi$ ($K^*$ veto)
XLabel=$m_{\psi(2S)\pi}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\psi(2S)\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
