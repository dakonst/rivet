BEGIN PLOT /BABAR_2012_I1079912/d01-x01-y01
Title=Branching ratio for $\bar{B}\to X_u\ell\bar\nu_\ell$ as function of $p^*_{\ell,\min}$
XLabel=$p^*_{\ell,\min}$ [GeV]
YLabel=$\Delta\mathcal{B}(\bar{B}\to X_u\ell\bar{\nu}_\ell)\times10^{-3}$
LogY=0
END PLOT
