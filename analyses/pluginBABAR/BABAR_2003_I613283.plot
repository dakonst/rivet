BEGIN PLOT /BABAR_2003_I613283/d01-x01-y01
Title=Longintudinal polarization in $B^0\to D^{*+}_sD^{*-}$
YLabel=$f_L$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2003_I613283/d02-x01-y01
Title=$D^*$ helicity angle in $B^0\to D^{*+}_sD^{*-}$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2003_I613283/d02-x02-y01
Title=$D^*_s$ helicity angle in $B^0\to D^{*+}_sD^{*-}$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
