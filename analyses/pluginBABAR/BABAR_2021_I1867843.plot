BEGIN PLOT /BABAR_2021_I1867843/d01-x01-y01
Title=$K^+K^-$ mass distribution in $\eta_c\to K^+K^-\eta^\prime$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2021_I1867843/d01-x01-y02
Title=$K^\pm\eta^\prime$ mass distribution in $\eta_c\to K^+K^-\eta^\prime$
XLabel=$m_{K^\pm\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^\pm\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2021_I1867843/dalitz1
Title=Dalitz plot for  $\eta_c\to K^+K^-\eta^\prime$
XLabel=$m^2_{K^+\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\eta^\prime}/{\rm d}m^2_{K^-\eta^\prime}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2021_I1867843/d02-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\eta_c\to \pi^+\pi^-\eta^\prime$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2021_I1867843/d02-x01-y02
Title=$\pi^\pm\eta^\prime$ mass distribution in $\eta_c\to \pi^+\pi^-\eta^\prime$
XLabel=$m_{\pi^\pm\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^\pm\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2021_I1867843/dalitz2
Title=Dalitz plot for  $\eta_c\to \pi^+\pi^-\eta^\prime$
XLabel=$m^2_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^-\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\eta^\prime}/{\rm d}m^2_{\pi^-\eta^\prime}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2021_I1867843/d03-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\eta_c\to \pi^+\pi^-\eta$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2021_I1867843/d03-x01-y02
Title=$\pi^\pm\eta$ mass distribution in $\eta_c\to \pi^+\pi^-\eta$
XLabel=$m_{\pi^\pm\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^\pm\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2021_I1867843/dalitz3
Title=Dalitz plot for  $\eta_c\to \pi^+\pi^-\eta$
XLabel=$m^2_{\pi^+\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^-\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\eta}/{\rm d}m^2_{\pi^-\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
