BEGIN PLOT /BESIII_2024_I2691955/d01-x01-y01
Title=$K^-\bar\Xi^+$ mass distribution in $\psi(2S)\to K^-\Lambda^0\bar\Xi^+$
XLabel=$m_{K^-\bar\Xi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\bar\Xi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2024_I2691955/d01-x01-y02
Title=$\Lambda^0\bar\Xi^+$ mass distribution in $\psi(2S)\to K^-\Lambda^0\bar\Xi^+$
XLabel=$m_{\Lambda^0\bar\Xi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda^0\bar\Xi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2024_I2691955/d01-x01-y03
Title=$K^-\Lambda^0$ mass distribution in $\psi(2S)\to K^-\Lambda^0\bar\Xi^+$
XLabel=$m_{K^-\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\Lambda^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
