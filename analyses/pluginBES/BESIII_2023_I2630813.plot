BEGIN PLOT /BESIII_2023_I2630813/d01-x01-y01
Title=$\pi^+\eta$ mass distribution in $D^+_s\to \pi^+\eta\omega$
XLabel=$m_{\pi^+\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2630813/d01-x01-y02
Title=$\pi^+\omega$ mass distribution in $D^+_s\to \pi^+\eta\omega$
XLabel=$m_{\pi^+\omega}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\omega}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2630813/d01-x01-y03
Title=$\eta\omega$ mass distribution in $D^+_s\to \pi^+\eta\omega$
XLabel=$m_{\eta\omega}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta\omega}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
