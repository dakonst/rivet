BEGIN PLOT /BESII_2006_I735496/d01-x01-y01
Title=$R=\sigma(e^+e^-\to uds+ \psi(3770))/\sigma(e^+e^-\to \mu^+\mu^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$R$
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESII_2006_I735496/d01-x01-y02
Title=$R=\sigma(e^+e^-\to \mathrm{hadrons})/\sigma(e^+e^-\to \mu^+\mu^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$R$
LogY=0
ConnectGaps=1
END PLOT
