BEGIN PLOT /BESIII_2024_I2792993/d01-x01-y03
Title=$\Lambda\bar\Lambda$ mass distribution in $\chi_{c1}\to \phi\Lambda\bar\Lambda$
XLabel=$m_{\Lambda\bar\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\bar\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2024_I2792993/d01-x01-y01
Title=$\phi\Lambda$ mass distribution in $\chi_{c1}\to \phi\Lambda\bar\Lambda$
XLabel=$m_{\phi\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2024_I2792993/d01-x01-y02
Title=$\phi\bar\Lambda$ mass distribution in $\chi_{c1}\to \phi\Lambda\bar\Lambda$
XLabel=$m_{\phi\bar\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\bar\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2024_I2792993/d02-x01-y03
Title=$\Lambda\bar\Lambda$ mass distribution in $\chi_{c2}\to \phi\Lambda\bar\Lambda$
XLabel=$m_{\Lambda\bar\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\bar\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2024_I2792993/d02-x01-y01
Title=$\phi\Lambda$ mass distribution in $\chi_{c2}\to \phi\Lambda\bar\Lambda$
XLabel=$m_{\phi\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2024_I2792993/d02-x01-y02
Title=$\phi\bar\Lambda$ mass distribution in $\chi_{c2}\to \phi\Lambda\bar\Lambda$
XLabel=$m_{\phi\bar\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\phi\bar\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT