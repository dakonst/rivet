Name: BESIII_2022_I2102455
Year: 2022
Summary: Mass distributions in $D^+$ and $D^0$ decays to pions
Experiment: BESIII
Collider: 
InspireID: 2102455
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2206.13864 [hep-ex]
Description:
  'Measurement of the mass distributions  in $D^+$ and $D^0$ decays to pions by the BESIII collaboration. The data were read from the plots in the paper and therefore for some points the error bars are the size of the point. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022qrs
BibTeX: '@article{BESIII:2022qrs,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Absolute Measurements of Branching Fractions of Cabibbo-Suppressed Hadronic $D^{0(+)}$ Decays Involving Multiple Pions}",
    eprint = "2206.13864",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "BAM-00516",
    month = "6",
    year = "2022"
}
'
