BEGIN PLOT /BESIII_2019_I1623214/d01-x01-y01
Title=Cross section for $e^+e^-\to \eta Y(2175)(\to\phi f_0(\to\pi^+\pi^-))$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \eta Y(2175)\times\mathcal{B}( Y(2175) \to\phi f_0(\to\pi^+\pi^-))$ [pb]
LogY=0
ConnectGaps=1
END PLOT
