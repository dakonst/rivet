BEGIN PLOT /BESIII_2017_I1607253/d01-x01-y01
Title=Cross section for $e^+e^-\to \phi\phi\omega$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \phi\phi\omega)$ [fb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2017_I1607253/d02-x01-y01
Title=Cross section for $e^+e^-\to \phi\phi\phi$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \phi\phi\phi)$ [fb]
LogY=0
ConnectGaps=1
END PLOT
