BEGIN PLOT /BESIII_2021_I1866051/d01-x01-y01
Title=$\sigma(e^+e^-\to K_S^0K_L^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to K_S^0K_L^0)$/pb
ConnectGaps=1
LogY=0
END PLOT
