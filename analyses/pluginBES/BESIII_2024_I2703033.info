Name: BESIII_2024_I2703033
Year: 2024
Summary: Analysis of $J/\psi$ decays to $\Xi^-\bar{\Xi}^+$
Experiment: BESIII
Collider: BEPC
InspireID: 2703033
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 132 (2024) 10, 101801
 - arXiv:2309.14667 [hep-ex]
RunInfo: e+e- > J/psi 
NeedCrossSection: no
Beams: [e-, e+]
Energies: [3.1]
Description:
  'Analysis of the angular distribution of the baryons, and decay products, produced in
  $e^+e^-\to J/\psi \to \Xi^-\bar{\Xi}^+$. The decay $\Xi^-\to\Lambda^0\pi^-$ and its charged conjugate
  are used together with $\Lambda^0\to p\pi^-$ and $\bar\Lambda^0\to\bar{n}\pi^0$, or
  the charge conjugate.
  Gives information about the decay and is useful for testing correlations in hadron decays.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2023jhj
BibTeX: '@article{BESIII:2023jhj,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Investigation of the \ensuremath{\Delta}I=1/2 Rule and Test of CP Symmetry through the Measurement of Decay Asymmetry Parameters in \ensuremath{\Xi}- Decays}",
    eprint = "2309.14667",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevLett.132.101801",
    journal = "Phys. Rev. Lett.",
    volume = "132",
    number = "10",
    pages = "101801",
    year = "2024"
}'
