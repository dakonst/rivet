BEGIN PLOT /BESIII_2024_I2753516
XLabel=$p_\eta$ [GeV]
YLabel=$1/\sigma_\mathrm{hadrons} \mathrm{d}\sigma/\mathrm{d}p_\eta$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2024_I2753516/d01-x01-y01
Title=$\eta$ momentum spectra at $\sqrt{s}=2$ GeV
END PLOT
BEGIN PLOT /BESIII_2024_I2753516/d01-x01-y02
Title=$\eta$ momentum spectra at $\sqrt{s}=2.2$ GeV
END PLOT
BEGIN PLOT /BESIII_2024_I2753516/d01-x01-y03
Title=$\eta$ momentum spectra at $\sqrt{s}=2.396$ GeV
END PLOT
BEGIN PLOT /BESIII_2024_I2753516/d01-x01-y04
Title=$\eta$ momentum spectra at $\sqrt{s}=2.6444$ GeV
END PLOT
BEGIN PLOT /BESIII_2024_I2753516/d01-x01-y05
Title=$\eta$ momentum spectra at $\sqrt{s}=2.9$ GeV
END PLOT
BEGIN PLOT /BESIII_2024_I2753516/d01-x01-y06
Title=$\eta$ momentum spectra at $\sqrt{s}=3.05$ GeV
END PLOT
BEGIN PLOT /BESIII_2024_I2753516/d01-x01-y07
Title=$\eta$ momentum spectra at $\sqrt{s}=3.5$ GeV
END PLOT
BEGIN PLOT /BESIII_2024_I2753516/d01-x01-y08
Title=$\eta$ momentum spectra at $\sqrt{s}=3.671$ GeV
END PLOT
