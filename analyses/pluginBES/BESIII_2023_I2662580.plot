BEGIN PLOT /BESIII_2023_I2662580/d01-x01-y01
Title=$K_S^0\pi^0$ mass distribution in $D^+\to K^0_S\pi^+\pi^0\pi^0$
XLabel=$m_{K_S^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K_S^0\pi^0}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2662580/d01-x01-y02
Title=$\pi^+\pi^0$ mass distribution in $D^+\to K^0_S\pi^+\pi^0\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2662580/d01-x01-y03
Title=$K_S^0\pi^+$ mass distribution in $D^+\to K^0_S\pi^+\pi^0\pi^0$
XLabel=$m_{K_S^0\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K_S^0\pi^+}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2662580/d01-x01-y04
Title=$\pi^0\pi^0$ mass distribution in $D^+\to K^0_S\pi^+\pi^0\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2662580/d01-x01-y05
Title=$K_S^0\pi^+\pi^0$ mass distribution in $D^+\to K^0_S\pi^+\pi^0\pi^0$
XLabel=$m_{K_S^0\pi^+\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K_S^0\pi^+\pi^0}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2662580/d01-x01-y06
Title=$K_S^0\pi^0\pi^0$ mass distribution in $D^+\to K^0_S\pi^+\pi^0\pi^0$
XLabel=$m_{K_S^0\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K_S^0\pi^0\pi^0}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2662580/d01-x01-y07
Title=$\pi^+\pi^0\pi^0$ mass distribution in $D^+\to K^0_S\pi^+\pi^0\pi^0$
XLabel=$m_{\pi^+\pi^0\pi^0}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^0\pi^0}$ [$\text{GeV}$]
LogY=0
END PLOT
