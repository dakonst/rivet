BEGIN PLOT /BES_1999_I505287/d01-x01-y01
Title=$K^{*0}\bar{K}^{*0}$ mass in $J/\psi\to\gamma K^{*0}\bar{K}^{*0}$
XLabel=$m_{K^{*0}\bar{K}^{*0}}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^{*0}\bar{K}^{*0}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BES_1999_I505287/d02-x01-y01
Title=Helicity angle in $J/\psi\to\gamma K^{*0}\bar{K}^{*0}$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BES_1999_I505287/d02-x01-y02
Title=Angle between decay planes in $J/\psi\to\gamma K^{*0}\bar{K}^{*0}$
XLabel=$\chi$ [degrees]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$ $\text{degrees}^{-1}$
LogY=0
END PLOT
