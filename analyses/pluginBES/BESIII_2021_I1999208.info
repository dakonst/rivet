Name: BESIII_2021_I1999208
Year: 2021
Summary: Cross section for $e^+e^-\to\omega\pi^0\pi^0$ between 2 and 3.08 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1999208
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2112.15076
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams:  [e+,e-]
Energies: [2.0000, 2.0500, 2.1000, 2.1250, 2.1500, 2.1750, 2.2000, 2.2324,
           2.3094, 2.3864, 2.3960, 2.6444, 2.6464, 2.9000, 2.9500, 2.9810,
           3.0000, 3.0200, 3.0800]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to\omega\pi^0\pi^0$ between 2 and 3.08 GeV by the BESIII collaboration.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021uni
BibTeX: '@article{BESIII:2021uni,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Measurement of $e^{+}e^{-}\rightarrow\omega\pi^{0}\pi^{0}$ cross section at center-of-mass energies from 2.00 to 3.08 GeV}",
    eprint = "2112.15076",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "12",
    year = "2021"
}
'
