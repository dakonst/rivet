Name: BESIII_2019_I1736235
Year: 2019
Summary:  Cross section for $e^+e^-\to$ proton and antiproton between 2. and 3.08 GeV
Experiment: BESIII
Collider: BEPC II
InspireID: 1736235
Status:   VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:1905.09001
RunInfo: e+ e- to hadrons
NeedCrossSection: yes
Beams: [e+, e-]
Energies:  [2.0000, 2.0500, 2.1000, 2.1250, 2.1500, 2.1750, 2.2000,
            2.2324, 2.3094, 2.3864, 2.3960, 2.5000, 2.6444, 2.6464,
            2.7000, 2.8000, 2.9000, 2.9500, 2.9810, 3.0000, 3.0200, 3.0800]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to p\bar{p}$ for energies between 2. and 3.08 GeV using.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords: []
BibKey: Ablikim:2019eau
BibTeX: '@article{Ablikim:2019eau,
      author         = "Ablikim, M. and others",
      title          = "{Measurement of proton electromagnetic form factors in
                        $e^+e^- \to p\bar{p}$ in the energy region 2.00 - 3.08
                        GeV}",
      collaboration  = "BESIII",
      year           = "2019",
      eprint         = "1905.09001",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      SLACcitation   = "%%CITATION = ARXIV:1905.09001;%%"
}'
