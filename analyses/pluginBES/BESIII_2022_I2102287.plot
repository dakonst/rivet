BEGIN PLOT /BESIII_2022_I2102287/d01-x01-y01
Title=$\sigma(e^+e^-\to e^+e^-)$ ($34^0<\theta<146^0$)
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to e^+e^-)$/nb
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2022_I2102287/d02-x01-y01
Title=$\sigma(e^+e^-\to \mu^+\mu^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \mu^+\mu^-)$/nb
LogY=0
ConnectGaps=1
END PLOT
