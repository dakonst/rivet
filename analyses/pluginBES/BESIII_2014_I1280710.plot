BEGIN PLOT /BESIII_2014_I1280710/d01-x01-y01
Title=$K^+K^-$ mass distribution in $\chi_{c1}\to \eta^{\prime}K^+K^-$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1280710/d01-x01-y02
Title=$\eta^{\prime}K^\pm$ mass distribution in $\chi_{c1}\to \eta^{\prime}K^+K^-$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{\eta^{\prime}K^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^{\prime}K^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1280710/d02-x01-y01
Title=$K^+K^-$ mass distribution in $\chi_{c1}\to \eta^{\prime}K^+K^-$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1280710/d02-x01-y02
Title=$\eta^{\prime}K^\pm$ mass distribution in $\chi_{c1}\to \eta^{\prime}K^+K^-$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{\eta^{\prime}K^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^{\prime}K^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1280710/dalitz_1
Title=Dalitz plot for $\chi_{c1}\to \eta^{\prime}K^+K^-$
XLabel=$m^2_{\eta^{\prime}K^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\eta^{\prime}K^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\eta^{\prime}K^+}/\mathrm{d}m^2_{\eta^{\prime}K^-}$ [$\mathrm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2014_I1280710/d03-x01-y01
Title=$K^+K^-$ mass distribution in $\chi_{c2}\to \eta^{\prime}K^+K^-$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1280710/d03-x01-y02
Title=$\eta^{\prime}K^\pm$ mass distribution in $\chi_{c2}\to \eta^{\prime}K^+K^-$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{\eta^{\prime}K^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^{\prime}K^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1280710/d04-x01-y01
Title=$K^+K^-$ mass distribution in $\chi_{c2}\to \eta^{\prime}K^+K^-$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1280710/d04-x01-y02
Title=$\eta^{\prime}K^\pm$ mass distribution in $\chi_{c2}\to \eta^{\prime}K^+K^-$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{\eta^{\prime}K^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\eta^{\prime}K^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1280710/dalitz_2
Title=Dalitz plot for $\chi_{c2}\to \eta^{\prime}K^+K^-$
XLabel=$m^2_{\eta^{\prime}K^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\eta^{\prime}K^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\eta^{\prime}K^+}/\mathrm{d}m^2_{\eta^{\prime}K^-}$ [$\mathrm{GeV}^{-4}$]
LogY=0
END PLOT
