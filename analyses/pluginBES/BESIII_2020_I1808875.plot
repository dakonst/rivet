BEGIN PLOT /BESIII_2020_I1808875/d01-x01-y01
Title=$\sigma(e^+e^-\to \mu^+\mu^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \mu^+\mu^-)$ [nb]
LogY=0
ConnectGaps=1
END PLOT
