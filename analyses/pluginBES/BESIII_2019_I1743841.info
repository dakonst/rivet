Name: BESIII_2019_I1743841
Year: 2019
Summary: Cross section for $K^+K^-K^+K^-$ and $K^+K^-\phi$ for $E_{\text{CMS}}=2.10$ to $3.08$ GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1743841
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:1907.06015 [hep-ex]
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams: [e+,e-]
Energies: [2.1000, 2.1250, 2.1500, 2.1750, 2.2000, 2.2324, 2.3094, 2.3864, 2.3960, 2.5000, 2.6444, 2.6464, 2.7000, 2.8000, 2.9000, 2.9500, 2.9810, 3.0000, 3.0200, 3.0800]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for  $e^+e^- \to$  $K^+K^-K^+K^-$ and
   $K^+K^-\phi$ for center of mass energies between 2.10 and 3.08 GeV by the BESIII experiment.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords: []
BibKey: Ablikim:2019tpp
BibTeX: '@article{Ablikim:2019tpp,
      author         = "Ablikim, M. and others",
      title          = "{Cross section measurements of $e^{+}e^{-} \to
                        K^{+}K^{-}K^{+}K^{-} $ and $ \phi K^{+}K^{-}$ at
                        center-of-mass energies from 2.10 to 3.08 GeV}",
      collaboration  = "BESIII",
      year           = "2019",
      eprint         = "1907.06015",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      SLACcitation   = "%%CITATION = ARXIV:1907.06015;%%"
}'
