BEGIN PLOT /BESIII_2015_I1391798/d01-x01-y01
Title=Cross section for $e^+e^-\to Z_c(3885)^\pm\pi^\mp\to(DD^*)^\pm\pi^\mp$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1391798/d01-x01-y02
Title=Cross section for $e^+e^-\to Z_c(3885)^\pm\pi^\mp\to(DD^*)^\pm\pi^\mp$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1391798/d02-x01-y01
Title=Mass distribution for $D^0D^{*-}$ at $\sqrt{s}=4.23$ GeV
XLabel=$m_{D^0D^{*-}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^0D^{*-}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1391798/d02-x01-y02
Title=Mass distribution for $D^0D^{*-}$ at $\sqrt{s}=4.26$ GeV
XLabel=$m_{D^0D^{*-}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^0D^{*-}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2015_I1391798/d02-x02-y01
Title=Mass distribution for $D^-D^{*0}$ at $\sqrt{s}=4.23$ GeV
XLabel=$m_{D^-D^{*0}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^-D^{*0}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1391798/d02-x02-y02
Title=Mass distribution for $D^-D^{*0}$ at $\sqrt{s}=4.26$ GeV
XLabel=$m_{D^-D^{*0}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^-D^{*0}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2015_I1391798/d03-x01-y01
Title=$|\cos\theta_\pi|$ distribution for $Z_c(3385)^-\to D^0D^{*-}$
XLabel=$|\cos\theta_\pi|$
YLabel=$1/\sigma\text{d}\sigma/\text{d}|\cos\theta_\pi|$
END PLOT
BEGIN PLOT /BESIII_2015_I1391798/d03-x01-y02
Title=$|\cos\theta_\pi|$ distribution for $Z_c(3385)^-\to D^0D^{*-}$
XLabel=$|\cos\theta_\pi|$
YLabel=$1/\sigma\text{d}\sigma/\text{d}|\cos\theta_\pi|$
END PLOT
