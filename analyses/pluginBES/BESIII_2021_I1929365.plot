BEGIN PLOT /BESIII_2021_I1929365/d01-x01-y02
Title=$\pi^+\pi^0$  mass distribution in $D_s^+\to \pi^+\pi^0\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1929365/d01-x01-y01
Title=$\pi^0\pi^0$  mass distribution in $D_s^+\to \pi^+\pi^0\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1929365/dalitz
Title=Dalitz plot for $D_s^+\to \pi^+\pi^0\pi^0$
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\pi^0}/{\rm d}m^2_{\pi^+\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
