BEGIN PLOT /BESIII_2022_I2512962/d01-x01-y01
Title=$\sigma(e^+e^-\to \eta\Lambda\bar{\Lambda})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \eta\Lambda\bar{\Lambda})$/pb
ConnectGaps=1
LogY=0
END PLOT
