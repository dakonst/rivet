Name: BESIII_2015_I1355215
Year: 2015
Summary: Cross section for $e^+e^-\to J/\psi \eta$ at energies between 3.82 and 4.6 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 1355215
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev. D91 (2015) 112005, 2015 
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams: [e+,e-]
Energies:  [4.19, 4.21, 4.22, 4.23, 4.245, 4.26, 4.36, 4.42]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to J/\psi \eta$ at energies between 3.82 and 4.6 GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords: []
BibKey: Ablikim:2015xhk
BibTeX: '@article{Ablikim:2015xhk,
      author         = "Ablikim, M. and others",
      title          = "{Measurement of the $e^{+}e^{-} \to \eta J/\psi$ cross
                        section and search for $e^{+}e^{-} \to \pi^{0} J/\psi$ at
                        center-of-mass energies between 3.810 and 4.600 GeV}",
      collaboration  = "BESIII",
      journal        = "Phys. Rev.",
      volume         = "D91",
      year           = "2015",
      number         = "11",
      pages          = "112005",
      doi            = "10.1103/PhysRevD.91.112005",
      eprint         = "1503.06644",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      SLACcitation   = "%%CITATION = ARXIV:1503.06644;%%"
}'
