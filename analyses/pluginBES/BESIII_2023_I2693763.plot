BEGIN PLOT /BESIII_2023_I2693763/d01-x01-y01
Title=$K^0_{S1}\phi$ mass distribution in $\psi(2S)\to \phi K^0_SK^0_S$
XLabel=$m_{K^0_{S1}\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_{S1}\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2693763/d01-x01-y02
Title=$K^0_{S2}\phi$ mass distribution in $\psi(2S)\to \phi K^0_SK^0_S$
XLabel=$m_{K^0_{S2}\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_{S2}\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2693763/d01-x01-y03
Title=$K^0_{S1}K^0_{S2}$ mass distribution in $\psi(2S)\to \phi K^0_SK^0_S$
XLabel=$m_{K^0_{S1}K^0_{S2}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_{S1}K^0_{S2}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2693763/dalitz
Title=Dalitz plot for  $\psi(2S)\to \phi K^0_SK^0_S$
XLabel=$m^2_{K^0_{S1}\phi}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_{S2}\phi}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_{S1}\phi}/{\rm d}m^2_{K^0_{S2}\phi}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
