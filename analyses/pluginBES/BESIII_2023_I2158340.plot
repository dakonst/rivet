BEGIN PLOT /BESIII_2023_I2158340/d01-x01-y01
Title=Ratio $\sigma(e^+e^-\to \pi^0\pi^0\psi_2(3823))/\sigma(e^+e^-\to \pi^+\pi^-\psi_2(3823))$
XLabel=$\sqrt{s}$ [GeV]
YLabel=Ratio
LogY=0
END PLOT
