BEGIN PLOT /BESIII_2022_I2070086/d01-x01-y01
Title=$K^0_SK^+$ mass distribution in $D_s^+\to K^0_S K^+ \pi^0$
XLabel=$m_{K^0_SK^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_SK^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2070086/d01-x01-y02
Title=$K^0_S\pi^0$ mass distribution in $D_s^+\to K^0_S K^+ \pi^0$
XLabel=$m_{K^0_S\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2070086/d01-x01-y03
Title=$K^+\pi^0$ mass distribution in $D_s^+\to K^0_S K^+ \pi^0$
XLabel=$m_{K^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2070086/dalitz
Title=Dalitz plot for $D_s^+\to K^0_S K^+ \pi^0$
XLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^{2}$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^0_S\pi^0}/{\rm d}m^2_{K^+\pi0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT


