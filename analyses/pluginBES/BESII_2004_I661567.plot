BEGIN PLOT /BESII_2004_I661567/d01-x01-y01
Title=Cross section for $e^+e^-\to\pi^0\omega$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\pi^0\omega)$ [pb]
ConnectGaps=1
END PLOT
BEGIN PLOT /BESII_2004_I661567/d01-x01-y02
Title=Cross section for $e^+e^-\to\rho^0\eta$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\rho^0\eta)$ [pb]
ConnectGaps=1
END PLOT
BEGIN PLOT /BESII_2004_I661567/d01-x01-y03
Title=Cross section for $e^+e^-\to\rho^0\eta^\prime$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\rho^0\eta^\prime)$ [pb]
ConnectGaps=1
END PLOT
