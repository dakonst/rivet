BEGIN PLOT /BESIII_2024_I2771682/d01-x01-y01
Title=Cross section for $e^+e^-\to\eta \psi(2S)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\eta \psi(2S))$ [pb]
LogY=0
ConnectGaps=1
END PLOT
