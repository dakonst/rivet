BEGIN PLOT /BESIII_2023_I2674768
LogY=0
END PLOT

BEGIN PLOT /BESIII_2023_I2674768/d01-x01-y01
Title=$K^+K^-$ mass in $D_s^+\to K^+K^- \mu^+\nu_\mu$
XLabel=$m_{K^+K^-}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2023_I2674768/d01-x01-y02
Title=$\mu^+\nu_\mu$ mass squared in $D_s^+\to K^+K^- \mu^+\nu_\mu$
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BESIII_2023_I2674768/d01-x01-y04
Title=$\cos\theta_\mu$ in $D_s^+\to K^+K^- \mu^+\nu_\mu$
XLabel=$\cos\theta_\mu$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_\mu$ 
END PLOT

BEGIN PLOT /BESIII_2023_I2674768/d01-x01-y03
Title=$\cos\theta_K$ in $D_s^+\to K^+K^- \mu^+\nu_\mu$
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ 
END PLOT

BEGIN PLOT /BESIII_2023_I2674768/d01-x01-y05
Title=$\chi$ in $D_s^+\to K^+K^- \mu^+\nu_\mu$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
