Name: BESIII_2023_I2660219
Year: 2023
Summary:  Analysis of $J/\psi$ decays to $\Xi^0\bar{\Xi}^0$
Experiment: BESIII
Collider: BEPC
InspireID: 2660219
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2305.09218 [hep-ex]
RunInfo: e+e- > J/psi
NeedCrossSection: no
Beams: [e-, e+]
Energies: [3.1]
Description:
  'Analysis of the angular distribution of the baryons, and decay products, produced in $e^+e^-\to J/\psi \to \Xi^0\bar{\Xi}^0$.
   Gives information about the decay and is useful for testing correlations in hadron decays. The phase and $\alpha$ parameters were taken from the tables in the paper.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2023drj
BibTeX: '@article{BESIII:2023drj,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Precise Measurements of the Decay Parameters and $CP$ Asymmetries with Entangled $\Xi^0-\bar{\Xi}^0$ Pairs}",
    eprint = "2305.09218",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "5",
    year = "2023"
}
'
