BEGIN PLOT /BESIII_2018_I1689296/d01-x01-y01
Title= $K^0_SK^0_S$ mass in $J/\psi\to\gamma K^0_SK^0_S$
XLabel=$m_{K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2018_I1689296/d01-x01-y02
Title= $K^0_S\gamma$ mass in $J/\psi\to\gamma K^0_SK^0_S$
XLabel=$m_{K^0_S\gamma}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\gamma}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2018_I1689296/d02-x01-y01
Title=$\cos\theta_\gamma$ for $\gamma$ in $e^+e^-\to J/\psi\to\gamma K^0_SK^0_S$
XLabel=$\cos\theta_\gamma$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1689296/d02-x01-y02
Title=$\cos\theta_{K^0_S}$ for $\gamma$ in $e^+e^-\to J/\psi\to\gamma K^0_SK^0_S$
XLabel=$\cos\theta_{K^0_S}$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_{K^0_S}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1689296/d02-x01-y03
Title=$\phi_{K^0_S}$ for $\gamma$ in $e^+e^-\to J/\psi\to\gamma K^0_SK^0_S$
XLabel=$\phi_{K^0_S}$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\phi_{K^0_S}$
LogY=0
END PLOT
