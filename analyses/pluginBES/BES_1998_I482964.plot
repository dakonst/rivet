BEGIN PLOT /BES_1998_I482964/d01-x01-y01
Title=$K^+K^-\pi^0$ mass distribution in $J/\psi\to\gamma K^+K^-\pi^0$
XLabel=$m_{K^+K^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BES_1998_I482964/d01-x01-y02
Title=$K^\pm\pi^0$ mass distribution in $J/\psi\to\gamma K^+K^-\pi^0$
XLabel=$m_{K^\pm\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^\pm\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BES_1998_I482964/d01-x01-y03
Title=$K^+K^-$ mass distribution in $J/\psi\to\gamma K^+K^-\pi^0$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
