BEGIN PLOT /BESIII_2023_I2614192/d01-x01-y01
Title=Ratio of $\left|G_E/G_M\right|$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$R$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2614192/d01-x01-y02
Title=Ratio of $\left|G_M\right|$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$|G_M|\times10^2$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2614192/d01-x01-y03
Title=Ratio of $\left|G_E\right|$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$|G_M|\times10^2$
LogY=0
END PLOT
