Name: BESIII_2023_I2674768
Year: 2023
Summary: Kinematic distributions in $D^+_s\to K^+K^- \mu^+\nu_\mu$
Experiment: BESIII
Collider: BEPC
InspireID: 2674768
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2307.03024 [hep-ex]
RunInfo: Any process producing Ds mesons
Description:
  'Measurement of the kinematic distributions in $D^+_s\to K^+K^- \mu^+\nu_\mu$ by BESIII. N.B. the plots where read from the paper and may not have been corrected for acceptance.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2023opt
BibTeX: '@article{BESIII:2023opt,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Studies of the decay $D^+_s\to K^+K^- \mu^+ \nu_{\mu}$}",
    eprint = "2307.03024",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "7",
    year = "2023"
}
'
