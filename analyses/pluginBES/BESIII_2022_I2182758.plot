BEGIN PLOT /BESIII_2022_I2182758/d01-x01-y01
Title=$\sigma(e^+e^-\to K^0_SK^0_SJ/\psi)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to K^0_SK^0_SJ/\psi)$/pb
ConnectGaps=1
LogY=0
END PLOT
