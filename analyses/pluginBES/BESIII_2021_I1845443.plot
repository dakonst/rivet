BEGIN PLOT /BESIII_2021_I1845443/d01-x01-y01
Title=$\sigma(e^+e^-\to p\bar{p}\eta)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to p\bar{p}\eta)$/pb
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2021_I1845443/d02-x01-y01
Title=$\sigma(e^+e^-\to p\bar{p}\omega)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to p\bar{p}\omega)$/pb
ConnectGaps=1
END PLOT
