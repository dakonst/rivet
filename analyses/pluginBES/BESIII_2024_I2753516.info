Name: BESIII_2024_I2753516
Year: 2024
Summary: $\eta$ spectra for various energies between 2 and 3.671 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 2753516
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 133 (2024) 2, 021901
 - arXiv:2401.17873 [hep-ex]
RunInfo: e+e- > hadrons
Beams: [e+, e-]
Energies: [2.0, 2.2, 2.396, 2.6444, 2.9, 3.05, 3.5, 3.671]
Options:
 - ENERGY=2.0,2.2,2.396,2.6444,2.9,3.05,3.5000,3.671
Description:
'Measurement of the momentum spectrum for $\eta$ production in $e^+e^-$ collisions at low energies by BES.
 Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2024hcs
BibTeX: '@article{BESIII:2024hcs,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Measurements of Normalized Differential Cross Sections of Inclusive \ensuremath{\eta} Production in e+e- Annihilation at Energy from 2.0000 to 3.6710~GeV}",
    eprint = "2401.17873",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevLett.133.021901",
    journal = "Phys. Rev. Lett.",
    volume = "133",
    number = "2",
    pages = "021901",
    year = "2024"
}'
