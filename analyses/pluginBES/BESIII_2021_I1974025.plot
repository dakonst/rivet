BEGIN PLOT /BESIII_2021_I1974025
LogY=0
END PLOT

BEGIN PLOT /BESIII_2021_I1974025/d01
LogY=0
XLabel=$\cos\theta_\Lambda$
END PLOT

BEGIN PLOT /BESIII_2021_I1974025/d01-x01-y01
Title=$F_1$ for $\Lambda\to p^+\pi^-$ (N.B. data not corrected)
YLabel=$F_1$
END PLOT
BEGIN PLOT /BESIII_2021_I1974025/d01-x01-y02
Title=$F_2$ for $\Lambda\to p^+\pi^-$ (N.B. data not corrected)
YLabel=$F_2$
END PLOT
BEGIN PLOT /BESIII_2021_I1974025/d01-x01-y03
Title=$F_3$ for $\Lambda\to p^+\pi^-$ (N.B. data not corrected)
YLabel=$F_3$
END PLOT
BEGIN PLOT /BESIII_2021_I1974025/d01-x01-y04
Title=$F_4$ for $\Lambda\to p^+\pi^-$ (N.B. data not corrected)
YLabel=$F_4$
END PLOT
BEGIN PLOT /BESIII_2021_I1974025/d01-x01-y05
Title=$F_5$ for $\Lambda\to p^+\pi^-$ (N.B. data not corrected)
YLabel=$F_5$
END PLOT


BEGIN PLOT /BESIII_2021_I1974025/d01-x01-y06
Title=Cross section vs polar angle (N.B. data not corrected)
LogY=0
XLabel=$\cos\theta_\Lambda$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_L$
END PLOT

BEGIN PLOT /BESIII_2021_I1974025/d02-x01-y01
Title=$\mu$ for $\Lambda\to p^+\pi^-$ (N.B. data not corrected)
YLabel=$\mu$
XLabel=$\cos\theta_{\Lambda}$
END PLOT

BEGIN PLOT /BESIII_2021_I1974025/d03-x01-y01
Title=$\alpha_\psi$ for $e^+e^-\to \Lambda^0\bar\Lambda^0$
YLabel=$\alpha_\psi$
END PLOT
BEGIN PLOT /BESIII_2021_I1974025/d03-x01-y02
Title=$\Delta\Phi$ for $e^+e^-\to \Lambda^0\bar\Lambda^0$
YLabel=$\Delta\Phi$ [degrees]
END PLOT
