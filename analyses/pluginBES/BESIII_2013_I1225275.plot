BEGIN PLOT /BESIII_2013_I1225275/d01-x01-y01
Title=Cross section for $e^+e^-\to J/\psi\psi\pi^+\pi^-$
YLabel=$\sigma(e^+e^-\to J/\psi\pi^+\pi^-)$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT

BEGIN PLOT /BESIII_2013_I1225275/d02-x01-y01
Title=$J/\psi\pi^+$ mass distribution in $e^+e^-\to J/\psi\pi^+\pi^-$ at $\sqrt{s}=4.26$\,GeV
XLabel=$m_{J/\psi\pi^+}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{J/\psi\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1225275/d02-x01-y02
Title=$J/\psi\pi^-$ mass distribution in $e^+e^-\to J/\psi\pi^+\pi^-$ at $\sqrt{s}=4.26$\,GeV
XLabel=$m_{J/\psi\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{J/\psi\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1225275/d02-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $e^+e^-\to J/\psi\pi^+\pi^-$ at $\sqrt{s}=4.26$\,GeV
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2013_I1225275/d03-x01-y01
Title=$\max(J/\psi\pi^\pm)$ mass distribution in $e^+e^-\to J/\psi\pi^+\pi^-$ at $\sqrt{s}=4.26$\,GeV
XLabel=$m_{\max(J/\psi\pi^\pm)}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\max(J/\psi\pi^\pm)}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
