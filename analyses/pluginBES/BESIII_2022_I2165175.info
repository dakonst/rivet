Name: BESIII_2022_I2165175
Year: 2022
Summary: Cross section for $e^+e^-\to\phi\eta^\prime$ between 3.508 and 4.6 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 2165175
Status: VALIDATED NOHEPDATA
Reentrant: false
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2210.06988 [hep-ex]
RunInfo: e+ e-
Beams: [e+, e-]
Energies: [3.508, 3.5106, 3.773, 3.86741, 3.87131, 4.00762, 4.178, 4.18899, 4.19903, 4.20925,
           4.21884, 4.22626, 4.23582, 4.24393, 4.25797, 4.2668, 4.27774, 4.35826, 4.41558, 4.59953]
Options:
 - ENERGY=*
Description:
  'Cross section for $e^+e^-\to\phi\eta^\prime$ between 3.508 and 4.6 GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022tjc
BibTeX: '@article{BESIII:2022tjc,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Measurement of $e^+ e^- \rightarrow \phi \eta^{\prime}$ cross sections at center-of-mass energies between 3.508 and 4.600 GeV}",
    eprint = "2210.06988",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "10",
    year = "2022"
}
'
