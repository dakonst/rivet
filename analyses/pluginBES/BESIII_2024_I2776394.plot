BEGIN PLOT /BESIII_2024_I2776394/d01-x01-y01
Title=Cross section for $e^+e^-\to\eta^\prime\omega$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\eta^\prime\omega)$ [pb]
ConnectGaps=1
END PLOT
