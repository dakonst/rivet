BEGIN PLOT /BESIII_2024_I2748736/d01-x01-y01
Title=Cross section for $e^+e^-\to\Sigma^+\bar{\Sigma}^-$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\Sigma^+\bar{\Sigma}^-)$ [fb]
ConnectGaps=1
LogY=1
END PLOT
