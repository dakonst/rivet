BEGIN PLOT /BESIII_2018_I1646687/d01-x01-y01
Title= $\gamma\phi$ mass in $J/\psi\to\gamma \gamma\phi$
XLabel=$m_{\gamma\phi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\gamma\phi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1646687/d02-x01-y01
Title=$\cos\theta_\gamma$ for $\gamma$ in $e^+e^-\to J/\psi\to\gamma (\gamma\phi)$ ($1.4<m_{\gamma\phi}<1.6$GeV)
XLabel=$\cos\theta_\gamma$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1646687/d02-x01-y02
Title=$\cos\theta_\gamma$ for $\gamma$ in $e^+e^-\to J/\psi\to\gamma (\gamma\phi)$ ($1.75<m_{\gamma\phi}<1.9$GeV)
XLabel=$\cos\theta_\gamma$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_\gamma$
LogY=0
END PLOT
