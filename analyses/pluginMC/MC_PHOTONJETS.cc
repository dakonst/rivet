// -*- C++ -*-
#include "Rivet/Analyses/MC_JETS_BASE.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief MC validation analysis for photon + jets events
  class MC_PHOTONJETS : public MC_JETS_BASE {
  public:

    /// Default constructor
    MC_PHOTONJETS()
      : MC_JETS_BASE("MC_PHOTONJETS", 4, "Jets")
    {    }


    /// @name Analysis methods
    /// @{

    /// Book histograms
    void init() {
      // General FS
      FinalState fs((Cuts::etaIn(-5.0, 5.0)));
      declare(fs, "FS");

      // set ptcut from input option
      const double jetptcut = getOption<double>("PTJMIN", 20.0);
      _jetptcut = jetptcut * GeV;

      // set clustering radius from input option
      const double R = getOption<double>("R", 0.4);

      // set clustering algorithm from input option
      JetAlg clusterAlgo;
      const string algoopt = getOption("ALGO", "ANTIKT");
      if ( algoopt == "KT" ) {
	clusterAlgo = JetAlg::KT;
      } else if ( algoopt == "CA" ) {
	clusterAlgo = JetAlg::CA;
      } else if ( algoopt == "ANTIKT" ) {
	clusterAlgo = JetAlg::ANTIKT;
      } else {
	MSG_WARNING("Unknown jet clustering algorithm option " + algoopt + ". Defaulting to anti-kT");
	clusterAlgo = JetAlg::ANTIKT;
      }
      
      // set photon cuts from input options
      const double etacut = getOption<double>("ABSETAGAMMAX", 2.5);
      const double ptcut = getOption<double>("PTGAMMIN", 30.);
      
      // Get leading photon
      LeadingParticlesFinalState photonfs(FinalState(Cuts::abseta < etacut && Cuts::pT >= ptcut*GeV));
      photonfs.addParticleId(PID::PHOTON);
      declare(photonfs, "LeadingPhoton");

      // FS for jets excludes the leading photon
      VetoedFinalState vfs(fs);
      vfs.addVetoOnThisFinalState(photonfs);
      declare(vfs, "JetFS");
      FastJets jetpro(vfs, clusterAlgo, R);
      declare(jetpro, "Jets");

      book(_h_photon_jet1_deta ,"photon_jet1_deta", 50, -5.0, 5.0);
      book(_h_photon_jet1_dphi ,"photon_jet1_dphi", 20, 0.0, M_PI);
      book(_h_photon_jet1_dR ,"photon_jet1_dR", 25, 0.5, 7.0);

      MC_JETS_BASE::init();
    }


    /// Do the analysis
    void analyze(const Event& e) {
      // Get the photon
      /// @todo share IsolatedPhoton projection between all MC_*PHOTON* analyses
      const Particles photons = apply<FinalState>(e, "LeadingPhoton").particles();
      if (photons.size() != 1) {
        vetoEvent;
      }
      const FourMomentum photon = photons.front().momentum();

      // Get all charged particles
      const FinalState& fs = apply<FinalState>(e, "JetFS");
      if (fs.empty()) {
        vetoEvent;
      }

      // Passed cuts, so get the weight

      // Isolate photon by ensuring that a 0.4 cone around it contains less than 7% of the photon's energy
      const double egamma = photon.E();
      double econe = 0.0;
      for (const Particle& p : fs.particles()) {
        if (deltaR(photon, p.momentum()) < 0.4) {
          econe += p.E();
          // Veto as soon as E_cone gets larger
          if (econe/egamma > 0.07) {
            vetoEvent;
          }
        }
      }

      const Jets& jets = apply<FastJets>(e, "Jets").jetsByPt(Cuts::pT > _jetptcut);
      if (jets.size()>0) {
        _h_photon_jet1_deta->fill(photon.eta()-jets[0].eta());
        _h_photon_jet1_dphi->fill(mapAngle0ToPi(photon.phi()-jets[0].phi()));
        _h_photon_jet1_dR->fill(deltaR(photon, jets[0].momentum()));
      }

      MC_JETS_BASE::analyze(e);
    }


    // Finalize
    void finalize() {
      scale(_h_photon_jet1_deta, crossSectionPerEvent());
      scale(_h_photon_jet1_dphi, crossSectionPerEvent());
      scale(_h_photon_jet1_dR, crossSectionPerEvent());

      MC_JETS_BASE::finalize();
    }

    /// @}


  private:

    /// @name Histograms
    /// @{
    Histo1DPtr _h_photon_jet1_deta;
    Histo1DPtr _h_photon_jet1_dphi;
    Histo1DPtr _h_photon_jet1_dR;
    /// @}

  };



  RIVET_DECLARE_PLUGIN(MC_PHOTONJETS);

}
