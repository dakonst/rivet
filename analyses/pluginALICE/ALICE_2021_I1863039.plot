BEGIN PLOT /ALICE_2021_I1863039/d01-x01-y01
Title=Prompt $\Xi^0_c$ differential cross section ($|y|<0.5$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /ALICE_2021_I1863039/d02-x01-y01
Title=Inclusive $\Xi^0_c$ differential cross section ($|y|<0.5$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /ALICE_2021_I1863039/d03-x01-y01
Title=Ratio of Prompt $\Xi^0_c$ to $D^0$ ($|y|<0.5$)
XLabel=$p_\perp$ [GeV]
YLabel=$\sigma(\Xi_c^0)/\sigma(D^0)$
END PLOT
BEGIN PLOT /ALICE_2021_I1863039/d04-x01-y01
Title=Ratio of inclusive $\Xi^0_c$ to prompt $D^0$ ($|y|<0.5$)
XLabel=$p_\perp$ [GeV]
YLabel=$\sigma(\Xi_c^0)/\sigma(D^0)$
END PLOT
BEGIN PLOT /ALICE_2021_I1863039/d05-x01-y01
Title=Prompt $\Xi^0_c$ cross section ($|y|<0.5$)
YLabel=$\sigma$ [$\mu$b]
XLabel=0
END PLOT
BEGIN PLOT /ALICE_2021_I1863039/d06-x01-y01
Title=Ratio of Prompt $\Xi^0_c$ to $D^0$ ($|y|<0.5$)
XLabel=
YLabel=$\sigma(\Xi_c^0)/\sigma(D^0)$
END PLOT
