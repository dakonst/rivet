BEGIN PLOT /ALICE_2019_I1735351/d02-x01-y01
Title=J/$\psi$ transverse momentum $|y|<0.9$ $\sqrt{s}=5.02$\,TeV
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
