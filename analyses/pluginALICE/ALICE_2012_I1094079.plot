BEGIN PLOT /ALICE_2012_I1094079/d01-x01-y01
Title=$J/\psi$ transverse momentum $2.5 < |y| < 4.$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /ALICE_2012_I1094079/d04-x01-y01
Title=$J/\psi$ rapidity
XLabel=$y^{J/\psi}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [$\mu$b]
END PLOT
