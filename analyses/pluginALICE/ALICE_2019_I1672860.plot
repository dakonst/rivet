BEGIN PLOT /ALICE_2019_I1672860/d06-x01-y01
Title=$\rho^{0}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
RatioPlotYMin=0.1
RatioPlotYMax=2
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d06-x01-y01-pion-pp
Title=$\pi^{\pm}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d07-x01-y01
Title=$\rho^{0}$, Pb-Pb, $\sqrt{s_\mathrm{NN}}=2.76$ TeV, $|y| < 0.5$. 0-20\% class
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
RatioPlotYMin=0.1
RatioPlotYMax=2
END PLOT


BEGIN PLOT /ALICE_2019_I1672860/d07-x01-y01-pp
Title=$\rho^{0}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT


BEGIN PLOT /ALICE_2019_I1672860/d07-x01-y01-pion
Title=$\pi^{\pm}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d07-x01-y01-pion-pp
Title=$\pi^{\pm}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d07-x01-y01-rho_pion_ratio-pp
Title=$pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=Ratio
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d08-x01-y01
Title=$\rho^{0}$, Pb-Pb, $\sqrt{s_\mathrm{NN}}=2.76$ TeV, $|y| < 0.5$. 20-40\% class
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
RatioPlotYMin=0.1
RatioPlotYMax=2
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d08-x01-y01-pp
Title=$\rho^{0}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d08-x01-y01-pion
Title=$\pi^{\pm}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d08-x01-y01-pion-pp
Title=$\pi^{\pm}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d08-x01-y01-rho_pion_ratio-pp
Title=pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=Ratio
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d08-x01-y01-rho_pion_ratio
Title=Pb-Pb, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=Ratio
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d09-x01-y01
Title=$\rho^{0}$, Pb-Pb, $\sqrt{s_\mathrm{NN}}=2.76$ TeV, $|y| < 0.5$. 40-60\% class
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
RatioPlotYMin=0.1
RatioPlotYMax=2
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d09-x01-y01-pp
Title=$\rho^{0}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d09-x01-y01-pion
Title=$\pi^{\pm}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d09-x01-y01-pion-pp
Title=$\pi^{\pm}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d09-x01-y01-rho_pion_ratio-pp
Title=pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=Ratio
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d09-x01-y01-rho_pion_ratio
Title=Pb-Pb, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=Ratio
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d10-x01-y01
Title=$\rho^{0}$, Pb-Pb, $\sqrt{s_\mathrm{NN}}=2.76$ TeV, $|y| < 0.5$. 60-80\% class
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
RatioPlotYMin=0.1
RatioPlotYMax=2
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d10-x01-y01-pp
Title=$\rho^{0}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT


BEGIN PLOT /ALICE_2019_I1672860/d10-x01-y01-pion
Title=$\pi^{\pm}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d10-x01-y01-pion-pp
Title=$\pi^{\pm}$, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\frac{1}{N}\frac{\mathrm{d}^2N}{\mathrm{d}p_\mathrm{T}\mathrm{d}y}$ ($c$/GeV)
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d10-x01-y01-rho_pion_ratio-pp
Title=$pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=Ratio
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d11-x01-y01
Title=$\rho/\pi$ integrated yield ratio, $\sqrt{s}=2.76$ TeV
XLabel=$\langle \mathrm{d}N_{{\rm ch}}/\mathrm{d}\eta \rangle ^{1/3}$
YLabel=$\rho/\pi$ ratio
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d12-x01-y01
Title=$\rho$, $\langle p_{\rm{T}} \rangle$, $\sqrt{s}=2.76$ TeV
XLabel=$\langle \mathrm{d}N_{{\rm ch}}/\mathrm{d}\eta \rangle ^{1/3}$
YLabel=$\langle p_\mathrm{T} \rangle$
END PLOT


BEGIN PLOT /ALICE_2019_I1672860/d13-x01-y01
Title=$\rho/\pi$ ratio, pp, $\sqrt{s}=2.76$ TeV
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\rho/\pi$ ratio
LogY=0
YMin=0.
YMax=1.5
RatioPlotYMin=0.1
RatioPlotYMax=3.4
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d14-x01-y01
Title=$\rho/\pi$ ratio, Pb-Pb, $\sqrt{s_\mathrm{NN}}=2.76$ TeV, $|y| < 0.5$. 0-20\% class
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\rho/\pi$ ratio
LogY=0
YMin=0.
YMax=1.5
RatioPlotYMin=0.1
RatioPlotYMax=3.4
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d15-x01-y01
Title=$\rho/\pi$ ratio, Pb-Pb, $\sqrt{s_\mathrm{NN}}=2.76$ TeV, $|y| < 0.5$. 60-80\% class
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$\rho/\pi$ ratio
LogY=0
YMin=0.
YMax=1.5
RatioPlotYMin=0.1
RatioPlotYMax=3.4
END PLOT


BEGIN PLOT /ALICE_2019_I1672860/d16-x01-y01
Title=$R_\mathrm{AA}$, Pb-Pb, $\sqrt{s_\mathrm{NN}}=2.76$ TeV, $|y| < 0.5$. 0-20\% class
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$R_\mathrm{AA}$
RatioPlotYMin=0.1
RatioPlotYMax=3.4
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d17-x01-y01
Title=$R_\mathrm{AA}$, Pb-Pb, $\sqrt{s_\mathrm{NN}}=2.76$ TeV, $|y| < 0.5$. 20-40\% class
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$R_\mathrm{AA}$
RatioPlotYMin=0.1
RatioPlotYMax=3.4
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d18-x01-y01
Title=$R_\mathrm{AA}$, Pb-Pb, $\sqrt{s_\mathrm{NN}}=2.76$ TeV, $|y| < 0.5$. 40-60\% class
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$R_\mathrm{AA}$
RatioPlotYMin=0.1
RatioPlotYMax=3.4
END PLOT

BEGIN PLOT /ALICE_2019_I1672860/d19-x01-y01
Title=$R_\mathrm{AA}$, Pb-Pb, $\sqrt{s_\mathrm{NN}}=2.76$ TeV, $|y| < 0.5$. 60-80\% class
XLabel=$p_\mathrm{T}$ (GeV/$c$)
YLabel=$R_\mathrm{AA}$
RatioPlotYMin=0.1
RatioPlotYMax=3.4
END PLOT

