BEGIN PLOT /FOCUS_2007_I741543/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2007_I741543/d01-x01-y02
Title=Maximum $\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$\max(m_{\pi^+\pi^-})$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\max(m_{\pi^+\pi^-})$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2007_I741543/d01-x01-y03
Title=Minimum $\pi^+\pi^-$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$\min(m_{\pi^+\pi^-})$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\min(m_{\pi^+\pi^-})$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2007_I741543/d01-x01-y04
Title=$\pi^\pm\pi^\pm$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$m_{\pi^\pm\pi^\pm}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^\pm\pi^\pm}$ [$\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2007_I741543/d02-x01-y01
Title=$\pi^\pm\pi^\pm\pi^\mp$ mass distribution in $D^0\to 2\pi^+2\pi^-$
XLabel=$m_{\pi^\pm\pi^\pm\pi^\mp}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}(m_{\pi^\pm\pi^\pm\pi^\mp}$ [$\text{GeV}$]
LogY=0
END PLOT
