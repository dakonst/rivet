BEGIN PLOT /FOCUS_2003_I618864/d01-x01-y01
Title=$\pi^+K^-$ mass distribution in $\Xi_c^0\to \Sigma^+K^-\pi^+$
XLabel=$m_{\pi^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2003_I618864/d02-x01-y01
Title=$\Xi^-\pi^+$ mass distribution in $\Xi_c^0\to \Xi^-\pi^+\pi^+$
XLabel=$m_{\Xi^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Xi^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
