# BEGIN PLOT /NUSEA_2003_I613362/DiMuon_pT
YMin=1E-5
YMax=10
Title=Drell-Yan production at $\sqrt{s}=38.8$ GeV
XLabel=$p_T$ (GeV)
YLabel=$1/\sigma d\sigma/dp_T$ (GeV$^{-1}$)
RatioPlot=0

# END PLOT

# BEGIN PLOT /NUSEA_2003_I613362/d0*
Title=Drell-Yan $\sqrt{s}=38.8$ GeV
#LogY=0
#LogX=0
# END PLOT
# BEGIN PLOT /NUSEA_2003_I613362/d1*
Title=NuSea: Drell-Yan $\sqrt{s}=38.8$ GeV
#LogY=0
#LogX=0
# END PLOT

# BEGIN PLOT /NUSEA_2003_I613362/d01-x01-y01
#Title=NuSea: Drell-Yan $\sqrt{s}=38.8$ GeV
XLabel=$M_{\mu^+\mu^-}$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$-0.05 < x_F < 0.05$


# END PLOT


# BEGIN PLOT /NUSEA_2003_I613362/d02-x01-y01
#Title=NuSea: Drell-Yan
YMin=0.5
YMax=50
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.05 < x_F < 0.1$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d03-x01-y01
#Title=NuSea: Drell-Yan
YMin=0.5
YMax=100
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.1 < x_F < 0.15$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d04-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.15 < x_F < 0.2$


# END PLOT




# BEGIN PLOT /NUSEA_2003_I613362/d05-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.2 < x_F < 0.25$


# END PLOT




# BEGIN PLOT /NUSEA_2003_I613362/d06-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.25 < x_F < 0.3$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d07-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.3 < x_F < 0.35$


# END PLOT


# BEGIN PLOT /NUSEA_2003_I613362/d08-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.35 < x_F < 0.4$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d09-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.4 < x_F < 0.45$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d10-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.45 < x_F < 0.5$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d11-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.5 < x_F < 0.55$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d12-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.55 < x_F < 0.6$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d13-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.6 < x_F < 0.65$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d14-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.65 < x_F < 0.7$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d15-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.7 < x_F < 0.75$


# END PLOT



# BEGIN PLOT /NUSEA_2003_I613362/d16-x01-y01
#Title=NuSea: Drell-Yan
XLabel=$M$ (GeV)
YLabel=$M^3d^2\sigma/dMdx_F$ (nb GeV$^2$)
LegendTitle=$0.75 < x_F < 0.8$


# END PLOT
# BEGIN PLOT /NUSEA_2003_I613362/d40*
# END PLOT


# BEGIN PLOT /NUSEA_2003_I613362/d40-x01-y01
LogY=1
LogX=0
YMin=0.01
YMax=300
#YMin=0.0001
#XMax=2.
Title=NuSea: Drell-Yan $\sqrt{s}=38.8$ GeV, $-0.05 < x_F < 0.15$
XLabel=$p_T$ (GeV)
YLabel=$(2 E)/(\pi \sqrt{s}) d\sigma/dx_Fdp_T^2$ (pb/GeV)
LegendTitle=$4.2 < M_{\mu^+\mu^-} < 5.2 $ GeV
GofType=chi2
# END PLOT

# BEGIN PLOT /NUSEA_2003_I613362/d40-x01-y02
LogY=1
LogX=0
Title=NuSea: Drell-Yan $\sqrt{s}=38.8$ GeV, $-0.05 < x_F < 0.15$
XLabel=$p_T$ (GeV)
YLabel=$(2 E)/(\pi \sqrt{s}) d\sigma/dx_Fdp_T^2$ (pb/GeV)
LegendTitle=$5.2 < M_{\mu^+\mu^-} < 6.2 $ GeV
#GofType=chi2
# END PLOT

# BEGIN PLOT /NUSEA_2003_I613362/d40-x01-y03
LogY=1
LogX=0
Title=NuSea: Drell-Yan $\sqrt{s}=38.8$ GeV, $-0.05 < x_F < 0.15$
XLabel=$p^{\mu+^+\mu^-}_T$ (GeV)
YLabel=$(2 E)/(\pi \sqrt{s}) d\sigma/dx_Fdp_T^2$ (pb/GeV)
LegendTitle=$6.2 < M_{\mu^+\mu^-} < 7.2 $ GeV
# END PLOT

# BEGIN PLOT /NUSEA_2003_I613362/d40-x01-y04
LogY=1
LogX=0
Title=NuSea: Drell-Yan $\sqrt{s}=38.8$ GeV, $-0.05 < x_F < 0.15$
XLabel=$p_T$ (GeV)
YLabel=$(2 E)/\pi \sqrt{s}) d\sigma/dx_Fdp_T^2$ (pb/GeV)
LegendTitle=$7.2 < M_{\mu^+\mu^-} < 8.7 $ GeV
# END PLOT

# BEGIN PLOT /NUSEA_2003_I613362/d40-x01-y05
LogY=1
LogX=0
Title=NuSea: Drell-Yan $\sqrt{s}=38.8$ GeV, $-0.05 < x_F < 0.15$
XLabel=$p_T$ (GeV)
YLabel=$(2 E)/(\pi \sqrt{s}) d\sigma/dx_Fdp_T^2$ (pb/GeV)
LegendTitle=$10.2< M_{\mu^+\mu^-} < 12.85 $ GeV
# END PLOT


# ... add more histograms as you need them ...
