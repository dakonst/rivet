BEGIN PLOT /CMS_2015_I1342266/.*
FullRange=1
LogY=1
LogX=1
END PLOT

BEGIN PLOT /CMS_2015_I1342266/d01-x01-y01
Title=$\Upsilon(1S)$ transverse momentum $0 < |y| < 0.6$
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1342266/d02-x01-y01
Title=$\Upsilon(1S)$ transverse momentum $0.6 < |y| < 1.2$
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1342266/d03-x01-y01
Title=$\Upsilon(1S)$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT

BEGIN PLOT /CMS_2015_I1342266/d01-x01-y02
Title=$\Upsilon(2S)$ transverse momentum $0 < |y| < 0.6$
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1342266/d02-x01-y02
Title=$\Upsilon(2S)$  transverse momentum $0.6 < |y| < 1.2$
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1342266/d03-x01-y02
Title=$\Upsilon(2S)$  transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT

BEGIN PLOT /CMS_2015_I1342266/d01-x01-y03
Title=$\Upsilon(3S)$  transverse momentum $0 < |y| < 0.6$
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1342266/d02-x01-y03
Title=$\Upsilon(3S)$  transverse momentum $0.6 < |y| < 1.2$
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /CMS_2015_I1342266/d03-x01-y03
Title=$\Upsilon(3S)$  transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT

BEGIN PLOT /CMS_2015_I1342266/d04-x01-y01
Title=Ratio of $\Upsilon(2S)$ to $\Upsilon(1S)$ vs transverse momentum $0 < |y| < 0.6$
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=$R_{32}$
END PLOT
BEGIN PLOT /CMS_2015_I1342266/d04-x01-y02
Title=Ratio of $\Upsilon(3S)$ to $\Upsilon(1S)$ vs transverse momentum $0 < |y| < 0.6$
XLabel=$p^{\Upsilon}_\perp$ [GeV]
YLabel=$R_{31}$
END PLOT

BEGIN PLOT /CMS_2015_I1342266/d05-x01-y01
Title=Ratio of $\Upsilon(2S)$ to $\Upsilon(1S)$ vs transverse momentum $0.6 < |y| < 1.2$
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=$R_{32}$
END PLOT
BEGIN PLOT /CMS_2015_I1342266/d05-x01-y02
Title=Ratio of $\Upsilon(3S)$ to $\Upsilon(1S)$ vs transverse momentum $0.6 < |y| < 1.2$
XLabel=$p^{\Upsilon}_\perp$ [GeV]
YLabel=$R_{31}$
END PLOT

BEGIN PLOT /CMS_2015_I1342266/d06-x01-y01
Title=Ratio of $\Upsilon(2S)$ to $\Upsilon(1S)$ vs transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=$R_{32}$
END PLOT
BEGIN PLOT /CMS_2015_I1342266/d06-x01-y02
Title=Ratio of $\Upsilon(3S)$ to $\Upsilon(1S)$ vs transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\Upsilon}_\perp$ [GeV]
YLabel=$R_{31}$
END PLOT
