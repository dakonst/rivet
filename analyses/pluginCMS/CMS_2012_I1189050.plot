BEGIN PLOT /CMS_2012_I1189050/d01-x01-y01
Title=Prompt $\chi_{c1}$/$\chi_{c2}$ as function of $p_\perp$ J/$\psi$ in $J/\psi\gamma$ ($p_\perp^\gamma>0.5$\,GeV)
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel =$N(\chi_{c1}\to J/\psi\gamma)/N(\chi_{c2}\to J/\psi\gamma)$
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I1189050/d02-x01-y01
Title=Prompt $\chi_{c1}$/$\chi_{c2}$ as function of $p_\perp$ J/$\psi$ in $J/\psi\gamma$ ($p_\perp^\gamma>0.5$\,GeV)
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel =$N(\chi_{c1})/N(\chi_{c2})$
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I1189050/d03-x01-y01
Title=Prompt $\chi_{c1}$/$\chi_{c2}$ as function of $p_\perp$ J/$\psi$ in $J/\psi\gamma$ ($p_\perp^\gamma>0$\,GeV)
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel =$N(\chi_{c1}\to J/\psi\gamma)/N(\chi_{c2}\to J/\psi\gamma)$
LogY=0
END PLOT
