# BEGIN PLOT /CMS_2012_I1090423/d0
XLabel=$\chi_\mathrm{dijet}$
YLabel=$1/\sigma_\mathrm{dijet} \, d\sigma_\mathrm{dijet} / d\chi_\mathrm{dijet}$
LogY=0
YMax=0.12
# END PLOT

# BEGIN PLOT /CMS_2012_I1090423/d01-x01-y01
Title=$\chi_\mathrm{dijet}$ for $M_{jj} > 3.0\,\mathrm{TeV}$, $|y_1+y_2|/2 < 1.11$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2012_I1090423/d02-x01-y01
Title=$\chi_\mathrm{dijet}$ for $2.4\,\mathrm{TeV} < M_{jj} < 3.0\,\mathrm{TeV}$, $|y_1+y_2|/2 < 1.11$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2012_I1090423/d03-x01-y01
Title=$\chi_\mathrm{dijet}$ for $1.9\,\mathrm{TeV} < M_{jj} < 2.4\,\mathrm{TeV}$, $|y_1+y_2|/2 < 1.11$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2012_I1090423/d04-x01-y01
Title=$\chi_\mathrm{dijet}$ for $1.5\,\mathrm{TeV} < M_{jj} < 1.9\,\mathrm{TeV}$, $|y_1+y_2|/2 < 1.11$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2012_I1090423/d05-x01-y01
Title=$\chi_\mathrm{dijet}$ for $1.2\,\mathrm{TeV} < M_{jj} < 1.5\,\mathrm{TeV}$, $|y_1+y_2|/2 < 1.11$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2012_I1090423/d06-x01-y01
Title=$\chi_\mathrm{dijet}$ for $1.0\,\mathrm{TeV} < M_{jj} < 1.2\,\mathrm{TeV}$, $|y_1+y_2|/2 < 1.11$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2012_I1090423/d07-x01-y01
Title=$\chi_\mathrm{dijet}$ for $0.8\,\mathrm{TeV} < M_{jj} < 1.0\,\mathrm{TeV}$, $|y_1+y_2|/2 < 1.11$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2012_I1090423/d08-x01-y01
Title=$\chi_\mathrm{dijet}$ for $0.6\,\mathrm{TeV} < M_{jj} < 0.8\,\mathrm{TeV}$, $|y_1+y_2|/2 < 1.11$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /CMS_2012_I1090423/d09-x01-y01
Title=$\chi_\mathrm{dijet}$ for $0.4\,\mathrm{TeV} < M_{jj} < 0.6\,\mathrm{TeV}$, $|y_1+y_2|/2 < 1.11$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT
