
# BEGIN PLOT /CMS_2024_I2760466/d01-x01-y01
Title=CMS, 13 TeV, $97 < p_\mathrm{T0} < 220$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E2C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d02-x01-y01
Title=CMS, 13 TeV, $220 < p_\mathrm{T0} < 330$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E2C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d03-x01-y01
Title=CMS, 13 TeV, $330 < p_\mathrm{T0} < 468$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E2C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d04-x01-y01
Title=CMS, 13 TeV, $468 < p_\mathrm{T0} < 638$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E2C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d05-x01-y01
Title=CMS, 13 TeV, $638 < p_\mathrm{T0} < 846$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E2C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d06-x01-y01
Title=CMS, 13 TeV, $846 < p_\mathrm{T0} < 1101$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E2C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d07-x01-y01
Title=CMS, 13 TeV, $1101 < p_\mathrm{T0} < 1410$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E2C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d08-x01-y01
Title=CMS, 13 TeV, $1410 < p_\mathrm{T0} < 1784$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E2C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d09-x01-y01
Title=CMS, 13 TeV, $97 < p_\mathrm{T0} < 220$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E3C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d10-x01-y01
Title=CMS, 13 TeV, $220 < p_\mathrm{T0} < 330$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E3C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d11-x01-y01
Title=CMS, 13 TeV, $330 < p_\mathrm{T0} < 468$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E3C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d12-x01-y01
Title=CMS, 13 TeV, $468 < p_\mathrm{T0} < 638$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E3C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d13-x01-y01
Title=CMS, 13 TeV, $638 < p_\mathrm{T0} < 846$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E3C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d14-x01-y01
Title=CMS, 13 TeV, $846 < p_\mathrm{T0} < 1101$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E3C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d15-x01-y01
Title=CMS, 13 TeV, $1101 < p_\mathrm{T0} < 1410$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E3C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d16-x01-y01
Title=CMS, 13 TeV, $1410 < p_\mathrm{T0} < 1784$ GeV
XMin=2e-4
XMax=0.4
LogX=1
LogY=1
XLabel=$x_\mathrm{L}$
YLabel=E3C
RatioPlotYMin=0.7
RatioPlotYMax=1.3
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d26-x01-y01
Title=CMS, 13 TeV, $97 < p_\mathrm{T0} < 220$ GeV
XMin=2e-4
XMax=0.4
LogX=1
XLabel=$x_\mathrm{L}$
YLabel=E3C/E2C
RatioPlotYMin=0.9
RatioPlotYMax=1.1
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d27-x01-y01
Title=CMS, 13 TeV, $220 < p_\mathrm{T0} < 330$ GeV
XMin=2e-4
XMax=0.4
LogX=1
XLabel=$x_\mathrm{L}$
YLabel=E3C/E2C
RatioPlotYMin=0.9
RatioPlotYMax=1.1
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d28-x01-y01
Title=CMS, 13 TeV, $330 < p_\mathrm{T0} < 468$ GeV
XMin=2e-4
XMax=0.4
LogX=1
XLabel=$x_\mathrm{L}$
YLabel=E3C/E2C
RatioPlotYMin=0.9
RatioPlotYMax=1.1
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d29-x01-y01
Title=CMS, 13 TeV, $468 < p_\mathrm{T0} < 638$ GeV
XMin=2e-4
XMax=0.4
LogX=1
XLabel=$x_\mathrm{L}$
YLabel=E3C/E2C
RatioPlotYMin=0.9
RatioPlotYMax=1.1
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d30-x01-y01
Title=CMS, 13 TeV, $638 < p_\mathrm{T0} < 846$ GeV
XMin=2e-4
XMax=0.4
LogX=1
XLabel=$x_\mathrm{L}$
YLabel=E3C/E2C
RatioPlotYMin=0.9
RatioPlotYMax=1.1
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d31-x01-y01
Title=CMS, 13 TeV, $846 < p_\mathrm{T0} < 1101$ GeV
XMin=2e-4
XMax=0.4
LogX=1
XLabel=$x_\mathrm{L}$
YLabel=E3C/E2C
RatioPlotYMin=0.9
RatioPlotYMax=1.1
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d32-x01-y01
Title=CMS, 13 TeV, $1101 < p_\mathrm{T0} < 1410$ GeV
XMin=2e-4
XMax=0.4
LogX=1
XLabel=$x_\mathrm{L}$
YLabel=E3C/E2C
RatioPlotYMin=0.9
RatioPlotYMax=1.1
# END PLOT

# BEGIN PLOT /CMS_2024_I2760466/d33-x01-y01
Title=CMS, 13 TeV, $1410 < p_\mathrm{T0} < 1784$ GeV
XMin=2e-4
XMax=0.4
LogX=1
XLabel=$x_\mathrm{L}$
YLabel=E3C/E2C
RatioPlotYMin=0.9
RatioPlotYMax=1.1
# END PLOT

