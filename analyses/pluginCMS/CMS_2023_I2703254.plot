BEGIN PLOT /CMS_2023_I2703254/d01-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets
XMinorTickMarks=0
XCustomMajorTicks=1 $\geq7\mathrm{j},\geq4\mathrm{b},\geq3\mathrm{l}$ 2 $\geq6\mathrm{j},\geq4\mathrm{b}$ 3 $\geq6\mathrm{j},\geq3\mathrm{b},\geq3\mathrm{l}$ 4 $\geq5\mathrm{j},\geq3\mathrm{b}$
YLabel= $\sigma_{\mathrm{fid}}$ [fb]
LogY=1
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d11-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $\Delta\mathrm{R}_{\mathrm{b}\mathrm{b}}^{\mathrm{avg}}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} \Delta\mathrm{R}_{\mathrm{b}\mathrm{b}}^{\mathrm{avg}}}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d12-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 5$ jets: $\geq 3 \mathrm{b}$
XLabel= $|\eta(\mathrm{b}_{3})|$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} |\eta(\mathrm{b}_{3})|}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d13-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $|\eta(\mathrm{b}_{3})|$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} |\eta(\mathrm{b}_{3})|}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d14-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 5$ jets: $\geq 3 \mathrm{b}$
XLabel= $p_{\mathrm{T}}(\mathrm{b}_{3})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} p_{\mathrm{T}}(\mathrm{b}_{3})}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d15-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $p_{\mathrm{T}}(\mathrm{b}_{3})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} p_{\mathrm{T}}(\mathrm{b}_{3})}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d16-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $|\eta(\mathrm{b}_{4})|$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} |\eta(\mathrm{b}_{4})|}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d17-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $p_{\mathrm{T}}(\mathrm{b}_{4})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} p_{\mathrm{T}}(\mathrm{b}_{4})}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d18-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 5$ jets: $\geq 3 \mathrm{b}$
XLabel= $H^{\mathrm{b}}_{\mathrm{T}}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} H^{\mathrm{b}}_{\mathrm{T}}}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d19-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $H^{\mathrm{b}}_{\mathrm{T}}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} H^{\mathrm{b}}_{\mathrm{T}}}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d20-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 3 \mathrm{b}$, $\geq 3$ light
XLabel= $|\Delta\phi(\mathrm{lj}^{\mathrm{extra}}_{1},\mathrm{b}_{\mathrm{soft}})|$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} |\Delta\phi(\mathrm{lj}^{\mathrm{extra}}_{1},\mathrm{b}_{\mathrm{soft}})|}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d21-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 7$ jets: $\geq 4 \mathrm{b}$, $\geq 3$ light
XLabel= $|\Delta\phi(\mathrm{lj}^{\mathrm{extra}}_{1},\mathrm{b}_{\mathrm{soft}})|$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} |\Delta\phi(\mathrm{lj}^{\mathrm{extra}}_{1},\mathrm{b}_{\mathrm{soft}})|}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d22-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $|\eta(\mathrm{b}^{\mathrm{extra}}_{1})|$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} |\eta(\mathrm{b}^{\mathrm{extra}}_{1})|}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d23-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $p_{\mathrm{T}}(\mathrm{b}^{\mathrm{extra}}_{1})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} p_{\mathrm{T}}(\mathrm{b}^{\mathrm{extra}}_{1})}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d24-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $|\eta(\mathrm{b}^{\mathrm{extra}}_{2})|$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} |\eta(\mathrm{b}^{\mathrm{extra}}_{2})|}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d25-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $p_{\mathrm{T}}(\mathrm{b}^{\mathrm{extra}}_{2})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} p_{\mathrm{T}}(\mathrm{b}^{\mathrm{extra}}_{2})}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d26-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $|\eta|(\mathrm{b}\mathrm{b}^{\mathrm{extra}})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} |\eta|(\mathrm{b}\mathrm{b}^{\mathrm{extra}})}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d27-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $\Delta\mathrm{R}(\mathrm{b}\mathrm{b}^{\mathrm{extra}})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} \Delta\mathrm{R}(\mathrm{b}\mathrm{b}^{\mathrm{extra}})}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d28-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $\mathrm{m}(\mathrm{b}\mathrm{b}^{\mathrm{extra}})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} \mathrm{m}(\mathrm{b}\mathrm{b}^{\mathrm{extra}})}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d29-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $p_{\mathrm{T}}(\mathrm{b}\mathrm{b}^{\mathrm{extra}})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} p_{\mathrm{T}}(\mathrm{b}\mathrm{b}^{\mathrm{extra}})}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d30-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 3 \mathrm{b}$, $\geq 3$ light
XLabel= $p_{\mathrm{T}}(\mathrm{lj}^{\mathrm{extra}}_{1})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} p_{\mathrm{T}}(\mathrm{lj}^{\mathrm{extra}}_{1})}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d31-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 7$ jets: $\geq 4 \mathrm{b}$, $\geq 3$ light
XLabel= $p_{\mathrm{T}}(\mathrm{lj}^{\mathrm{extra}}_{1})$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} p_{\mathrm{T}}(\mathrm{lj}^{\mathrm{extra}}_{1})}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d32-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 5$ jets: $\geq 3 \mathrm{b}$
XLabel= $H^{\mathrm{j}}_{\mathrm{T}}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} H^{\mathrm{j}}_{\mathrm{T}}}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d33-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $H^{\mathrm{j}}_{\mathrm{T}}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} H^{\mathrm{j}}_{\mathrm{T}}}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d34-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XLabel= $\mathrm{m}_{\mathrm{b}\mathrm{b}}^{\mathrm{max}}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} \mathrm{m}_{\mathrm{b}\mathrm{b}}^{\mathrm{max}}}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d35-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 3 \mathrm{b}$, $\geq 3$ light
XLabel= $H^{\mathrm{light}}_{\mathrm{T}}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} H^{\mathrm{light}}_{\mathrm{T}}}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d36-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 7$ jets: $\geq 4 \mathrm{b}$, $\geq 3$ light
XLabel= $H^{\mathrm{light}}_{\mathrm{T}}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} H^{\mathrm{light}}_{\mathrm{T}}}$ $[GeV^{-1}]$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d37-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 5$ jets: $\geq 3 \mathrm{b}$
XMinorTickMarks=0
XCustomMajorTicks=5 5 6 6 7 7 8 8 9 $\geq9$
XLabel= $N_{\mathrm{jets}}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} N_{\mathrm{jets}}}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d38-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 6$ jets: $\geq 4 \mathrm{b}$
XMinorTickMarks=0
XCustomMajorTicks=6 6 7 7 8 8 9 $\geq9$
XLabel= $N_{\mathrm{jets}}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} N_{\mathrm{jets}}}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2023_I2703254/d39-x01-y01 
Title=CMS, 13 TeV, $t\bar{t}b\bar{b}$ lepton + jets, $\geq 5$ jets: $\geq 3 \mathrm{b}$
XMinorTickMarks=0
XCustomMajorTicks=3 3 4 $\geq4$
XLabel= $N_{\mathrm{b}}^{M}$
YLabel= $\frac{1}{\sigma_{tot}} \frac{\mathrm{d} \sigma}{\mathrm{d} N_{\mathrm{b}}^{M}}$
LogY=0
END PLOT

