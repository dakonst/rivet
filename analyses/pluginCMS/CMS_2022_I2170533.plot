BEGIN PLOT /CMS_2022_I2170533/d01-x01-y01
Title=CMS, 13 TeV, $200 < p_{T1} <  400$ GeV, $0^{\circ} < \Delta \phi_{1,2} < 150^{\circ} $
XLabel=$N_{jet}$
YLabel=$d\sigma/d N_{jet} [pb]$
YMin=1.2
YMax=1E05
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d02-x01-y01
Title=CMS, 13 TeV, $200 < p_{T1} <  400$ GeV, $150^{\circ} < \Delta \phi_{1,2} < 170^{\circ} $
XLabel=$N_{jet}$
YLabel=$d\sigma/d N_{jet} [pb]$
YMin=1.2
YMax=1E05
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d03-x01-y01
Title=CMS, 13 TeV, $200 < p_{T1} <  400$ GeV, $170^{\circ} < \Delta \phi_{1,2} < 180^{\circ} $
XLabel=$N_{jet}$
YLabel=$d\sigma/d N_{jet} [pb]$
YMin=1.2
YMax=1E05
Label=$d\sigma/d N_{jet} [pb]$
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d04-x01-y01
Title=CMS, 13 TeV, $400 < p_{T1} < 800$ GeV, $0^{\circ} < \Delta \phi_{1,2} < 150^{\circ} $
XLabel=$N_{jet}$
YLabel=$d\sigma/d N_{jet} [pb]$
YMin=0.2
YMax=2E04
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d05-x01-y01
Title=CMS, 13 TeV, $400 < p_{T1} < 800$ GeV, $150^{\circ} < \Delta \phi_{1,2} < 170^{\circ} $
XLabel=$N_{jet}$
YLabel=$d\sigma/d N_{jet} [pb]$
YMin=0.2
YMax=2E04
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d06-x01-y01
Title=CMS, 13 TeV, $400 < p_{T1} \leq 800$ GeV, $170^{\circ} < \Delta \phi_{1,2} < 180^{\circ} $
XLabel=$N_{jet}$
YLabel=$d\sigma/d N_{jet} [pb]$
YMin=0.2
YMax=2E04
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d07-x01-y01
Title=CMS, 13 TeV, $p_{T1} > 800$ GeV, $0^{\circ} < \Delta \phi_{1,2} < 150^{\circ} $
XLabel=$N_{jet}$
YLabel=$d\sigma/d N_{jet} [pb]$
YMin=0.0002
YMax=2E02
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d08-x01-y01
Title=CMS, 13 TeV, $p_{T1} > 800$ GeV, $150^{\circ} < \Delta \phi_{1,2} < 170^{\circ} $
XLabel=$N_{jet}$
YLabel=$d\sigma/d N_{jet} [pb]$
YMin=0.02
YMax=2E02
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d09-x01-y01
Title=CMS, 13 TeV, $p_{T1} > 800$ GeV, $170^{\circ} < \Delta \phi_{1,2} < 180^{\circ} $
XLabel=$N_{jet}$
YLabel=$d\sigma/d N_{jet} [pb]$
YMin=0.02
YMax=2E02
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d10-x01-y01
Title=CMS, 13 TeV, $1^{st}$ leading jet,  $N_{jet} \geq 2$
XLabel=$p_{T1} [GeV]$
YLabel=$d\sigma/d p_{T1} [pb/GeV]$
XMin=200
YMin=2E-05
YMax=1E05
LogX=1
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d11-x01-y01
Title=CMS, 13 TeV, $2^{nd}$ leading jet, $N_{jet} \geq 2$
XLabel=$p_{T2} [GeV]$
YLabel=$d\sigma/d p_{T2} [pb/GeV]$
XMin=100
YMin=2E-05
YMax=1E05
LogX=1
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d12-x01-y01
Title=CMS, 13 TeV, $3^{rd}$ leading jet,  $N_{jet} \geq 3$
XLabel=$p_{T3} [GeV]$
YLabel=$d\sigma/d p_{T3} [pb/GeV]$
XMin=50
XMax=970
YMin=2E-05
YMax=1E05
LogX=1
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT

BEGIN PLOT /CMS_2022_I2170533/d13-x01-y01
Title=CMS, 13 TeV, $4^{th}$ leading jet,  $N_{jet} \geq 4$
XLabel=$p_{T4} [GeV]$
YLabel=$d\sigma/d p_{T4} [pb/GeV]$
XMin=50
XMax=640
YMin=2E-05
YMax=1E05
LogX=1
RatioPlotYMin=0.2
RatioPlotYMax=1.8
END PLOT
