BEGIN PLOT /CMS_2017_I1485195/d05-x01-y03
Title=Cross section for $B^+$ production ($p_\perp<17$\,GeV $|y|<1.45$ $p_\perp>17$\,GeV $|y|<2.1$)
YLabel=$\text{d}\sigma/\text{d}p_\perp$   [$\mu$b/GeV]
XLabel=$p_\perp$ [GeV]
END PLOT
BEGIN PLOT /CMS_2017_I1485195/d06-x01-y03
Title=Cross section for $B^+$ production ($10<p_\perp>100$\,GeV, $|y|<1.45$, ($17<p_\perp>100$\,GeV, $1.45<|y|<2.1$)
YLabel=$\text{d}\sigma/\text{d}y$
XLabel=$y$
LogY=0
END PLOT
