# BEGIN PLOT /CMS_2018_I1653948/d01-x01-y01
Title=CMS, $13$ TeV, proton-proton inelastic cross section
XCustomMajorTicks=1	$\xi>10^{-6}$	2	$\xi_{X}>10^{-7}\;\text{or}\;\xi_{Y}>10^{-6}$
YLabel=$\sigma$ [mb]
LogY=0
# END PLOT
