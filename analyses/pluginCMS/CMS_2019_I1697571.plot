BEGIN PLOT /CMS_2019_I1697571/d01-x01-y01
Title=$B^0_s$ differential cross section ($|y|<2.4$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2019_I1697571/d02-x01-y01
Title=$B^0_s$ differential cross section ($|y|<2.4$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [pb/GeV]
END PLOT
