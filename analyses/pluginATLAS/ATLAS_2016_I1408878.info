Name: ATLAS_2016_I1408878
Year: 2016
Summary: $D^{*\pm}$, $D^\pm$ and $D^\pm_s$ production at 7 TeV
Experiment: ATLAS
Collider: LHC
InspireID: 1408878
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Nucl.Phys.B 907 (2016) 717-763, 2016. 
 - ATLAS-BPHY-2013-04
 - arXiv:1512.02913 [hep-ex]
RunInfo: charm meson production
Beams: [p+, p+]
Energies: [7000]
Description:
  'Measurement of the production of $D^{*\pm}$, $D^\pm$ and $D^\pm_s$ production at 7 TeV by the ATLAS collaboration.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: ATLAS:2015igt
BibTeX: '@article{ATLAS:2015igt,
    author = "Aad, Georges and others",
    collaboration = "ATLAS",
    title = "{Measurement of $D^{*\pm}$, $D^\pm$ and $D_s^\pm$ meson production cross sections in $pp$ collisions at $\sqrt{s}=7$ TeV with the ATLAS detector}",
    eprint = "1512.02913",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-PH-EP-2015-288",
    doi = "10.1016/j.nuclphysb.2016.04.032",
    journal = "Nucl. Phys. B",
    volume = "907",
    pages = "717--763",
    year = "2016"
}
'
