BEGIN PLOT /ATLAS_2014_I1292798/d01-x01-y01
Title=Prompt $\chi_{c1}\to\gamma J/\psi$ production as function of J/$\psi$ $p_\perp$
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel =$\mathrm{Br}(\chi_{c1}\to\gamma J/\psi)\mathrm{Br}(J/\psi\to\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [pb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d02-x01-y01
Title=Prompt $\chi_{c2}\to\gamma J/\psi$ production as function of J/$\psi$ $p_\perp$
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel =$\mathrm{Br}(\chi_{c2}\to\gamma J/\psi)\mathrm{Br}(J/\psi\to\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [pb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d03-x01-y01
Title=Non-prompt $\chi_{c1}\to\gamma J/\psi$ production as function of J/$\psi$ $p_\perp$
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel =$\mathrm{Br}(\chi_{c1}\to\gamma J/\psi)\mathrm{Br}(J/\psi\to\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [pb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d04-x01-y01
Title=Non-prompt $\chi_{c2}\to\gamma J/\psi$ production as function of J/$\psi$ $p_\perp$
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel =$\mathrm{Br}(\chi_{c2}\to\gamma J/\psi)\mathrm{Br}(J/\psi\to\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [pb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d05-x01-y01
Title=Prompt $\chi_{c1}\to\gamma J/\psi$ production as function of $\chi_{c1}$ $p_\perp$
XLabel=$p_\perp^{\chi_{c1}}$ [GeV]
YLabel =$\mathrm{Br}(\chi_{c1}\to\gamma J/\psi)\mathrm{Br}(J/\psi\to\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [pb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d06-x01-y01
Title=Prompt $\chi_{c2}\to\gamma J/\psi$ production as function of $\chi_{c2}$ $p_\perp$
XLabel=$p_\perp^{\chi_{c2}}$ [GeV]
YLabel =$\mathrm{Br}(\chi_{c2}\to\gamma J/\psi)\mathrm{Br}(J/\psi\to\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [pb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d07-x01-y01
Title=Non-prompt $\chi_{c1}\to\gamma J/\psi$ production as function of $\chi_{c1}$ $p_\perp$
XLabel=$p_\perp^{\chi_{c1}}$ [GeV]
YLabel =$\mathrm{Br}(\chi_{c1}\to\gamma J/\psi)\mathrm{Br}(J/\psi\to\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [pb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d08-x01-y01
Title=Non-prompt $\chi_{c2}\to\gamma J/\psi$ production as function of $\chi_{c2}$ $p_\perp$
XLabel=$p_\perp^{\chi_{c2}}$ [GeV]
YLabel =$\mathrm{Br}(\chi_{c2}\to\gamma J/\psi)\mathrm{Br}(J/\psi\to\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [pb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d09-x01-y01
Title=Fraction of prompt J/$\psi$ from $\chi_{c(1,2)}\to\gamma J/\psi$ as function of J/$\psi$ $p_\perp$
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{J/\psi}$
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d10-x01-y01
Title=Relative prompt $\chi_{c2}$ vs $\chi_{c1}$ production rate vs J/$\psi$ $p_\perp$
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel=$N_{\chi_{c2}} /N_{\chi_{c1}}$
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d11-x01-y01
Title=Relative non-prompt $\chi_{c2}$ vs $\chi_{c1}$ production rate vs J/$\psi$ $p_\perp$
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel=$N_{\chi_{c2}} /N_{\chi_{c1}}$
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d12-x01-y01
Title=Non-prompt $\chi_{c1}$  fraction
XLabel=$p_\perp^{\chi_{c1}}$ [GeV]
YLabel=$N_{\mathrm{prompt}}/N$
END PLOT

BEGIN PLOT /ATLAS_2014_I1292798/d13-x01-y01
Title=Non-prompt $\chi_{c2}$  fraction
XLabel=$p_\perp^{\chi_{c2}}$ [GeV]
YLabel=$N_{\mathrm{prompt}}/N$
END PLOT
