BEGIN PLOT /ATLAS_2013_I1240670/d01-x01-y01
Title=Cross section for $B^+$ production ($|y|<0.5$)
YLabel=$\frac{\text{d}^2\sigma}{\text{d}p_\perp\text{d}y}\times\text{Br}(B^+\to J/\psi(\mu^+\mu^-)K^+)$   [pb/GeV]
XLabel=$p_\perp$ [GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1240670/d02-x01-y01
Title=Cross section for $B^+$ production ($0.5<|y|<1.0$)
YLabel=$\frac{\text{d}^2\sigma}{\text{d}p_\perp\text{d}y}\times\text{Br}(B^+\to J/\psi(\mu^+\mu^-)K^+)$   [pb/GeV]
XLabel=$p_\perp$ [GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1240670/d03-x01-y01
Title=Cross section for $B^+$ production ($1.0<|y|<1.5$)
YLabel=$\frac{\text{d}^2\sigma}{\text{d}p_\perp\text{d}y}\times\text{Br}(B^+\to J/\psi(\mu^+\mu^-)K^+)$   [pb/GeV]
XLabel=$p_\perp$ [GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1240670/d04-x01-y01
Title=Cross section for $B^+$ production ($1.5<|y|<2.25$)
YLabel=$\frac{\text{d}^2\sigma}{\text{d}p_\perp\text{d}y}\times\text{Br}(B^+\to J/\psi(\mu^+\mu^-)K^+)$   [pb/GeV]
XLabel=$p_\perp$ [GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1240670/d05-x01-y01
Title=Cross section for $B^+$ production ($|y|<2.25$)
YLabel=$\frac{\text{d}\sigma}{\text{d}p_\perp}\times\text{Br}(B^+\to J/\psi(\mu^+\mu^-)K^+)$   [pb/GeV]
XLabel=$p_\perp$ [GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1240670/d06-x01-y01
Title=Cross section for $B^+$ production ($9<p_\perp>120$\,GeV)
YLabel=$\frac{\text{d}\sigma}{\text{d}y}\times\text{Br}(B^+\to J/\psi(\mu^+\mu^-)K^+)$ [pb]
XLabel=$y$
LogY=0
END PLOT
