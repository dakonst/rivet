# BEGIN PLOT /ATLAS_2022_I2077575/*
XTwosidedTicks=1
YTwosidedTicks=1
LogY=1
RatioPlotYMin=0.75
RatioPlotYMax=1.25
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d02-x01-y01
XLabel=$\sigma_\mathrm{fiducial}$
YLabel=Cross section [fb]
Title=Particle-level fiducial phase-space total cross section
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d03-x01-y01
XLabel=$p_{\mathrm{T}}^{t} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d04-x01-y01
XLabel=$|y^{t}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|y^{t}| \ [\mathrm{fb}]$
Title=Particle-level top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d05-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d06-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|y^{t,1}| \ [\mathrm{fb}]$
Title=Particle-level leading top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d07-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level subleading top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d08-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|y^{t,2}| \ [\mathrm{fb}]$
Title=Particle-level subleading top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d09-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d10-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d11-x01-y01
XLabel=$|y^{t\bar{t}}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|y^{t\bar{t}}| \ [\mathrm{fb}]$
Title=Particle-level $t\bar{t}$ rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d12-x01-y01
XLabel=$\chi^{t\bar{t}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \chi^{t\bar{t}} \ [\mathrm{fb}]$
Title=Particle-level $t\bar{t}$ $\chi$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d13-x01-y01
XLabel=$y_{B}^{t\bar{t}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} y_{B}^{t\bar{t}} \ [\mathrm{fb}]$
Title=Particle-level $t\bar{t}$ longitudinal boost
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d14-x01-y01
XLabel=$|p_{\mathrm{out}}^{t\bar{t}}| \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d}| p_{\mathrm{out}}^{t\bar{t}}| \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ out-of-plane momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d15-x01-y01
XLabel=$\Delta\phi(t_{1},t_{2})$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \Delta\phi(t_{1},t_{2}) \ [\mathrm{fb}]$
Title=Particle-level $t\bar{t}$ azimuthal opening angle
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d16-x01-y01
XLabel=$H_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d} H_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$  $H_{\mathrm{T}}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d17-x01-y01
XLabel=$|\cos{\theta^\ast}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\cos{\theta^\ast}| \ [\mathrm{fb}]$
Title=Particle-level $t\bar{t}$ $\cos{\theta^\ast}$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d18-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d19-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.55,0.6] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d20-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.6,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d21-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d22-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}| \ [\mathrm{fb}]$
Title=Particle-level subleading top rapidity, $|y^{t,1}| \in [0,0.2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d23-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}| \ [\mathrm{fb}]$
Title=Particle-level subleading top rapidity, $|y^{t,1}| \in [0.2,0.5]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d24-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}| \ [\mathrm{fb}]$
Title=Particle-level subleading top rapidity, $|y^{t,1}| \in [0.5,1]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d25-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}| \ [\mathrm{fb}]$
Title=Particle-level subleading top rapidity, $|y^{t,1}| \in [1,2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d26-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t,1}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d27-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t,1}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d28-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t,1}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d29-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t,1}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d30-x01-y01
XLabel==$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level subleading top transverse momentum, $|y^{t,2}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d31-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level subleading top transverse momentum, $|y^{t,2}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d32-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level subleading top transverse momentum, $|y^{t,2}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d33-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level subleading top transverse momentum, $|y^{t,2}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d34-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d35-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.55,0.625] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d36-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.625,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d37-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d38-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d39-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.55,0.625] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d40-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.625,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d41-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d42-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d43-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d44-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d45-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d46-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}| \ [\mathrm{fb}]$
Title=Particle-level leading top rapidity, $|y^{t\bar{t}}| \in [0,0.2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d47-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}| \ [\mathrm{fb}]$
Title=Particle-level leading top rapidity, $|y^{t\bar{t}}| \in [0.2,0.5]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d48-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}| \ [\mathrm{fb}]$
Title=Particle-level leading top rapidity, $|y^{t\bar{t}}| \in [0.5,1]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d49-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}| \ [\mathrm{fb}]$
Title=Particle-level leading top rapidity, $|y^{t\bar{t}}| \in [1,2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d50-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d51-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d52-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d53-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d54-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d55-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d56-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d57-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d58-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0,0.1] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d59-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.1,0.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d60-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.2,0.35] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d61-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.35,1] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d62-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d63-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d64-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d65-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d66-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d67-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d68-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d69-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d70-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d71-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d72-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d73-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d74-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d75-x01-y01
XLabel=$p_{\mathrm{T}}^{t} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d76-x01-y01
XLabel=$|y^{t}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}|y^{t}|$
Title=Particle-level top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d77-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d78-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}|y^{t,1}|$
Title=Particle-level leading top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d79-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level subleading top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d80-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}|y^{t,2}|$
Title=Particle-level subleading top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d81-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d82-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d83-x01-y01
XLabel=$|y^{t\bar{t}}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}|y^{t\bar{t}}|$
Title=Particle-level $t\bar{t}$ rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d84-x01-y01
XLabel=$\chi^{t\bar{t}}$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} \chi^{t\bar{t}}$
Title=Particle-level $t\bar{t}$ $\chi$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d85-x01-y01
XLabel=$y_{B}^{t\bar{t}}$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} y_{B}^{t\bar{t}}$
Title=Particle-level $t\bar{t}$ longitudinal boost
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d86-x01-y01
XLabel=$|p_{\mathrm{out}}^{t\bar{t}}| \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}|p_{\mathrm{out}}^{t\bar{t}}| \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ out-of-plane momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d87-x01-y01
XLabel=$\Delta\phi(t_{1},t_{2})$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} \Delta\phi(t_{1},t_{2})$
Title=Particle-level $t\bar{t}$ azimuthal opening angle
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d88-x01-y01
XLabel=$H_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} H_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$  $H_{\mathrm{T}}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d89-x01-y01
XLabel=$|\cos{\theta^\ast}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma /\mathrm{d}|\cos{\theta^\ast}|$
Title=Particle-level $t\bar{t}$ $\cos{\theta^\ast}$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d90-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d91-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.55,0.6] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d92-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.6,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d93-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d94-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}|$
Title=Particle-level subleading top rapidity, $|y^{t,1}| \in [0,0.2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d95-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}|$
Title=Particle-level subleading top rapidity, $|y^{t,1}| \in [0.2,0.5]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d96-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}|$
Title=Particle-level subleading top rapidity, $|y^{t,1}| \in [0.5,1]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d97-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}|$
Title=Particle-level subleading top rapidity, $|y^{t,1}| \in [1,2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d98-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t,1}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d99-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t,1}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d100-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t,1}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d101-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t,1}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d102-x01-y01
XLabel==$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level subleading top transverse momentum, $|y^{t,2}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d103-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level subleading top transverse momentum, $|y^{t,2}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d104-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level subleading top transverse momentum, $|y^{t,2}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d105-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level subleading top transverse momentum, $|y^{t,2}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d106-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d107-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.55,0.625] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d108-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.625,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d109-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d110-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d111-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.55,0.625] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d112-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.625,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d113-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d114-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d115-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d116-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d117-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d118-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}|$
Title=Particle-level leading top rapidity, $|y^{t\bar{t}}| \in [0,0.2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d119-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}|$
Title=Particle-level leading top rapidity, $|y^{t\bar{t}}| \in [0.2,0.5]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d120-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}|$
Title=Particle-level leading top rapidity, $|y^{t\bar{t}}| \in [0.5,1]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d121-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}|$
Title=Particle-level leading top rapidity, $|y^{t\bar{t}}| \in [1,2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d122-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d123-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d124-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d125-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d126-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d127-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d128-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d129-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d130-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0,0.1] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d131-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.1,0.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d132-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.2,0.35] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d133-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.35,1] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d134-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d135-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d136-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d137-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Particle-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d138-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d139-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d140-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d141-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d142-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d143-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d144-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d145-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d146-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Particle-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d147-x01-y01
XLabel=$\sigma_\mathrm{fiducial}$
YLabel=Cross section [fb]
Title=Parton-level fiducial phase-space total cross section
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d148-x01-y01
XLabel=$p_{\mathrm{T}}^{t} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d149-x01-y01
XLabel=$|y^{t}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|y^{t}| \ [\mathrm{fb}]$
Title=Parton-level top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d150-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d151-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|y^{t,1}| \ [\mathrm{fb}]$
Title=Parton-level leading top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d152-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level subleading top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d153-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|y^{t,2}| \ [\mathrm{fb}]$
Title=Parton-level subleading top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d154-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d155-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d156-x01-y01
XLabel=$|y^{t\bar{t}}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|y^{t\bar{t}}| \ [\mathrm{fb}]$
Title=Parton-level $t\bar{t}$ rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d157-x01-y01
XLabel=$\chi^{t\bar{t}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \chi^{t\bar{t}} \ [\mathrm{fb}]$
Title=Parton-level $t\bar{t}$ $\chi$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d158-x01-y01
XLabel=$y_{B}^{t\bar{t}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} y_{B}^{t\bar{t}} \ [\mathrm{fb}]$
Title=Parton-level $t\bar{t}$ longitudinal boost
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d159-x01-y01
XLabel=$|p_{\mathrm{out}}^{t\bar{t}}| \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d}| p_{\mathrm{out}}^{t\bar{t}}| \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ out-of-plane momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d160-x01-y01
XLabel=$\Delta\phi(t_{1},t_{2})$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \Delta\phi(t_{1},t_{2}) \ [\mathrm{fb}]$
Title=Parton-level $t\bar{t}$ azimuthal opening angle
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d161-x01-y01
XLabel=$H_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}\sigma / \mathrm{d} H_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$  $H_{\mathrm{T}}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d162-x01-y01
XLabel=$|\cos{\theta^\ast}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\cos{\theta^\ast}| \ [\mathrm{fb}]$
Title=Parton-level $t\bar{t}$ $\cos{\theta^\ast}$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d163-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d164-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.55,0.6] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d165-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.6,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d166-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d167-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}| \ [\mathrm{fb}]$
Title=Parton-level subleading top rapidity, $|y^{t,1}| \in [0,0.2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d168-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}| \ [\mathrm{fb}]$
Title=Parton-level subleading top rapidity, $|y^{t,1}| \in [0.2,0.5]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d169-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}| \ [\mathrm{fb}]$
Title=Parton-level subleading top rapidity, $|y^{t,1}| \in [0.5,1]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d170-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}| \ [\mathrm{fb}]$
Title=Parton-level subleading top rapidity, $|y^{t,1}| \in [1,2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d171-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t,1}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d172-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t,1}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d173-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t,1}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d174-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t,1}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d175-x01-y01
XLabel==$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level subleading top transverse momentum, $|y^{t,2}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d176-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level subleading top transverse momentum, $|y^{t,2}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d177-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level subleading top transverse momentum, $|y^{t,2}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d178-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level subleading top transverse momentum, $|y^{t,2}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d179-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d180-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.55,0.625] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d181-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.625,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d182-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d183-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d184-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.55,0.625] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d185-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.625,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d186-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d187-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d188-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d189-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d190-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d191-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}| \ [\mathrm{fb}]$
Title=Parton-level leading top rapidity, $|y^{t\bar{t}}| \in [0,0.2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d192-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}| \ [\mathrm{fb}]$
Title=Parton-level leading top rapidity, $|y^{t\bar{t}}| \in [0.2,0.5]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d193-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}| \ [\mathrm{fb}]$
Title=Parton-level leading top rapidity, $|y^{t\bar{t}}| \in [0.5,1]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d194-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}| \ [\mathrm{fb}]$
Title=Parton-level leading top rapidity, $|y^{t\bar{t}}| \in [1,2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d195-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d196-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d197-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d198-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d199-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d200-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d201-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d202-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d203-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0,0.1] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d204-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.1,0.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d205-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.2,0.35] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d206-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.35,1] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d207-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d208-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d209-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d210-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{fb} \ \mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d211-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d212-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d213-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d214-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d215-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d216-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d217-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d218-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d219-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$\mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{fb} \ \mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d220-x01-y01
XLabel=$p_{\mathrm{T}}^{t} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d221-x01-y01
XLabel=$|y^{t}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}|y^{t}|$
Title=Parton-level top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d222-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d223-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}|y^{t,1}|$
Title=Parton-level leading top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d224-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level subleading top transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d225-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}|y^{t,2}|$
Title=Parton-level subleading top rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d226-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d227-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ transverse momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d228-x01-y01
XLabel=$|y^{t\bar{t}}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}|y^{t\bar{t}}|$
Title=Parton-level $t\bar{t}$ rapidity
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d229-x01-y01
XLabel=$\chi^{t\bar{t}}$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} \chi^{t\bar{t}}$
Title=Parton-level $t\bar{t}$ $\chi$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d230-x01-y01
XLabel=$y_{B}^{t\bar{t}}$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} y_{B}^{t\bar{t}}$
Title=Parton-level $t\bar{t}$ longitudinal boost
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d231-x01-y01
XLabel=$|p_{\mathrm{out}}^{t\bar{t}}| \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}| p_{\mathrm{out}}^{t\bar{t}}| \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ out-of-plane momentum
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d232-x01-y01
XLabel=$\Delta\phi(t_{1},t_{2})$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} \Delta\phi(t_{1},t_{2})$
Title=Parton-level $t\bar{t}$ azimuthal opening angle
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d233-x01-y01
XLabel=$H_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d} H_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$  $H_{\mathrm{T}}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d234-x01-y01
XLabel=$|\cos{\theta^\ast}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}\sigma / \mathrm{d}|\cos{\theta^\ast}|$
Title=Parton-level $t\bar{t}$ $\cos{\theta^\ast}$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d235-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d236-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.55,0.6] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d237-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.6,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d238-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level subleading top transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d239-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}|$
Title=Parton-level subleading top rapidity, $|y^{t,1}| \in [0,0.2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d240-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}|$
Title=Parton-level subleading top rapidity, $|y^{t,1}| \in [0.2,0.5]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d241-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}|$
Title=Parton-level subleading top rapidity, $|y^{t,1}| \in [0.5,1]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d242-x01-y01
XLabel=$|y^{t,2}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}|y^{t,2}|$
Title=Parton-level subleading top rapidity, $|y^{t,1}| \in [1,2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d243-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t,1}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d244-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t,1}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d245-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t,1}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d246-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t,1}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d247-x01-y01
XLabel==$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level subleading top transverse momentum, $|y^{t,2}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d248-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level subleading top transverse momentum, $|y^{t,2}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d249-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level subleading top transverse momentum, $|y^{t,2}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d250-x01-y01
XLabel=$p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,2}| \mathrm{d}p_{\mathrm{T}}^{t,2} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level subleading top transverse momentum, $|y^{t,2}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d251-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d252-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.55,0.625] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d253-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.625,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d254-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d255-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.5,0.55] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d256-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.55,0.625] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d257-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.625,0.75] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d258-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t,1} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t,1} \in [0.75,2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d259-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d260-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d261-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d262-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d263-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}|$
Title=Parton-level leading top rapidity, $|y^{t\bar{t}}| \in [0,0.2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d264-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}|$
Title=Parton-level leading top rapidity, $|y^{t\bar{t}}| \in [0.2,0.5]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d265-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}|$
Title=Parton-level leading top rapidity, $|y^{t\bar{t}}| \in [0.5,1]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d266-x01-y01
XLabel=$|y^{t,1}|$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}|y^{t,1}|$
Title=Parton-level leading top rapidity, $|y^{t\bar{t}}| \in [1,2]$
LogY=0
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d267-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d268-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d269-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d270-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t,1}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t,1}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d271-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d272-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d273-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d274-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ invariant mass, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d275-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0,0.1] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d276-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.1,0.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d277-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.2,0.35] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d278-x01-y01
XLabel=$m^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \mathrm{d}m^{t\bar{t}} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level $t\bar{t}$ invariant mass, $p_{\mathrm{T}}^{t\bar{t}} \in [0.35,1] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d279-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0,0.2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d280-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0.2,0.5]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d281-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [0.5,1]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d282-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^2\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}p_{\mathrm{T}}^{t\bar{t}} \ [\mathrm{TeV}^{-1}]$
Title=Parton-level $t\bar{t}$ transverse momentum, $|y^{t\bar{t}}| \in [1,2]$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d283-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d284-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d285-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0,0.3]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d286-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d287-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d288-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.3,0.9]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d289-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [0.9,1.2] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d290-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [1.2,1.5] \ \mathrm{TeV}$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2077575/d291-x01-y01
XLabel=$p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}]$
YLabel=$1 / \sigma \ \cdot \ \mathrm{d}^3\sigma / \mathrm{d}|y^{t\bar{t}}| \mathrm{d}m^{t\bar{t}} \mathrm{d}p_{\mathrm{T}}^{t,1} \ [\mathrm{TeV}^{-2}]$
Title=Parton-level leading top transverse momentum, $|y^{t\bar{t}}| \in [0.9,2]$ and $m^{t\bar{t}} \in [1.5,4] \ \mathrm{TeV}$
# END PLOT
