Name: ATLAS_2015_I1351916
Year: 2015
Summary: $Z$ forward-backward asymmetry
Experiment: ATLAS
Collider: LHC
InspireID: 1351916
Status: VALIDATED
Reentrant: true
Authors:
 - Christian Gutschow <chris.g@cern.ch>
References:
 - ATLAS-STDM-2011-34
 - arXiv:1503.03709 [hep-ex]
 - JHEP 09 (2015) 049
 - doi:10.1007/JHEP09(2015)049
RunInfo:
  Inclusive $Z$ in the electron channel
Beams: [p+, p+]
Energies: [7000]
PtCuts: [25,25]
NeedCrossSection: True
Options:
 - LMODE=EL,MU
Description:
  'This paper presents measurements from the ATLAS experiment of the forward-backward
  asymmetry in the reaction $pp \to Z/\gamma^\ast \to \ell^+ \ell^-$, with $\ell$ being electrons or muons,
  and the extraction of the effective weak mixing angle. The results are based on the full set
  of data collected in 2011 in pp collisions at the LHC at $\sqrt{s}$ = 7 TeV, corresponding
  to an integrated luminosity of 4.8 fb$^{-1}$. The measured asymmetry values are found to
  be in agreement with the corresponding Standard Model predictions. The combination
  of the muon and electron channels yields a value of the effective weak mixing angle of
  $\sin^2\theta^\text{lept}_\text{eff}$ = 0.2308$\pm$0.0005(stat.)$\pm$0.0006(syst.)$\pm$0.0009(PDF),
  where the first uncertainty corresponds to data statistics, the second to systematic effects
  and the third to knowledge of the parton density functions.
  This result agrees with the current world average from the Particle Data Group fit.'
BibKey: Aad:2015uau
BibTeX: '@article{Aad:2015uau,
      author         = "Aad, Georges and others",
      title          = "{Measurement of the forward-backward asymmetry of
                        electron and muon pair-production in $pp$ collisions at
                        $\sqrt{s} = 7$~TeV with the ATLAS detector}",
      collaboration  = "ATLAS",
      year           = "2015",
      eprint         = "1503.03709",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "CERN-PH-EP-2014-259",
      SLACcitation   = "%%CITATION = ARXIV:1503.03709;%%"
}'
ReleaseTests:
 - $A LHC-Z-e :LMODE=EL
 - $A-2 LHC-Z-mu :LMODE=MU
