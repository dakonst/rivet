# BEGIN PLOT /ATLAS_2015_I1390114/d..
XTwosidedTicks=1
YTwosidedTicks=1
XLabel=
#RatioPlotYMax=2.1
#RatioPlotYMin=0.1
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1390114/d01
XCustomMajorTicks=1.0	$\sigma_\mathrm{ttb(lepton+jets)}$	2.0	$\sigma_{e\mu}$	3.0	$\sigma_{ttbb\mathrm{(cut-based)}}$	4.0	$\sigma_{ttbb\mathrm{(fit-based)}}$
YLabel=Cross section [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2015_I1390114/d02
LogY=0
XCustomMajorTicks=1.0	$R_{ttbb}$ 
YLabel=Cross section ratio [$\%$]
# END PLOT
