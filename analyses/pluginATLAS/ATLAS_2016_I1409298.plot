BEGIN PLOT /ATLAS_2016_I1409298/d0[1,2,3,4]
XLabel=$p_\perp$ [GeV]
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d0[5,6,7,8]
XLabel=$p_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d[09,10,11,12]
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction/\%
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d[13,14,15,16]
XLabel=$p_\perp$ [GeV]
YLabel=$\psi(2S)/J/\psi$ /\%
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d01-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 7 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d01-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 7 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d01-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 7 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d01-x01-y04                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 7 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d01-x01-y05                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 7 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d01-x01-y06                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 7 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d01-x01-y07                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 7 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d01-x01-y08
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 7 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d02-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d02-x01-y04                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d02-x01-y05                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d02-x01-y06                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d02-x01-y07                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d02-x01-y08
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ at 8 TeV ($1.75<y<2.00$) 
END PLOT


BEGIN PLOT /ATLAS_2016_I1409298/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 7 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d03-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 7 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d03-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 7 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d03-x01-y04                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 7 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d03-x01-y05                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 7 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d03-x01-y06                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 7 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d03-x01-y07                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 7 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d03-x01-y08
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 7 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d04-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d04-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d04-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d04-x01-y04                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d04-x01-y05                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d04-x01-y06                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d04-x01-y07                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d04-x01-y08
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ at 8 TeV ($1.75<y<2.00$) 
END PLOT


BEGIN PLOT /ATLAS_2016_I1409298/d05-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 7 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d05-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 7 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d05-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 7 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d05-x01-y04                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 7 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d05-x01-y05                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 7 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d05-x01-y06                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 7 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d05-x01-y07                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 7 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d05-x01-y08
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 7 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d06-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 8 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d06-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 8 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d06-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 8 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d06-x01-y04                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 8 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d06-x01-y05                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 8 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d06-x01-y06                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 8 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d06-x01-y07                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 8 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d06-x01-y08
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ at 8 TeV ($1.75<y<2.00$) 
END PLOT


BEGIN PLOT /ATLAS_2016_I1409298/d07-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 7 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d07-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 7 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d07-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 7 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d07-x01-y04                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 7 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d07-x01-y05                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 7 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d07-x01-y06                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 7 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d07-x01-y07                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 7 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d07-x01-y08
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 7 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d08-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 8 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d08-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 8 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d08-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 8 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d08-x01-y04                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 8 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d08-x01-y05                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 8 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d08-x01-y06                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 8 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d08-x01-y07                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 8 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d08-x01-y08
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ at 8 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d09-x01-y01
Title=Non-prompt fraction for J/$\psi$ at 7 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d09-x01-y02
Title=Non-prompt fraction for J/$\psi$ at 7 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d09-x01-y03                                                    
Title=Non-prompt fraction for J/$\psi$ at 7 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d09-x01-y04                                                    
Title=Non-prompt fraction for J/$\psi$ at 7 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d09-x01-y05                                                    
Title=Non-prompt fraction for J/$\psi$ at 7 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d09-x01-y06                                                    
Title=Non-prompt fraction for J/$\psi$ at 7 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d09-x01-y07                                                    
Title=Non-prompt fraction for J/$\psi$ at 7 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d09-x01-y08
Title=Non-prompt fraction for J/$\psi$ at 7 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d10-x01-y01
Title=Non-prompt fraction for J/$\psi$ at 8 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d10-x01-y02
Title=Non-prompt fraction for J/$\psi$ at 8 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d10-x01-y03                                                    
Title=Non-prompt fraction for J/$\psi$ at 8 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d10-x01-y04                                                    
Title=Non-prompt fraction for J/$\psi$ at 8 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d10-x01-y05                                                    
Title=Non-prompt fraction for J/$\psi$ at 8 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d10-x01-y06                                                    
Title=Non-prompt fraction for J/$\psi$ at 8 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d10-x01-y07                                                    
Title=Non-prompt fraction for J/$\psi$ at 8 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d10-x01-y08
Title=Non-prompt fraction for J/$\psi$ at 8 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d11-x01-y01
Title=Non-prompt fraction for $\psi(2S)$ at 7 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d11-x01-y02
Title=Non-prompt fraction for $\psi(2S)$ at 7 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d11-x01-y03                                                    
Title=Non-prompt fraction for $\psi(2S)$ at 7 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d11-x01-y04                                                    
Title=Non-prompt fraction for $\psi(2S)$ at 7 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d11-x01-y05                                                    
Title=Non-prompt fraction for $\psi(2S)$ at 7 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d11-x01-y06                                                    
Title=Non-prompt fraction for $\psi(2S)$ at 7 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d11-x01-y07                                                    
Title=Non-prompt fraction for $\psi(2S)$ at 7 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d11-x01-y08
Title=Non-prompt fraction for $\psi(2S)$ at 7 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d12-x01-y01
Title=Non-prompt fraction for $\psi(2S)$ at 8 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d12-x01-y02
Title=Non-prompt fraction for $\psi(2S)$ at 8 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d12-x01-y03                                                    
Title=Non-prompt fraction for $\psi(2S)$ at 8 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d12-x01-y04                                                    
Title=Non-prompt fraction for $\psi(2S)$ at 8 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d12-x01-y05                                                    
Title=Non-prompt fraction for $\psi(2S)$ at 8 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d12-x01-y06                                                    
Title=Non-prompt fraction for $\psi(2S)$ at 8 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d12-x01-y07                                                    
Title=Non-prompt fraction for $\psi(2S)$ at 8 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d12-x01-y08
Title=Non-prompt fraction for $\psi(2S)$ at 8 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d13-x01-y01
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d13-x01-y02
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d13-x01-y03                                                    
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d13-x01-y04                                                    
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d13-x01-y05                                                    
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d13-x01-y06                                                    
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d13-x01-y07                                                    
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d13-x01-y08
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d14-x01-y01
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d14-x01-y02
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d14-x01-y03                                                    
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d14-x01-y04                                                    
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d14-x01-y05                                                    
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d14-x01-y06                                                    
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d14-x01-y07                                                    
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d14-x01-y08
Title=Ratio of prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d15-x01-y01
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d15-x01-y02
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d15-x01-y03                                                    
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d15-x01-y04                                                    
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d15-x01-y05                                                    
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d15-x01-y06                                                    
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d15-x01-y07                                                    
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d15-x01-y08
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 7 TeV ($1.75<y<2.00$) 
END PLOT

BEGIN PLOT /ATLAS_2016_I1409298/d16-x01-y01
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($0<y<0.25$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d16-x01-y02
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($0.25<y<0.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d16-x01-y03                                                    
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($0.50<y<0.75$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d16-x01-y04                                                    
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($0.75<y<1.00$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d16-x01-y05                                                    
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($1.00<y<1.25$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d16-x01-y06                                                    
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($1.25<y<1.50$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2016_I1409298/d16-x01-y07                                                    
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($1.50<y<1.75$) 
END PLOT
BEGIN PLOT /ATLAS_2016_I1409298/d16-x01-y08
Title=Ratio of non-prompt $\psi(2S)$ to J$/\psi$ at 8 TeV ($1.75<y<2.00$) 
END PLOT
