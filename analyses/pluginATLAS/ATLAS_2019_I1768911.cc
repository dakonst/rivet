#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/DileptonFinder.hh"

namespace Rivet {


  /// @brief Z pT and Z phi* at 13 TeV
  class ATLAS_2019_I1768911 : public Analysis {
  public:

    /// Constructor
    RIVET_DEFAULT_ANALYSIS_CTOR(ATLAS_2019_I1768911);


    /// @name Analysis methods
    /// @{

    /// Book histograms and initialise projections before the run
    void init() {

      // Get options
      _mode = 0;
      if ( getOption("LMODE") == "EL" ) _mode = 1;
      if ( getOption("LMODE") == "MU" ) _mode = 2;

      // Configure projections
      Cut cuts = Cuts::abseta < 2.5 && Cuts::pT > 27*GeV;
      DileptonFinder zmmFinder(91.2*GeV, 0.1, cuts && Cuts::abspid == PID::MUON, Cuts::massIn(66*GeV, 116*GeV));
      declare(zmmFinder, "DileptonFinder_mu");
      DileptonFinder zeeFinder(91.2*GeV, 0.1, cuts && Cuts::abspid == PID::ELECTRON, Cuts::massIn(66*GeV, 116*GeV));
      declare(zeeFinder, "DileptonFinder_el");

      // Book histograms
      book(_h["zpt_combined_dressed_normalised"], 27, 1, 1);
      book(_h["zphistar_combined_dressed_normalised"], 28, 1, 1);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      // Get leptonic Z boson
      const DileptonFinder& zmmFinder = apply<DileptonFinder>(event, "DileptonFinder_mu");
      const DileptonFinder& zeeFinder = apply<DileptonFinder>(event, "DileptonFinder_el");
      if (_mode == 2 && zmmFinder.bosons().size() != 1 && zeeFinder.bosons().size())   vetoEvent;
      if (_mode == 1 && zeeFinder.bosons().size() != 1 && zmmFinder.bosons().size())   vetoEvent;
      if (_mode == 0 && (zeeFinder.bosons().size() + zmmFinder.bosons().size()) != 1)  vetoEvent;
      const Particle& Zboson = zeeFinder.bosons().size()? zeeFinder.boson() : zmmFinder.boson();

      // cut on Z boson leptons and calculate pTll and phistar
      const Particles& leptons = zeeFinder.bosons().size()? zeeFinder.constituents() : zmmFinder.constituents();
      if (leptons.size() != 2 || leptons[0].charge3() * leptons[1].charge3() > 0) vetoEvent;

      const double zpt = Zboson.pT()/GeV;
      const Particle& lminus = leptons[0].charge() < 0 ? leptons[0] : leptons[1];
      const Particle& lplus  = leptons[0].charge() < 0 ? leptons[1] : leptons[0];
      const double phi_acop = M_PI - deltaPhi(lminus, lplus);
      const double costhetastar = tanh( 0.5 * (lminus.eta() - lplus.eta()) );
      const double sin2thetastar = (costhetastar > 1) ? 0.0 : (1.0 - sqr(costhetastar));
      const double phistar = tan(0.5 * phi_acop) * sqrt(sin2thetastar);

      _h["zpt_combined_dressed_normalised"]->fill(zpt);
      _h["zphistar_combined_dressed_normalised"]->fill(phistar);
    }


    /// Normalise histograms etc., after the run
    void finalize() {  normalize(_h);  }
    /// @}


  protected:

    size_t _mode;


  private:

    /// @name Histograms
    /// @{
    map<string, Histo1DPtr> _h;
    /// @}

  };


  RIVET_DECLARE_PLUGIN(ATLAS_2019_I1768911);
}
