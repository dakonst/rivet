BEGIN PLOT /ATLAS_2018_I1622737/d01
XLabel=$p_\perp$ [GeV]
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d01-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ ($0<y<0.75$) 
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d01-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ ($0.75<y<1.5$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2018_I1622737/d01-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ ($1.5<y<2.0$) 
END PLOT

BEGIN PLOT /ATLAS_2018_I1622737/d02
XLabel=$p_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ ($0<y<0.75$) 
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ ($0.75<y<1.5$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2018_I1622737/d02-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt $\psi(2S)$ ($1.5<y<2.0$) 
END PLOT

BEGIN PLOT /ATLAS_2018_I1622737/d03
XLabel=$p_\perp$ [GeV]
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ ($0<y<0.75$) 
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d03-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ ($0.75<y<1.5$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2018_I1622737/d03-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ ($1.5<y<2.0$) 
END PLOT

BEGIN PLOT /ATLAS_2018_I1622737/d04
XLabel=$p_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d04-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ ($0<y<0.75$) 
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d04-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ ($0.75<y<1.5$) 
END PLOT                                                                                       
BEGIN PLOT /ATLAS_2018_I1622737/d04-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt $\psi(2S)$ ($1.5<y<2.0$) 
END PLOT

BEGIN PLOT /ATLAS_2018_I1622737/d05
XLabel=$p_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d05-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ ($0<y<0.75$) 
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d05-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ ($0.75<y<1.5$) 
END PLOT                                                                                
BEGIN PLOT /ATLAS_2018_I1622737/d05-x01-y03                                             
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ ($1.5<y<2.0$) 
END PLOT

BEGIN PLOT /ATLAS_2018_I1622737/d06
XLabel=$p_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d06-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ ($0<y<0.75$) 
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d06-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ ($0.75<y<1.5$) 
END PLOT                                                                                
BEGIN PLOT /ATLAS_2018_I1622737/d06-x01-y03                                             
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ ($1.5<y<2.0$) 
END PLOT

BEGIN PLOT /ATLAS_2018_I1622737/d07
XLabel=$p_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d07-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ ($0<y<0.75$) 
END PLOT
BEGIN PLOT /ATLAS_2018_I1622737/d07-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ ($0.75<y<1.5$) 
END PLOT                                                                                
BEGIN PLOT /ATLAS_2018_I1622737/d07-x01-y03                                             
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ ($1.5<y<2.0$) 
END PLOT
