# BEGIN PLOT /ATLAS_2016_I1502620/d..-x..-y..
LeftMargin=1.5
LogX=0
LogY=0
GofType=chi2
RatioPlotYMin=0.85
RatioPlotYMax=1.15
YLabel=$\mathrm{d}\sigma / \mathrm{d}y_{\ell\ell}$ [pb]
XLabel=$|y_{\ell\ell}|$
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1502620/d11
Title=central $Z/\gamma^*\rightarrow \ell\ell$, $46 < m_{\ell\ell} < 66$ GeV
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1502620/d12
Title=central $Z/\gamma^*\rightarrow \ell\ell$, $66 < m_{\ell\ell} < 116$ GeV
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1502620/d13
Title=central $Z/\gamma^*\rightarrow \ell\ell$, $116 < m_{\ell\ell} < 150$ GeV
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1502620/d14
Title=forward $Z/\gamma^*\rightarrow \ell\ell$, $66 < m_{\ell\ell} < 116$ GeV
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1502620/d15
Title=forward $Z/\gamma^*\rightarrow \ell\ell$, $116 < m_{\ell\ell} < 150$ GeV
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1502620/d35
RatioPlotYMin=0.9
RatioPlotYMax=1.1
XLabel=$|\eta_\ell|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta_\ell$ [pb]
Title=$W$ charge asymmetry, dressed level
YLabel=$A_\ell$
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1502620/d09
RatioPlotYMin=0.9
RatioPlotYMax=1.1
XLabel=$|\eta_\ell|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta_\ell$ [pb]
Title=$W^+\rightarrow \ell^+\nu$, dressed level
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1502620/d10
RatioPlotYMin=0.9
RatioPlotYMax=1.1
XLabel=$|\eta_\ell|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta_\ell$ [pb]
Title=$W^-\rightarrow \ell^-\bar{\nu}$, dressed level
# END PLOT

