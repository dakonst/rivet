# BEGIN PLOT /ATLAS_2010_I882463/d01-x01-y01
Title=Transverse energy of isolated prompt photon, $|\eta| < 0.60$
XLabel=$E_\perp^\gamma$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}E_\perp^\gamma$ [nb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2010_I882463/d02-x01-y01
Title=Transverse energy of isolated prompt photon, $0.60 \leq |\eta| < 1.37$
XLabel=$E_\perp^\gamma$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}E_\perp^\gamma$ [nb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2010_I882463/d03-x01-y01
Title=Transverse energy of isolated prompt photon, $1.52 \leq |\eta| < 1.81$
XLabel=$E_\perp^\gamma$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}E_\perp^\gamma$ [nb/GeV]
# END PLOT
