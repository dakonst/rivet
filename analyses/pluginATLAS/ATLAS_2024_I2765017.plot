BEGIN PLOT /ATLAS_2024_I2765017/.*
LogY=1
LeftMargin=1.5
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/.*_met_.*
XLabel= $p_\text{T}^\text{recoil}$ [GeV]
YLabel= $\text{d}\sigma/\text{d}p_\text{T}^\text{recoil}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/.*_mjj_.*
XLabel= $m_{jj}$ [GeV]
YLabel= $\text{d}\sigma/\text{d}m_{jj}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/.*_dphij.*
XLabel= $\Delta\phi_{jj}$ [rad/$\pi$]
YLabel= $\text{d}\sigma/\text{d}\Delta\phi_{jj}$ [fb/rad]
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/sr0l_.*_mono
Title=0-lepton region, $\geq 1$ jet phase space
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/sr0l_.*_vbf
Title=0-lepton region, VBF phase space
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/sr0l_cr1im_.*_mono
Title=1-$\mu$ region, $\geq 1$ jet phase space
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/sr0l_cr1im_.*_vbf
Title=1-$\mu$ region, VBF phase space
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/sr0l_cr1ie_.*_mono
Title=1-$e$ region, $\geq 1$ jet phase space
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/sr0l_cr1ie_.*_vbf
Title=1-$e$ region, VBF phase space
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/sr0l_cr2im_.*_mono
Title=2-$\mu$ region, $\geq 1$ jet phase space
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/sr0l_cr2im_.*_vbf
Title=2-$\mu$ region, VBF phase space
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/sr0l_cr2ie_.*_mono
Title=2-$e$ region, $\geq 1$ jet phase space
END PLOT

BEGIN PLOT /ATLAS_2024_I2765017/sr0l_cr2ie_.*_vbf
Title=2-$e$ region, VBF phase space
END PLOT

