# BEGIN PLOT /D0_2000_I535017/d01-x01-y01
Title=W boson pT
XLabel=$p_{\perp}^W$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp^W$ [pb/GeV]
# END PLOT
