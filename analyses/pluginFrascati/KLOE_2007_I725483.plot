BEGIN PLOT /KLOE_2007_I725483/d01-x0[1,2,3,4]-y01
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^0\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /KLOE_2007_I725483/d01-x0[1,2,3,4]-y02
XLabel=$m_{\pi^0\gamma}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^0\gamma}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /KLOE_2007_I725483/d01-x01-y01
Title=$\pi^0\pi^0$ mass for $e^+e^-\to\pi^0\pi^0\gamma$ at $\sqrt{s}=1019.55\,$MeV
END PLOT
BEGIN PLOT /KLOE_2007_I725483/d01-x01-y02
Title=$\gamma\pi^0$ mass for $e^+e^-\to\pi^0\pi^0\gamma$ at $\sqrt{s}=1019.55\,$MeV
END PLOT

BEGIN PLOT /KLOE_2007_I725483/d01-x02-y01
Title=$\pi^0\pi^0$ mass for $e^+e^-\to\pi^0\pi^0\gamma$ at $\sqrt{s}=1019.65\,$MeV
END PLOT
BEGIN PLOT /KLOE_2007_I725483/d01-x02-y02
Title=$\gamma\pi^0$ mass for $e^+e^-\to\pi^0\pi^0\gamma$ at $\sqrt{s}=1019.65\,$MeV
END PLOT

BEGIN PLOT /KLOE_2007_I725483/d01-x03-y01
Title=$\pi^0\pi^0$ mass for $e^+e^-\to\pi^0\pi^0\gamma$ at $\sqrt{s}=1019.85\,$MeV
END PLOT
BEGIN PLOT /KLOE_2007_I725483/d01-x03-y02
Title=$\gamma\pi^0$ mass for $e^+e^-\to\pi^0\pi^0\gamma$ at $\sqrt{s}=1019.85\,$MeV
END PLOT

BEGIN PLOT /KLOE_2007_I725483/d01-x04-y01
Title=$\pi^0\pi^0$ mass for $e^+e^-\to\pi^0\pi^0\gamma$ at $\sqrt{s}=1019.95\,$MeV
END PLOT
BEGIN PLOT /KLOE_2007_I725483/d01-x04-y02
Title=$\gamma\pi^0$ mass for $e^+e^-\to\pi^0\pi^0\gamma$ at $\sqrt{s}=1019.95\,$MeV
END PLOT