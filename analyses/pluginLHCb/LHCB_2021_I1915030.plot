BEGIN PLOT /LHCB_2021_I1915030/d0[1,2]
XLabel=$p_\perp$ [GeV]
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /LHCB_2021_I1915030/d01-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2021_I1915030/d01-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($2.5<y<3.0$) 
END PLOT                                                                                       
BEGIN PLOT /LHCB_2021_I1915030/d01-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($3.0<y<3.5$) 
END PLOT                                                                                       
BEGIN PLOT /LHCB_2021_I1915030/d01-x01-y04                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($3.5<y<4.0$) 
END PLOT                                                                                       
BEGIN PLOT /LHCB_2021_I1915030/d01-x01-y05                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($4.0<y<4.5$) 
END PLOT                                        

BEGIN PLOT /LHCB_2021_I1915030/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2021_I1915030/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($2.5<y<3.0$) 
END PLOT                                                                                       
BEGIN PLOT /LHCB_2021_I1915030/d02-x01-y03                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($3.0<y<3.5$) 
END PLOT                                                                                       
BEGIN PLOT /LHCB_2021_I1915030/d02-x01-y04                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($3.5<y<4.0$) 
END PLOT                                                                                       
BEGIN PLOT /LHCB_2021_I1915030/d02-x01-y05                                                    
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2021_I1915030/d03-x01-y01                  
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for prompt J/$\psi$  ($2.0<y<4.5$) 
XLabel=$p_\perp$ [GeV]
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2021_I1915030/d04-x01-y01                  
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for non-prompt J/$\psi$  ($2.0<y<4.5$) 
XLabel=$p_\perp$ [GeV]
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /LHCB_2021_I1915030/d05-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for prompt J/$\psi$  ($p_\perp<14.$\,GeV) 
XLabel=$y$ [GeV]
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /LHCB_2021_I1915030/d05-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for prompt J/$\psi$  ($p_\perp<8.$\,GeV) 
XLabel=$y$ [GeV]
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /LHCB_2021_I1915030/d06-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for non-prompt J/$\psi$  ($p_\perp<14.$\,GeV) 
XLabel=$y$ [GeV]
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT

BEGIN PLOT /LHCB_2021_I1915030/d07-x01-y01
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction/\%
END PLOT
BEGIN PLOT /LHCB_2021_I1915030/d07-x01-y01
Title=Non-prompt fraction for J/$\psi$  ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2021_I1915030/d07-x01-y02
Title=Non-prompt fraction for J/$\psi$  ($2.5<y<3.0$) 
END PLOT                                                                                       
BEGIN PLOT /LHCB_2021_I1915030/d07-x01-y03                                                    
Title=Non-prompt fraction for J/$\psi$  ($3.0<y<3.5$) 
END PLOT                                                                                       
BEGIN PLOT /LHCB_2021_I1915030/d07-x01-y04                                                    
Title=Non-prompt fraction for J/$\psi$  ($3.5<y<4.0$) 
END PLOT                                                                                       
BEGIN PLOT /LHCB_2021_I1915030/d07-x01-y05                                                    
Title=Non-prompt fraction for J/$\psi$  ($4.0<y<4.5$) 
END PLOT                  
