BEGIN PLOT /LHCB_2017_I1509507/d01-x01-y01
Title=Prompt J/$\psi$ fragmentation function 
XLabel=$z$
YLabel=$\frac1\sigma\mathrm{d}\sigma/\mathrm{d}z$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2017_I1509507/d01-x01-y02
Title=Non-prompt J/$\psi$ fragmentation function 
XLabel=$z$
YLabel=$\frac1\sigma\mathrm{d}\sigma/\mathrm{d}z$
LogY=0
END PLOT
