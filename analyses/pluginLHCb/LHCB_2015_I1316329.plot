BEGIN PLOT /LHCB_2015_I1316329/d
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [nb/GeV]
END PLOT

BEGIN PLOT /LHCB_2015_I1316329/d01-x01-y01
Title=Prompt $\eta_c$ production w.r.t $p_\perp$ $\sqrt{s}=7$\,TeV ($2<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2015_I1316329/d02-x01-y01
Title=Prompt $\eta_c$ production w.r.t $p_\perp$ $\sqrt{s}=8$\,TeV ($2<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2015_I1316329/d03-x01-y01
Title=Non-prompt $\eta_c$ production w.r.t $p_\perp$ $\sqrt{s}=7$\,TeV ($2<y<4.5$)
END PLOT
BEGIN PLOT /LHCB_2015_I1316329/d04-x01-y01
Title=Non-prompt $\eta_c$ production w.r.t $p_\perp$ $\sqrt{s}=8$\,TeV ($2<y<4.5$)
END PLOT
