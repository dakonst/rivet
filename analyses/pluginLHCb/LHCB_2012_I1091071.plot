BEGIN PLOT /LHCB_2012_I1091071/d[02,03,04,05,06,07,08,09,10,11,12,13,14,15,16]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /LHCB_2012_I1091071/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$  ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$  ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d04-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$  ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d05-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$  ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d06-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$  ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2012_I1091071/d07-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$  ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d08-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$  ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d09-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$  ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d10-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$  ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d11-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$  ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2012_I1091071/d12-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$  ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d13-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$  ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d14-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$  ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d15-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$  ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/d16-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$  ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2012_I1091071/d17-x01-y01
Title=$\Upsilon(2S)$/$\Upsilon(1S)$ ratio
XLabel=$p_\perp$ [GeV]
YLabel=$N_(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S\to\mu^+\mu^-))$
END PLOT
BEGIN PLOT /LHCB_2012_I1091071/17-x01-y02
Title=$\Upsilon(3S)$/$\Upsilon(1S)$ ratio
XLabel=$p_\perp$ [GeV]
YLabel=$N_(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S\to\mu^+\mu^-))$
END PLOT
