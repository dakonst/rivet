Name: LHCB_2013_I1230344
Year: 2013
Summary: Differential cross sections for $J/\psi$ and $\Upsilon(1,2,3S)$ production at 8 TeV
Experiment: LHCB
Collider: LHC
InspireID: 1230344
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 06 (2013) 064, 2013
RunInfo: hadronic events with Jpsi and Upsilon production
Beams: [p+, p+]
Energies: [8000]
Description:
  'Measurement of the double differential (in $p_\perp$ and $y$) cross section for $J/\psi$ and $\Upsilon(1,2,3S)$ production at 8 TeV by the LHCB collaboration.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2013itw
BibTeX: '@article{LHCb:2013itw,
    author = "Aaij, R and others",
    collaboration = "LHCb",
    title = "{Production of J/psi and Upsilon mesons in pp collisions at sqrt(s) = 8 TeV}",
    eprint = "1304.6977",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-PH-EP-2013-071, LHCB-PAPER-2013-016",
    doi = "10.1007/JHEP06(2013)064",
    journal = "JHEP",
    volume = "06",
    pages = "064",
    year = "2013"
}
'
