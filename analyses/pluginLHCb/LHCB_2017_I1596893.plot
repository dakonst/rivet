BEGIN PLOT /LHCB_2017_I1596893/d01-x01-y01
Title=$p\bar\Lambda^0$ mass distribution in $B^0\to p\bar\Lambda^0 \pi^-$
XLabel=$m_{p\bar\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar\Lambda^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2017_I1596893/d01-x01-y02
Title=$p\pi^-$ mass distribution in $B^0\to p\bar\Lambda^0 \pi^-$
XLabel=$m_{p\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2017_I1596893/d01-x01-y03
Title=$\bar\Lambda^0\pi^-$ mass distribution in $B^0\to p\bar\Lambda^0 \pi^-$
XLabel=$m_{\bar\Lambda^0\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Lambda^0\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2017_I1596893/d02-x01-y01
Title=$p\bar\Lambda^0$ mass distribution in $B^0_s\to p\bar\Lambda^0 K^-$
XLabel=$m_{p\bar\Lambda^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\bar\Lambda^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2017_I1596893/d02-x01-y02
Title=$pK^-$ mass distribution in $B^0_s\to p\bar\Lambda^0 K^-$
XLabel=$m_{pK^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{pK^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2017_I1596893/d02-x01-y03
Title=$\bar\Lambda^0K^-$ mass distribution in $B^0_s\to p\bar\Lambda^0 K^-$
XLabel=$m_{\bar\Lambda^0K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Lambda^0K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
