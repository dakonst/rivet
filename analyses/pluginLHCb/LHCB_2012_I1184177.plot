BEGIN PLOT /LHCB_2012_I1184177/d01-x01-y01
Title=Fraction of $\Upsilon(1S)$ from $\chi_{b(0,1,2)}(1P)\to\gamma \Upsilon(1S)$
XLabel=$p_\perp^{\Upsilon(1S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(1S)}$ [$\%$]
LogY=0
END PLOT
