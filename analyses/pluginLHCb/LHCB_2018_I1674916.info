Name: LHCB_2018_I1674916
Year: 2018
Summary: Prompt $D_s+/D_s^-$ Asymmetry at 7 and 8 TeV
Experiment: LHCB
Collider: LHC
InspireID: 1674916
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 08 (2018) 008, 2018
RunInfo: D_s production
Beams: [p+, p+]
Energies: [7000,8000]
Options:
 - ENERGY=7000,8000
Description:
  'Measurement of the production asymmetry for prompt $D_s+/D_s^-$ at 7 and 8 TeV by LHCb.
  Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  Herwig 7 '
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2018elu
BibTeX: '@article{LHCb:2018elu,
    author = "Aaij, Roel and others",
    collaboration = "LHCb",
    title = "{Measurement of $D_s^{\pm}$ production asymmetry in $pp$ collisions at $\sqrt{s} =7$ and 8 TeV}",
    eprint = "1805.09869",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "LHCb-PAPER-2018-010, CERN-EP-2018-073, LHCB-PAPER-2018-010",
    doi = "10.1007/JHEP08(2018)008",
    journal = "JHEP",
    volume = "08",
    pages = "008",
    year = "2018"
}
'
