Name: LHCB_2012_I1091071
Year: 2012
Summary: Differential cross sections for $\Upsilon(1,2,3S)$ production at 7 TeV
Experiment: LHCB
Collider: LHC
InspireID: 1091071
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 06 (2013) 064
RunInfo: hadronic events with Upsilon production
Beams: [p+, p+]
Energies: [7000]
Description:
  'Measurement of the double differential (in $p_\perp$ and $y$) cross section for $\Upsilon(1,2,3S)$ production at 7 TeV by the LHCB collaboration.'
ValidationInfo:
  'Herwig7 ewvents'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2013itw
BibTeX: '@article{LHCb:2013itw,
    author = "Aaij, R and others",
    collaboration = "LHCb",
    title = "{Production of J/psi and Upsilon mesons in pp collisions at sqrt(s) = 8 TeV}",
    eprint = "1304.6977",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-PH-EP-2013-071, LHCB-PAPER-2013-016",
    doi = "10.1007/JHEP06(2013)064",
    journal = "JHEP",
    volume = "06",
    pages = "064",
    year = "2013"
}
'
