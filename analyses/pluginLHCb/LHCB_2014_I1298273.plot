BEGIN PLOT /LHCB_2014_I1298273/d01-x01-y01
XLabel=$p_\perp$ [GeV]
YLabel=$f_{\Lambda_b^0}/f_d$
Title=$\Lambda_b^0$ fraction
LogY=0
END PLOT
BEGIN PLOT /LHCB_2014_I1298273/d02-x01-y01
XLabel=$y$
YLabel=$f_{\Lambda_b^0}/f_d$
Title=$\Lambda_b^0$ fraction
LogY=0
END PLOT
