BEGIN PLOT /LHCB_2011_I891233
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d10
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
END PLOT

BEGIN PLOT /LHCB_2011_I891233/d06-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (no pol) ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d06-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (no pol) ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d06-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (no pol) ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d06-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (no pol) ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d06-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (no pol) ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2011_I891233/d07-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ (no pol) ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d07-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ (no pol) ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d07-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ (no pol) ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d07-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ (no pol) ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d07-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$ (no pol) ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2011_I891233/d08-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (trans pol) ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d08-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (trans pol) ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d08-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (trans pol) ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d08-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (trans pol) ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d08-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (trans pol) ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2011_I891233/d09-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (long pol) ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d09-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (long pol) ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d09-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (long pol) ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d09-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (long pol) ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d09-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$ (long pol) ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2011_I891233/d10-x01-y01
Title=Non-prompt J/$\psi$ fraction at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d10-x01-y02
Title=Non-prompt J/$\psi$ fraction at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d10-x01-y03
Title=Non-prompt J/$\psi$ fraction at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d10-x01-y04
Title=Non-prompt J/$\psi$ fraction at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2011_I891233/d10-x01-y05
Title=Non-prompt J/$\psi$ fraction at 8 TeV ($4.0<y<4.5$) 
END PLOT
