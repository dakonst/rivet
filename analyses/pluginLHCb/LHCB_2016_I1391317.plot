BEGIN PLOT /LHCB_2016_I1391317/d01-x01-y01
END PLOT

BEGIN PLOT /LHCB_2016_I1391317/d0[1,2]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathcal(Br)\Lambda^0_b\to J/\psi xpK^-)\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d01-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Lambda^0_b$ at 7 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d01-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Lambda^0_b$ at 7 TeV  ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d01-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Lambda^0_b$ at 7 TeV  ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d01-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Lambda^0_b$ at 7 TeV  ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d01-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Lambda^0_b$ at 7 TeV  ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2016_I1391317/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Lambda^0_b$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Lambda^0_b$ at 8 TeV  ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d02-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Lambda^0_b$ at 8 TeV  ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d02-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Lambda^0_b$ at 8 TeV  ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d02-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Lambda^0_b$ at 8 TeV  ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2016_I1391317/d0[3,4]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathcal(Br)B^0to J/\psi K^*{0})\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /LHCB_2016_I1391317/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ at 7 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d03-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ at 7 TeV  ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d03-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ at 7 TeV  ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d03-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ at 7 TeV  ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d03-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ at 7 TeV  ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2016_I1391317/d04-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d04-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ at 8 TeV  ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d04-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ at 8 TeV  ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d04-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ at 8 TeV  ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d04-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ at 8 TeV  ($4.0<y<4.5$) 
END PLOT


BEGIN PLOT /LHCB_2016_I1391317/d05-x01-y01
Title=$\Lambda_b^0$ $\bar{\Lambda}^0_b$ asymmetry at 7 TeV
XLabel=$p_\perp$ [GeV]
YLabel=Asymmetry
LogY=0
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d05-x01-y02
Title=$\Lambda_b^0$ $\bar{\Lambda}^0_b$ asymmetry at 8 TeV
XLabel=$p_\perp$ [GeV]
YLabel=Asymmetry
LogY=0
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d06-x01-y01
Title=$\Lambda_b^0$ $\bar{\Lambda}^0_b$ asymmetry at 7 TeV
XLabel=$y$
YLabel=Asymmetry [$\%$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2016_I1391317/d06-x01-y02
Title=$\Lambda_b^0$ $\bar{\Lambda}^0_b$ asymmetry at 8 TeV
XLabel=$y$
YLabel=Asymmetry [$\%$]
LogY=0
END PLOT
