BEGIN PLOT /CLEOII_1995_I382221/d01-x01-y01
Title=$J/\psi$ momentum distribution
XLabel=$p$ [GeV]
YLabel=$\mathrm{d}B/\mathrm{d}p$ [$\%/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1995_I382221/d02-x01-y01
Title=$J/\psi$ momentum distribution (excluding $\psi(2S)$ and $\chi_c$ decay)
XLabel=$p$ [GeV]
YLabel=$\mathrm{d}B/\mathrm{d}p$ [$\%/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1995_I382221/d03-x01-y01
Title=$\psi(2S)$ momentum distribution
XLabel=$p$ [GeV]
YLabel=$\mathrm{d}B/\mathrm{d}p$ [$\%/\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1995_I382221/d04-x01-y01
Title=$\chi_{c1}$ momentum distribution
XLabel=$p$ [GeV]
YLabel=$\mathrm{d}B/\mathrm{d}p$ [$\%/\mathrm{GeV}$]
LogY=0
END PLOT
