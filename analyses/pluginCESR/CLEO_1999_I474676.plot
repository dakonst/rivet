BEGIN PLOT /CLEO_1999_I474676/d01-x01-y01
Title=$\sigma(e^+e^-\to \gamma\Upsilon(3S))$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \gamma\Upsilon(3S))$ [pb]
ConnectGaps=1
END PLOT
BEGIN PLOT /CLEO_1999_I474676/d01-x01-y02
Title=$\sigma(e^+e^-\to \gamma\Upsilon(2S))$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \gamma\Upsilon(2S))$ [pb]
ConnectGaps=1
END PLOT1
