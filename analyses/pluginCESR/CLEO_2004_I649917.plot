BEGIN PLOT /CLEO_2004_I649917/d01-x01-y01
Title=$K^0_S\pi^0$ mass distribution in $D^0\to K^0_S\pi^0\eta$
XLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2004_I649917/d01-x01-y02
Title=$\pi^0\eta$ mass distribution in $D^0\to K^0_S\pi^0\eta$
XLabel=$m^2_{\pi^0\eta}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^0\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2004_I649917/d01-x01-y03
Title=$K^0_S\eta$ mass distribution in $D^0\to K^0_S\pi^0\eta$
XLabel=$m^2_{K^0_S\eta}$ [$\mathrm{GeV}^2$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\eta}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2004_I649917/dalitz
Title=Dalitz plot for $D^0\to K^0_S\pi^0\eta$
XLabel=$m^2_{\pi^0\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_S\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^0\eta}/{\rm d}m^2_{K^0_S\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT