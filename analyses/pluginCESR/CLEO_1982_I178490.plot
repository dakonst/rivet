BEGIN PLOT /CLEO_1982_I178490/d01-x01-y01
Title=Cross section for $e^+e^-\to D^{*+}X$ 
YLabel=$\sigma$ [nb]
XCustomMajorTicks=1 $x_E>0.5$   2   $x_E>0.7$ 
LogY=0
END PLOT
BEGIN PLOT /CLEO_1982_I178490/d02-x01-y01
Title=$D^{*+}$ scaled momentum
XLabel=$x_E$ 
YLabel=$s\times\mathrm{d}\sigma/\mathrm{d}x_E$ [$\text{GeV}^2\mu\text{b}$]
LogY=0
END PLOT
