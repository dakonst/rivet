BEGIN PLOT /CLEO_2013_I1081165/d01-x01-y01
Title=$e^+\nu_e$ mass squared in $D\to \rho e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2013_I1081165/d01-x01-y03
Title=$\cos\theta_e$ in $D\to\rho e^+\nu_e$
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
LogY=0
END PLOT
BEGIN PLOT /CLEO_2013_I1081165/d01-x01-y02
Title=$\cos\theta_\pi$ in $D\to\rho e^+\nu_e$
XLabel=$\cos\theta_\pi$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_\pi$ 
LogY=0
END PLOT
BEGIN PLOT /CLEO_2013_I1081165/d01-x01-y04
Title=$\chi$ in $D\to\rho e^+\nu_e$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
LogY=0
END PLOT
