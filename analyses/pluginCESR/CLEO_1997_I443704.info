Name: CLEO_1997_I443704
Year: 1997
Summary:  Exclusive semileptonic $B$ to $D$ decays.
Experiment: CLEO
Collider: CESR
InspireID: 443704
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 79 (1997) 2208-2212
RunInfo: Any process producing B mesons, originally Upslion(4S) decay
Description:
  'Measurement of the differential partial width with respect to $w$ for $B$ to $D$ semi-leptonic decays.'
ValidationInfo:
  'Herwig7 events using EvtGen for B decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:1997yyh
BibTeX: '@article{CLEO:1997yyh,
    author = "Athanas, M. and others",
    collaboration = "CLEO",
    title = "{Measurement of the anti-B ---\ensuremath{>} D lepton anti-neutrino partial width and form-factor parameters}",
    eprint = "hep-ex/9705019",
    archivePrefix = "arXiv",
    reportNumber = "SLAC-PUB-9776, CLNS-97-1486, CLEO-97-12",
    doi = "10.1103/PhysRevLett.79.2208",
    journal = "Phys. Rev. Lett.",
    volume = "79",
    pages = "2208--2212",
    year = "1997"
}
'
