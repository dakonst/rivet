BEGIN PLOT /CLEOII_1997_I440969/d0[1,2,3,4]-x01-y01
XLabel=$x_p$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}x_p$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d01-x01-y01
Title=Scaled $D^0$ momentum spectrum
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d02-x01-y01
Title=Scaled $D^+$ momentum spectrum
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d03-x01-y01
Title=Scaled $D^{*0}$ momentum spectrum
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d04-x01-y01
Title=Scaled $D^{*+}$ momentum spectrum
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d0[5,6]-x01-
XLabel=$\cos\theta$
YLabel=$1/N\text{d}N/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d05-x01-y01
Title=$D^{*+}$ decay angle ($0.1<x<0.2$)
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d05-x01-y02
Title=$D^{*+}$ decay angle ($0.2<x<0.3$)
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d05-x01-y03
Title=$D^{*+}$ decay angle ($0.3<x<0.4$)
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d05-x01-y04
Title=$D^{*+}$ decay angle ($0.4<x<0.5$)
END PLOT

BEGIN PLOT /CLEOII_1997_I440969/d06-x01-y01
Title=$D^{*0}$ decay angle ($0.0<x<0.1$)
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d06-x01-y02
Title=$D^{*0}$ decay angle ($0.1<x<0.2$)
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d06-x01-y03
Title=$D^{*0}$ decay angle ($0.2<x<0.3$)
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d06-x01-y04
Title=$D^{*0}$ decay angle ($0.3<x<0.4$)
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d06-x01-y05
Title=$D^{*0}$ decay angle ($0.4<x<0.5$)
END PLOT

BEGIN PLOT /CLEOII_1997_I440969/d07-x01-y0[1,2]
XLabel=$x_p$
YLabel=$\beta$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d07-x01-y01
Title=$\beta$ for $D^{*+}$ production
END PLOT
BEGIN PLOT /CLEOII_1997_I440969/d07-x01-y02
Title=$\beta$ for $D^{*0}$ production
END PLOT
