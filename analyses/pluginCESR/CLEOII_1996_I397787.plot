BEGIN PLOT /CLEOII_1996_I397787/d01-x01-y01
Title=$\pi^+K^-$ mass distribution in $\Xi_c^+\to \Sigma^+K^-\pi^+$
XLabel=$m_{\pi^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
