BEGIN PLOT /OPAL_1995_I382219/d03-x01-y01
Title=Scaled energy of $D^{*\pm}$ at $\sqrt{s}=91.2$~GeV
XLabel=$x_E$
YLabel=$BR(D^{*+}\to D^0(K^-\pi^+)\pi^+)/N_\mathrm{Zhad} \mathrm{d}N(D^{*})/\mathrm{d}x_E$
LogY=0
END PLOT
BEGIN PLOT /OPAL_1995_I382219/d04-x01-y01
Title=Scaled energy of $D^{*\pm}$ in bottom events at $\sqrt{s}=91.2$~GeV
XLabel=$x_E$
YLabel=$BR(D^{*+}\to D^0(K^-\pi^+)\pi^+)/N_\mathrm{Zhad} \mathrm{d}N(D^{*})/\mathrm{d}x_E$
LogY=0
END PLOT
BEGIN PLOT /OPAL_1995_I382219/d05-x01-y01
Title=Scaled energy of $D^{*\pm}$ in charm events at $\sqrt{s}=91.2$~GeV
XLabel=$x_E$
YLabel=$BR(D^{*+}\to D^0(K^-\pi^+)\pi^+)/N_\mathrm{Zhad} \mathrm{d}N(D^{*})/\mathrm{d}x_E$
LogY=0
END PLOT