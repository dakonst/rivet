# BEGIN PLOT /OPAL_1992_I321190/d01-x01-y01
Title=Total charged multiplicity
XLabel=$n_\mathrm{ch}$
YLabel=$2/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{n_\mathrm{ch}}$ ($\%$)
# END PLOT


# BEGIN PLOT /OPAL_1992_I321190/d05-x01-y01
Title=Mean charged multiplicity
LogY=0
# END PLOT

