# BEGIN PLOT /DELPHI_1991_I301657/d02-x01-y01
Title=Total charged multiplicity
XLabel=$n_\mathrm{ch}$
YLabel=$2/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{n_\mathrm{ch}}$ ($\%$)
# END PLOT


# BEGIN PLOT /DELPHI_1991_I301657/d04-x01-y01
Title=Mean charged multiplicity
LogY=0
# END PLOT

