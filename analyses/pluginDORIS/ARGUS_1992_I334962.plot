BEGIN PLOT /ARGUS_1992_I334962/d01-x01-y01
Title=Proton momentum spectrum in B decays
XLabel=$p$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}p$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1992_I334962/d02-x01-y01
Title=$\Lambda^0$ momentum spectrum in B decays
XLabel=$p$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}p$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /ARGUS_1992_I334962/d03-x01-y01
Title=Proton from charm baryon momentum spectrum in B decays, with proton 
XLabel=$p$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}p$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1992_I334962/d03-x01-y02
Title=Proton not from charm baryon momentum spectrum in B decays
XLabel=$p$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}p$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
