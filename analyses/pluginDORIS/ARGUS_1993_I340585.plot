BEGIN PLOT /ARGUS_1993_I340585/d01-x01-y01
Title=Different branching ratio w.r.t $q^2$ for $\bar{B}^0\to D^{*+}\ell^-\bar\nu_\ell$
XLabel=$q^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\mathcal{B}/\text{d}q^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
