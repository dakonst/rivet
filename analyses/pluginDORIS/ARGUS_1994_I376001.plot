BEGIN PLOT /ARGUS_1994_I376001/d01-x01-y01
Title=$J/\psi$ helicity angle for $B\to J/\psi K^*$
XLabel=$\cos\theta_{J/\psi}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{J/\psi}$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1994_I376001/d01-x01-y02
Title=Kaon helicity angle for $B\to J/\psi K^*$
XLabel=$\cos\theta_{K^*}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{K^*}$
LogY=0
END PLOT
