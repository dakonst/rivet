BEGIN PLOT /ARGUS_1996_I403304
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1996_I403304/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \omega\omega$
END PLOT
BEGIN PLOT /ARGUS_1996_I403304/d01-x01-y02
Title=Cross section for $\gamma\gamma\to \omega\pi^+\pi^-\pi^0$
END PLOT
BEGIN PLOT /ARGUS_1996_I403304/d01-x01-y03
Title=Cross section for $\gamma\gamma\to 2\pi^+2\pi^-2\pi^0$ (non-resonant)
END PLOT
