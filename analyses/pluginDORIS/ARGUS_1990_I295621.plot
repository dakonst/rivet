BEGIN PLOT /ARGUS_1990_I295621/d01-x01-y01
Title=Proton mupltiplicity in continuum
YLabel=$n_p$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1990_I295621/d02-x01-y01
Title=$pp$ multiplicity in continuum
YLabel=$n_{pp}$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1990_I295621/d03-x01-y01
Title=$\Lambda\Lambda$ multiplicity in continuum
YLabel=$n_{\Lambda\Lambda}$
LogY=0
END PLOT

BEGIN PLOT /ARGUS_1990_I295621/d04-x01-y01
Title=Proton mupltiplicity in $\Upsilon$ decays
YLabel=$n_p$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1990_I295621/d05-x01-y01
Title=$pp$ multiplicity in $\Upsilon$ decays
YLabel=$n_{pp}$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1990_I295621/d06-x01-y01
Title=$\Lambda\Lambda$ multiplicity in $\Upsilon$ decays
YLabel=$n_{\Lambda\Lambda}$
LogY=0
END PLOT

BEGIN PLOT /ARGUS_1990_I295621/d07-x01-y01
Title=Ratio of proton multiplicity in $\Upsilon$ decays vs continuum
YLabel=$r_p$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1990_I295621/d08-x01-y01
Title=Ratio of $pp$ multiplicity in $\Upsilon$ decays vs continuum
YLabel=$r_{pp}$
LogY=0
END PLOT

BEGIN PLOT /ARGUS_1990_I295621/d09-x01-y01
Title=Cosine of proton-proton angle in continuum
XLabel=$\cos\theta_{pp}$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_{pp}\times10^4$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1990_I295621/d09-x01-y02
Title=Cosine of proton-proton angle in $\Upsilon$ decays
XLabel=$\cos\theta_{pp}$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_{pp}\times10^3$
LogY=0
END PLOT
