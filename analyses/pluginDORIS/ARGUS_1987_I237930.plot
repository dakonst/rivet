BEGIN PLOT /ARGUS_1987_I237930/d01-x01-y01
Title=Helicity angle for $\tau\to\omega\pi^-\nu_\tau$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1987_I237930/d02-x01-y01
Title=Spectral function for $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau$
XLabel=$q$ [GeV]
YLabel=$v_1(q)$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1987_I237930/d02-x01-y02
Title=Spectral function for $\tau\to\omega\pi^-\nu_\tau$
XLabel=$q$ [GeV]
YLabel=$v_1(q)$
LogY=0
END PLOT
