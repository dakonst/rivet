Name: DASP_1978_I129715
Year: 1978
Summary: Measurement of $R$ for energies between 3.603 and 5.195 GeV
Experiment: DASP
Collider: DORIS
InspireID: 129715
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev. D76 (2007) 072008, 2007 
RunInfo: e+ e- to hadrons and e+ e- to mu+ mu- (for normalization)
NeedCrossSection: no
Beams: [e-, e+]
Energies:  [3.6025, 3.6125, 3.625, 3.65, 3.845, 3.885, 3.93, 3.95, 3.9775, 4.0, 4.01, 4.02, 4.03,
            4.04, 4.05, 4.06, 4.07, 4.08, 4.09, 4.1, 4.11, 4.12, 4.13, 4.14, 4.155, 4.17, 4.18,
            4.1925, 4.2125, 4.23, 4.24, 4.255, 4.2825, 4.305, 4.315, 4.325, 4.335, 4.345, 4.355,
            4.365, 4.375, 4.3875, 4.4, 4.41, 4.42, 4.43, 4.44, 4.45, 4.46, 4.4725, 4.49, 4.505,
            4.515, 4.525, 4.535, 4.545, 4.555, 4.565, 4.575, 4.59, 4.605, 4.62, 4.635, 4.645,
            4.66, 4.675, 4.69, 4.72, 4.76, 4.79, 5.0, 5.195]
Description:
  'Measurement of $R$ in $e^+e^-$ collisions by DASP for energies between 3.603 and 5.195 GeV.
   The individual hadronic and muonic cross sections are also outputted to the yoda file
   so that ratio $R$ can be recalculated if runs are combined.'
Keywords: []
BibKey: Brandelik:1978ei
BibTeX: '@article{Brandelik:1978ei,
      author         = "Brandelik, R. and others",
      title          = "{Total Cross-section for Hadron Production by $e^+ e^-$
                        Annihilation at Center-of-mass Energies Between 3.6-{GeV}
                        and 5.2-{GeV}}",
      collaboration  = "DASP",
      journal        = "Phys. Lett.",
      volume         = "76B",
      year           = "1978",
      pages          = "361",
      doi            = "10.1016/0370-2693(78)90807-9",
      reportNumber   = "DESY-78-18",
      SLACcitation   = "%%CITATION = PHLTA,76B,361;%%"
}'
