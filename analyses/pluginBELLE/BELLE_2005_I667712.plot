BEGIN PLOT /BELLE_2005_I667712
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to \pi^+\pi^-)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d0[3,4]
YLabel=$1/\sigma^*\frac{\text{d}\sigma(\gamma\gamma\to \pi^+\pi^-)}{\text{d}|\cos\theta|}$ [nb]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I667712/d01-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $|\cos\theta|<0.6$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d01-x01-y02
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $|\cos\theta|<0.6$
END PLOT

BEGIN PLOT /BELLE_2005_I667712/d03-x01-y01
Title=Normalized cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0<|\cos\theta|<0.1$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d03-x01-y02
Title=Normalized cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.1<|\cos\theta|<0.2$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d03-x01-y03
Title=Normalized cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.2<|\cos\theta|<0.3$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d03-x01-y04
Title=Normalized cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.3<|\cos\theta|<0.4$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d03-x01-y05
Title=Normalized cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.4<|\cos\theta|<0.5$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d03-x01-y06
Title=Normalized cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.5<|\cos\theta|<0.6$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d04-x01-y01
Title=Normalized cross section for $\gamma\gamma\to K^+K^-$ with $0<|\cos\theta|<0.1$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d04-x01-y02
Title=Normalized cross section for $\gamma\gamma\to K^+K^-$ with $0.1<|\cos\theta|<0.2$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d04-x01-y03
Title=Normalized cross section for $\gamma\gamma\to K^+K^-$ with $0.2<|\cos\theta|<0.3$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d04-x01-y04
Title=Normalized cross section for $\gamma\gamma\to K^+K^-$ with $0.3<|\cos\theta|<0.4$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d04-x01-y05
Title=Normalized cross section for $\gamma\gamma\to K^+K^-$ with $0.4<|\cos\theta|<0.5$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d04-x01-y06
Title=Normalized cross section for $\gamma\gamma\to K^+K^-$ with $0.5<|\cos\theta|<0.6$
END PLOT


BEGIN PLOT /BELLE_2005_I667712/d05-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0<|\cos\theta|<0.1$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d05-x01-y02
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.1<|\cos\theta|<0.2$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d05-x01-y03
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.2<|\cos\theta|<0.3$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d05-x01-y04
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.3<|\cos\theta|<0.4$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d05-x01-y05
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.4<|\cos\theta|<0.5$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d05-x01-y06
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.5<|\cos\theta|<0.6$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d06-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $0<|\cos\theta|<0.1$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d06-x01-y02
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $0.1<|\cos\theta|<0.2$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d06-x01-y03
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $0.2<|\cos\theta|<0.3$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d06-x01-y04
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $0.3<|\cos\theta|<0.4$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d06-x01-y05
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $0.4<|\cos\theta|<0.5$
END PLOT
BEGIN PLOT /BELLE_2005_I667712/d06-x01-y06
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $0.5<|\cos\theta|<0.6$
END PLOT
