BEGIN PLOT /BELLE_2008_I754089/d01-x01-y01
Title=Tranversality angle in $\eta_c\to f_2 f_2$
XLabel=$\chi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I754089/d01-x01-y02
Title=Tranversality angle in $\eta_c\to f_2 f^\prime_2$
XLabel=$\chi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$
LogY=0
END PLOT
