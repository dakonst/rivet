BEGIN PLOT /BELLE_2011_I897683/d01-x01-y01
Title=$K^+\phi$ mass  distribution in $B^+\to K^+\phi\gamma$
XLabel=$m_{K^+\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2011_I897683/d01-x01-y02
Title=$K^0_S\phi$ mass  distribution in $B^+\to K^0_S\phi\gamma$
XLabel=$m_{K^0_S\phi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\phi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
