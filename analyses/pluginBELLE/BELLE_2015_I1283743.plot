BEGIN PLOT /BELLE_2015_I1283743
LogY=0
END PLOT

BEGIN PLOT /BELLE_2015_I1283743/d01-x01-y02
Title=Cross section for $\Upsilon(1s)\pi^+\pi^-$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d02-x01-y02
Title=Cross section for $\Upsilon(2S)\pi^+\pi^-$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d03-x01-y02
Title=Cross section for $\Upsilon(3S)\pi^+\pi^-$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
END PLOT

BEGIN PLOT /BELLE_2015_I1283743/d10-x01-y01
Title=Max $\Upsilon(1S)\pi^\pm$ mass in $\Upsilon(1S)\pi^+\pi^-$ ($m^2_{\pi^+\pi^-}>0.2\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d10-x01-y02
Title=Max $\Upsilon(2S)\pi^\pm$ mass in $\Upsilon(2S)\pi^+\pi^-$ ($m^2_{\pi^+\pi^-}>0.14\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d10-x01-y03
Title=Max $\Upsilon(3S)\pi^\pm$ mass in $\Upsilon(3S)\pi^+\pi^-$ ($m^2_{\pi^+\pi^-}>0.1\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d10-x01-y04
Title=$\pi^+\pi^-$ mass in $\Upsilon(1S)\pi^+\pi^-$
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d10-x01-y05
Title=$\pi^+\pi^-$ mass in $\Upsilon(2S)\pi^+\pi^-$
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d10-x01-y06
Title=$\pi^+\pi^-$ mass in $\Upsilon(3S)\pi^+\pi^-$
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT

BEGIN PLOT /BELLE_2015_I1283743/d11-x01-y01
Title=Max $\Upsilon(1S)\pi^\pm$ mass in $\Upsilon(1S)\pi^+\pi^-$ ($0.2<m^2_{\pi^+\pi^-}<1\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d11-x01-y02
Title=Max $\Upsilon(1S)\pi^\pm$ mass in $\Upsilon(1S)\pi^+\pi^-$ ($1<m^2_{\pi^+\pi^-}<1.5\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d11-x01-y03
Title=Max $\Upsilon(1S)\pi^\pm$ mass in $\Upsilon(1S)\pi^+\pi^-$ ($m^2_{\pi^+\pi^-}>1.5\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(1S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d11-x01-y04
Title=$\pi^+\pi^-$ mass in $\Upsilon(1S)\pi^+\pi^-$ ($m^2_{\Upsilon(1S)\pi^\pm,\max}<105\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d11-x01-y05
Title=$\pi^+\pi^-$ mass in $\Upsilon(1S)\pi^+\pi^-$ ($105<m^2_{\Upsilon(1S)\pi^\pm,\max}<110\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d11-x01-y06
Title=$\pi^+\pi^-$ mass in $\Upsilon(1S)\pi^+\pi^-$ ($m^2_{\Upsilon(1S)\pi^\pm,\max}>110\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT

BEGIN PLOT /BELLE_2015_I1283743/d12-x01-y01
Title=Max $\Upsilon(2S)\pi^\pm$ mass in $\Upsilon(2S)\pi^+\pi^-$ ($0.14<m^2_{\pi^+\pi^-}<0.3\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(2S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(2S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d12-x01-y02
Title=Max $\Upsilon(2S)\pi^\pm$ mass in $\Upsilon(2S)\pi^+\pi^-$ ($0.3<m^2_{\pi^+\pi^-}<0.5\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(2S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(2S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d12-x01-y03
Title=Max $\Upsilon(2S)\pi^\pm$ mass in $\Upsilon(2S)\pi^+\pi^-$ ($0.5<m^2_{\pi^+\pi^-}<0.6\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(2S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(2S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d12-x01-y04
Title=Max $\Upsilon(2S)\pi^\pm$ mass in $\Upsilon(2S)\pi^+\pi^-$ ($m^2_{\pi^+\pi^-}>0.6\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(2S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(2S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d12-x01-y05
Title=$\pi^+\pi^-$ mass in $\Upsilon(2S)\pi^+\pi^-$ ($m^2_{\Upsilon(2S)\pi^\pm,\max}<110\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d12-x01-y06
Title=$\pi^+\pi^-$ mass in $\Upsilon(2S)\pi^+\pi^-$ ($110<m^2_{\Upsilon(2S)\pi^\pm,\max}<112\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d12-x01-y07
Title=$\pi^+\pi^-$ mass in $\Upsilon(2S)\pi^+\pi^-$ ($112<m^2_{\Upsilon(2S)\pi^\pm,\max}<114\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d12-x01-y08
Title=$\pi^+\pi^-$ mass in $\Upsilon(2S)\pi^+\pi^-$ ($m^2_{\Upsilon(2S)\pi^\pm,\max}>114\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT

BEGIN PLOT /BELLE_2015_I1283743/d13-x01-y01
Title=Max $\Upsilon(3S)\pi^\pm$ mass in $\Upsilon(3S)\pi^+\pi^-$ ($0.1<m^2_{\pi^+\pi^-}<0.15\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(3S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(3S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d13-x01-y02
Title=Max $\Upsilon(3S)\pi^\pm$ mass in $\Upsilon(3S)\pi^+\pi^-$ ($0.15<m^2_{\pi^+\pi^-}<0.2\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(3S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(3S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d13-x01-y03
Title=Max $\Upsilon(3S)\pi^\pm$ mass in $\Upsilon(3S)\pi^+\pi^-$ ($m^2_{\pi^+\pi^-}>0.2\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\Upsilon(3S)\pi^\pm,\max}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\Upsilon(3S)\pi^\pm,\max}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d13-x01-y04
Title=$\pi^+\pi^-$ mass in $\Upsilon(3S)\pi^+\pi^-$ ($m^2_{\Upsilon(3S)\pi^\pm,\max}<113\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d13-x01-y05
Title=$\pi^+\pi^-$ mass in $\Upsilon(3S)\pi^+\pi^-$ ($113<m^2_{\Upsilon(3S)\pi^\pm,\max}<114\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
BEGIN PLOT /BELLE_2015_I1283743/d13-x01-y06
Title=$\pi^+\pi^-$ mass in $\Upsilon(3S)\pi^+\pi^-$ ($m^2_{\Upsilon(3S)\pi^\pm,\max}>114\,\text{GeV}^2$)
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
END PLOT
