BEGIN PLOT /BELLE_2023_I2649712
LogY=0
XLabel=$q^2_{\min}$ [$\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /BELLE_2023_I2649712/d01-x01-y01
Title=$\langle q^2\rangle$ moment in $B\to X_c\ell\nu_\ell$
YLabel=$\langle q^2\rangle$ [$\mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /BELLE_2023_I2649712/d02-x01-y01
Title=$\langle q^4\rangle$ moment in $B\to X_c\ell\nu_\ell$
YLabel=$\langle q^4\rangle$ [$\mathrm{GeV}^4$]
END PLOT
BEGIN PLOT /BELLE_2023_I2649712/d03-x01-y01
Title=$\langle q^6\rangle$ moment in $B\to X_c\ell\nu_\ell$
YLabel=$\langle q^6\rangle$ [$\mathrm{GeV}^6$]
END PLOT
BEGIN PLOT /BELLE_2023_I2649712/d04-x01-y01
Title=$\langle q^8\rangle$ moment in $B\to X_c\ell\nu_\ell$
YLabel=$\langle q^8\rangle$ [$\mathrm{GeV}^8$]
END PLOT
BEGIN PLOT /BELLE_2023_I2649712/d05-x01-y01
Title=$\langle\left(q^2-\langle q^2\rangle\right)^2\rangle$ moment in $B\to X_c\ell\nu_\ell$
YLabel=$\langle\left(q^2-\langle q^2\rangle\right)^2\rangle$ [$\mathrm{GeV}^4$]
END PLOT
BEGIN PLOT /BELLE_2023_I2649712/d06-x01-y01
Title=$\langle\left(q^2-\langle q^2\rangle\right)^3\rangle$ moment in $B\to X_c\ell\nu_\ell$
YLabel=$\langle\left(q^2-\langle q^2\rangle\right)^3\rangle$ [$\mathrm{GeV}^6$]
END PLOT
BEGIN PLOT /BELLE_2023_I2649712/d07-x01-y01
Title=$\langle\left(q^2-\langle q^2\rangle\right)^4\rangle$ moment in $B\to X_c\ell\nu_\ell$
YLabel=$\langle\left(q^2-\langle q^2\rangle\right)^4\rangle$ [$\mathrm{GeV}^8$]
END PLOT
