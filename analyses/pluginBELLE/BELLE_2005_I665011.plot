BEGIN PLOT /BELLE_2005_I665011/d01-x01-y01
Title=Electron momentum spectrum in semileptonic $B^0$ decays
XLabel=$|p|$ [GeV]
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}|p|$ [$\mathrm{GeV}^{-1}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I665011/d01-x01-y02
Title=Electron momentum spectrum in semileptonic $B^+$ decays
XLabel=$|p|$ [GeV]
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}|p|$ [$\mathrm{GeV}^{-1}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I665011/d01-x01-y03
Title=Ratio Electron momentum spectrum in semileptonic $B^+$ to $B^0$ decays
XLabel=$|p|$ [GeV]
YLabel=Ratio
LogY=0
END PLOT
