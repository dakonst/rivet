BEGIN PLOT /BELLE_2022_I2173361/d01-x01-y01
Title=Cross section for $e^+e^-\to\Sigma^0\bar{\Sigma}^0$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\Sigma^0\bar{\Sigma}^0)$ [pb]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2173361/d02-x01-y01
Title=Cross section for $e^+e^-\to\Sigma^+\bar{\Sigma}^-$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\Sigma^+\bar{\Sigma}^-)$ [pb]
ConnectGaps=1
LogY=0
END PLOT
