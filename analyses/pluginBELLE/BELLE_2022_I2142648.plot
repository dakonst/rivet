BEGIN PLOT /BELLE_2022_I2142648/d01-x01-y01
Title=Cross section for $e^+e^-\to \omega\chi_{b0}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \omega \chi_{b0})$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BELLE_2022_I2142648/d01-x01-y02
Title=Cross section for $e^+e^-\to \omega\chi_{b1}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \omega \chi_{b1})$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BELLE_2022_I2142648/d01-x01-y03
Title=Cross section for $e^+e^-\to \omega\chi_{b2}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \omega \chi_{b2})$ [pb]
LogY=0
ConnectGaps=1
END PLOT
