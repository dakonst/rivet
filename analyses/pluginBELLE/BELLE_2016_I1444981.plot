BEGIN PLOT /BELLE_2016_I1444981/d01-x01-y01
Title=Cross section for $\gamma\gamma\to p\bar{p} K^+K^-$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2016_I1444981/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \Lambda(1520)^0\bar{p} K^-$+c.c.
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
