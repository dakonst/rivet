BEGIN PLOT /BELLE_2023_I2099998/d01-x01-y01
Title=Differential branching ratio for $B^+\to K^+K^-\pi^+$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{K^+K^-}\times10^7$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2023_I2099998/d02-x01-y01
Title=Differential branching ratio for $B^+\to K^+K^-\pi^+$
XLabel=$m_{K^-\pi^+}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{K^-\pi^+}\times10^7$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2023_I2099998/d03-x01-y01
Title=Helicity angle for $B^+\to K^+K^-\pi^+$ ($m_{K^+K^-}<1.1\,$GeV)
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
