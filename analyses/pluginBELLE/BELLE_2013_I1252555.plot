BEGIN PLOT /BELLE_2013_I1252555
YLabel=$\sigma$ [fb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
ConnectGaps=1
END PLOT

BEGIN PLOT /BELLE_2013_I1252555/d01-x01-y01
Title=Cross section for $e^+e^-\to\omega\pi^0$
END PLOT
BEGIN PLOT /BELLE_2013_I1252555/d02-x01-y01
Title=Cross section for $e^+e^-\to K^{*0}\bar{K}^0$+c.c.
END PLOT
BEGIN PLOT /BELLE_2013_I1252555/d03-x01-y01
Title=Cross section for $e^+e^-\to K^{*+}\bar{K}^-$+c.c.
END PLOT
BEGIN PLOT /BELLE_2013_I1252555/d04-x01-y01
Title=Cross section for $e^+e^-\to K_2^{*0}\bar{K}^0$+c.c.
END PLOT
BEGIN PLOT /BELLE_2013_I1252555/d05-x01-y01
Title=Cross section for $e^+e^-\to K_2^{*+}\bar{K}^-$+c.c.
END PLOT
