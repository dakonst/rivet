BEGIN PLOT /BELLE_2004_I623994/d01-x01-y01
Title=Helicity angle for $B^+\to\psi(3770)K^+$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
