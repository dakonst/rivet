BEGIN PLOT /BELLE_2008_I756554/d01-x01-y01
Title=$D^0\bar{D}^0$ mass in $B^+\to D^0\bar{D}^0K^+$
XLabel=$m_{D^0\bar{D}^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^0\bar{D}^0}$ $\text{GeV}^{-1}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I756554/d01-x01-y02
Title=$D^0\bar{D}^0$ mass in $B^+\to D^0\bar{D}^0K^+$ for $\cos\theta>0$
XLabel=$m_{D^0\bar{D}^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^0\bar{D}^0}$ $\text{GeV}^{-1}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I756554/d01-x01-y03
Title=$D^0K^+$ mass in $B^+\to D^0\bar{D}^0K^+$ for $\cos\theta>0$
XLabel=$m_{D^0K^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{D^0K^+}$ $\text{GeV}^{-1}$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2008_I756554/d02-x01-y01
Title=Helicity angle for $D_s(2700)$ in $B^+\to D^0\bar{D}^0K^+$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
