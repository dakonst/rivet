BEGIN PLOT /BELLE_2008_I754259/d0[1,2]
XLabel=$m_{p\bar{p}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar{p}}\times10^6$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I754259/d01-x01-y01
Title=$p\bar{p}$ mass for $B^+\to p\bar{p}K^+$
END PLOT
BEGIN PLOT /BELLE_2008_I754259/d02-x01-y01
Title=$p\bar{p}$ mass for $B^+\to p\bar{p}\pi^+$
END PLOT

BEGIN PLOT /BELLE_2008_I754259/d03
XLabel=$\cos\theta_p$
YLabel=$\text{d}\mathcal{B}/\text{d}\cos\theta_p\times10^6$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I754259/d03-x01-y01
Title=$\cos\theta_p$ for $B^+\to p\bar{p}K^+$ ($m_{p\bar{p}}<2.85\,$GeV)
END PLOT
BEGIN PLOT /BELLE_2008_I754259/d03-x01-y02
Title=$\cos\theta_p$ for $B^+\to p\bar{p}\pi^+$ ($m_{p\bar{p}}<2.85\,$GeV)
END PLOT

BEGIN PLOT /BELLE_2008_I754259/d04
XLabel=$\cos\theta_p$
YLabel=$1\Gamma\text{d}\Gamma/\text{d}\cos\theta_p$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I754259/d04-x01-y01
Title=$\cos\theta_p$ for $B^+\to p\bar{p}K^+$ ($m_{p\bar{p}}<2.0\,$GeV)
END PLOT
BEGIN PLOT /BELLE_2008_I754259/d04-x01-y02
Title=$\cos\theta_p$ for $B^+\to p\bar{p}K^+$ ($2.0<m_{p\bar{p}}<2.2\,$GeV)
END PLOT
BEGIN PLOT /BELLE_2008_I754259/d04-x01-y03
Title=$\cos\theta_p$ for $B^+\to p\bar{p}K^+$ ($2.2<m_{p\bar{p}}<2.4\,$GeV)
END PLOT
BEGIN PLOT /BELLE_2008_I754259/d04-x01-y04
Title=$\cos\theta_p$ for $B^+\to p\bar{p}K^+$ ($2.4<m_{p\bar{p}}<2.6\,$GeV)
END PLOT
BEGIN PLOT /BELLE_2008_I754259/d04-x01-y05
Title=$\cos\theta_p$ for $B^+\to p\bar{p}K^+$ ($2.6<m_{p\bar{p}}<2.85\,$GeV)
END PLOT
