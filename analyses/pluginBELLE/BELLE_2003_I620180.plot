BEGIN PLOT /BELLE_2003_I620180/d01-x01-y01
Title=Polarization in $B^+\to\rho^+\rho^0$
YLabel=$\Gamma_L/\Gamma$ [$\%$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2003_I620180/d02-x01-y01
Title=$\rho^0$ mass in $B^+\to\rho^+\rho^0$
XLabel=$m_{\rho^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\rho^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2003_I620180/d02-x01-y02
Title=$\rho^+$ mass in $B^+\to\rho^+\rho^0$
XLabel=$m_{\rho^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\rho^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2003_I620180/d03-x01-y01
Title=$\rho^0$ helicity angle $B^+\to\rho^+\rho^0$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
