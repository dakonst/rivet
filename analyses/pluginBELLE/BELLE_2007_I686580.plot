BEGIN PLOT /BELLE_2007_I686580/d01-x01-y01
Title=Cross section for $e^+e^-\to J/\psi X(3940)$ ($X(3940)\to>2$tracks)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [fb]
LogY=0
END PLOT
