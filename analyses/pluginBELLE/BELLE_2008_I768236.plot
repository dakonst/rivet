BEGIN PLOT /BELLE_2008_I768236/d01-x01-y01
Title=$D^-\pi^+$ mass in $B^+\to D^-\pi^+ \ell^+\nu_\ell$
XLabel=$m_{D^-\pi^+}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{D^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I768236/d01-x01-y02
Title=$D^{*-}\pi^+$ mass in $B^+\to D^{*-}\pi^+ \ell^+\nu_\ell$
XLabel=$m_{D^{*-}\pi^+}-m(\bar{D}^{*-})$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{D^{*-}\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I768236/d01-x01-y03
Title=$\bar{D}^0\pi^-$ mass in $B^0\to \bar{D}^0\pi^- \ell^+\nu_\ell$
XLabel=$m_{\bar{D}^0\pi^-}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\bar{D}^0\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2008_I768236/d01-x01-y04
Title=$D^{*-}\pi^+$ mass in $B^+\to D^{*-}\pi^+ \ell^+\nu_\ell$
XLabel=$m_{D^{*-}\pi^+}-m(\bar{D}^{*-})$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{D^{*-}\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2008_I768236/d02-x01
LogY=0
XLabel=$\cos\theta_v$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_v$
END PLOT
BEGIN PLOT /BELLE_2008_I768236/d02-x01-y01
Title=Hadron helicity in for $B\to D^*_0\ell\nu_\ell$
END PLOT
BEGIN PLOT /BELLE_2008_I768236/d02-x01-y03
Title=Hadron helicity in for $B\to D^*_2\ell\nu_\ell$
END PLOT

BEGIN PLOT /BELLE_2008_I768236/d03-x01
LogY=0
XLabel=$w$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}w$
END PLOT
BEGIN PLOT /BELLE_2008_I768236/d03-x01-y01
Title=$w$ in for $B\to D^*_0\ell\nu_\ell$
END PLOT
BEGIN PLOT /BELLE_2008_I768236/d03-x01-y03
Title=$w$ in for $B\to D^*_2\ell\nu_\ell$
END PLOT
