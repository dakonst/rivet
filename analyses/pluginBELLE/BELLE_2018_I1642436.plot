BEGIN PLOT /BELLE_2018_I1642436/d01-x01-y01
Title=$\Lambda_c^+K^-$ mass distribution in $B^-\to \Lambda_c^+\bar{\Lambda}^-_c K^-$
XLabel=$m_{\Lambda_c^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2018_I1642436/d02-x01-y01
Title=$\Lambda_c^+\bar \Lambda_c^-$ mass distribution in $B^-\to \Lambda_c^+\bar{\Lambda}^-_c K^-$
XLabel=$m_{\Lambda_c^+\bar \Lambda_c^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+\bar \Lambda_c^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
