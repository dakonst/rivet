BEGIN PLOT /BELLE_2022_I2131772/d01-x01-y01
Title=Minimum $\pi^+\pi^0$ mass in $B^+\to\pi^+\pi^0\pi^0$
XLabel=$m^{\min}_{\pi^+\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m^{\min}_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2131772/d01-x01-y02
Title=$\pi^0\pi^0$ mass in $B^+\to\pi^+\pi^0\pi^0$
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m^{\min}_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
