BEGIN PLOT /BELLE_2011_I889524/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $B_s^0\to\pi^+\pi^- J/\psi$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2011_I889524/d02-x01-y01
Title=Helicity angle for $0.8<m_{\pi^+\pi^-}<1.15$ GeV
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2011_I889524/d02-x01-y02
Title=Helicity angle for $1.3<m_{\pi^+\pi^-}<1.5$ GeV
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
