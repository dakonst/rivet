BEGIN PLOT /BELLE_2015_I1392799/d01-x01-y01
Title=$p\bar{\Lambda}^0$ mass in $B^0\to p\bar{\Lambda}^0D^-$
XLabel=$m_{p\bar{\Lambda}^0}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar{\Lambda}^0}\times10^{3}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2015_I1392799/d01-x01-y02
Title=$p\bar{\Lambda}^0$ mass in $B^0\to p\bar{\Lambda}^0D^{*-}$
XLabel=$m_{p\bar{\Lambda}^0}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}m_{p\bar{\Lambda}^0}\times10^{3}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2015_I1392799/d02-x01-y01
Title=$\cos{pD}$ in $B^0\to p\bar{\Lambda}^0D^^-$
XLabel=$\cos{pD}$ 
YLabel=$\text{d}\mathcal{B}/\text{d}\cos{pD}\times10^{6}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2015_I1392799/d02-x01-y02
LogY=0
Title=$\cos{pD^*}$ in $B^0\to p\bar{\Lambda}^0D^{*-}$
XLabel=$\cos{pD^*}$
YLabel=$\text{d}\mathcal{B}/\text{d}\cos{pD^*}\times10^{6}$
END PLOT
