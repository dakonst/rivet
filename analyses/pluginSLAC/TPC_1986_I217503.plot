BEGIN PLOT /TPC_1986_I217503/d01-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\eta$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
