# BEGIN PLOT /SLD_1999_I469925/d01-x01-y01
Title=Ratio $N_{\pi^+}/N_\mathrm{charged}$ 
XLabel=$x_p$
YLabel=$\mathrm{d}\left(N_{\pi^+}/N_\mathrm{charged}\right)/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d01-x01-y02
Title=$\pi^+$ scaled momentum
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d02-x01-y01
Title=Ratio $N_{K^+}/N_\mathrm{charged}$ 
XLabel=$x_p$
YLabel=$\mathrm{d}\left(N_{K^+}/N_\mathrm{charged}\right)/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d02-x01-y02
Title=$K^+$ scaled momentum
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d03-x01-y01
Title=Ratio $N_{p^+}/N_\mathrm{charged}$ 
XLabel=$x_p$
YLabel=$\mathrm{d}\left(N_{p^+}/N_\mathrm{charged}\right)/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d03-x01-y02
Title=$p^+$ scaled momentum
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d04-x01-y01
Title=Charged Particle scaled momentum
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d05-x01-y01
Title=$K^0$ scaled momentum
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d07-x01-y01
Title=$\Lambda^0$ scaled momentum
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d08-x01-y01
Title=$K^{*0}$ scaled momentum
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d09-x01-y01
Title=$\phi^0$ scaled momentum
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d10-x01-y01
Title=$\pi^+$ scaled momentum, (uds) events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d10-x01-y02
Title=$\pi^+$ scaled momentum, c events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d10-x01-y03
Title=$\pi^+$ scaled momentum, b events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d11-x01-y01
Title=$\pi^+$ scaled momentum, ratio c to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d11-x01-y02
Title=$\pi^+$ scaled momentum, ratio b to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d12-x01-y01
Title=$K^+$ scaled momentum, (uds) events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d12-x01-y02
Title=$K^+$ scaled momentum, c events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d12-x01-y03
Title=$K^+$ scaled momentum, b events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d13-x01-y01
Title=$K^+$ scaled momentum, ratio c to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d13-x01-y02
Title=$K^+$ scaled momentum, ratio b to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d14-x01-y01
Title=$K^{*0}$ scaled momentum, (uds) events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d14-x01-y02
Title=$K^{*0}$ scaled momentum, c events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d14-x01-y03
Title=$K^{*0}$ scaled momentum, b events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d15-x01-y01
Title=$K^{*0}$ scaled momentum, ratio c to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d15-x01-y02
Title=$K^{*0}$ scaled momentum, ratio b to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d16-x01-y01
Title=$p^+$ scaled momentum, (uds) events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d16-x01-y02
Title=$p^+$ scaled momentum, c events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d16-x01-y03
Title=$p^+$ scaled momentum, b events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d17-x01-y01
Title=$p^+$ scaled momentum, ratio c to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d17-x01-y02
Title=$p^+$ scaled momentum, ratio b to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d18-x01-y01
Title=$\Lambda^0$ scaled momentum, (uds) events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d18-x01-y02
Title=$\Lambda^0$ scaled momentum, c events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d18-x01-y03
Title=$\Lambda^0$ scaled momentum, b events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d19-x01-y01
Title=$\Lambda^0$ scaled momentum, ratio c to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d19-x01-y02
Title=$\Lambda^0$ scaled momentum, ratio b to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d20-x01-y01
Title=$K^0$ scaled momentum, (uds) events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d20-x01-y02
Title=$K^0$ scaled momentum, c events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d20-x01-y03
Title=$K^0$ scaled momentum, b events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d21-x01-y01
Title=$K^0$ scaled momentum, ratio c to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d21-x01-y02
Title=$K^0$ scaled momentum, ratio b to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d22-x01-y01
Title=$\phi^0$ scaled momentum, (uds) events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d22-x01-y02
Title=$\phi^0$ scaled momentum, c events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d22-x01-y03
Title=$\phi^0$ scaled momentum, b events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d23-x01-y01
Title=$\phi^0$ scaled momentum, ratio c to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d23-x01-y02
Title=$\phi^0$ scaled momentum, ratio b to uds events
XLabel=$x_p$
YLabel=$\mathrm{d}N/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d26-x01-y01
Title=$R^q_{\pi^+}=\frac{1}{2N_\mathrm{events}}\frac{\mathrm{d}}{\mathrm{d}x_p}\left[N(q\to\pi^+)+N(\bar q\to\pi^-)\right]$
XLabel=$x_p$
YLabel=$R^q_{\pi^+}$
FullRange=1
LogY=1
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d26-x01-y02
Title=$R^q_{\pi^-}=\frac{1}{2N_\mathrm{events}}\frac{\mathrm{d}}{\mathrm{d}x_p}\left[N(q\to\pi^-)+N(\bar q\to\pi^+)\right]$
XLabel=$x_p$
YLabel=$R^q_{\pi^-}$
FullRange=1
LogY=1
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d27-x01-y01
Title=$D^q_{\pi^-}=(R^q_{\pi^-}-R^q_{\pi^+})/(R^q_{\pi^-}+R^q_{\pi^+})$
XLabel=$x_p$
YLabel=$D^q_{\pi^-}$ 
FullRange=1
LogY=0
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d28-x01-y01
Title=$R^q_{K^{*0}}=\frac{1}{2N_\mathrm{events}}\frac{\mathrm{d}}{\mathrm{d}x_p}\left[N(q\to K^{*0})+N(\bar q\to \bar K^{*0})\right]$
XLabel=$x_p$
YLabel=$R^q_{K^{*0}}$
FullRange=1
LogY=1
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d28-x01-y02
Title=$R^q_{\bar K^{*0}}=\frac{1}{2N_\mathrm{events}}\frac{\mathrm{d}}{\mathrm{d}x_p}\left[N(q\to \bar K^{*0})+N(\bar q\to K^{*0})\right]$
XLabel=$x_p$
YLabel=$R^q_{\bar K^{*0}}$
FullRange=1
LogY=1
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d29-x01-y01
Title=$D^q_{\bar K^{*0}}=(R^q_{\bar K^{*0}}-R^q_{K^{*0}})/(R^q_{\bar K^{*0}}+R^q_{K^{*0}})$
XLabel=$x_p$
YLabel=$D^q_{\bar K^{*0}}$ 
FullRange=1
LogY=0
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d30-x01-y01
Title=$R^q_{K^+}=\frac{1}{2N_\mathrm{events}}\frac{\mathrm{d}}{\mathrm{d}x_p}\left[N(q\to K^+)+N(\bar q\to K^-)\right]$
XLabel=$x_p$
YLabel=$R^q_{K^+}$
FullRange=1
LogY=1
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d30-x01-y02
Title=$R^q_{K^-}=\frac{1}{2N_\mathrm{events}}\frac{\mathrm{d}}{\mathrm{d}x_p}\left[N(q\to K^-)+N(\bar q\to K^+)\right]$
XLabel=$x_p$
YLabel=$R^q_{K^-}$
FullRange=1
LogY=1
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d31-x01-y01
Title=$D^q_{K^-}=(R^q_{K^-}-R^q_{K^+})/(R^q_{K^-}+R^q_{K^+})$
XLabel=$x_p$
YLabel=$D^q_{K^-}$ 
FullRange=1
LogY=0
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d32-x01-y01
Title=$R^q_{p}=\frac{1}{2N_\mathrm{events}}\frac{\mathrm{d}}{\mathrm{d}x_p}\left[N(q\to p)+N(\bar q\to \bar p)\right]$
XLabel=$x_p$
YLabel=$R^q_{p}$
FullRange=1
LogY=1
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d32-x01-y02
Title=$R^q_{\bar p}=\frac{1}{2N_\mathrm{events}}\frac{\mathrm{d}}{\mathrm{d}x_p}\left[N(q\to \bar p)+N(\bar q\to p)\right]$
XLabel=$x_p$
YLabel=$R^q_{\bar p}$
FullRange=1
LogY=1
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d33-x01-y01
Title=$D^q_{p}=(R^q_{p}-R^q_{\bar p})/(R^q_{p}+R^q_{\bar p})$
XLabel=$x_p$
YLabel=$D^q_{p}$ 
FullRange=1
LogY=0
# END PLOT

# BEGIN PLOT /SLD_1999_I469925/d34-x01-y01
Title=$R^q_{\Lambda^0}=\frac{1}{2N_\mathrm{events}}\frac{\mathrm{d}}{\mathrm{d}x_p}\left[N(q\to \Lambda^0)+N(\bar q\to \bar \Lambda^0)\right]$
XLabel=$x_p$
YLabel=$R^q_{\Lambda^0}$
FullRange=1
LogY=1
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d34-x01-y02
Title=$R^q_{\bar \Lambda^0}=\frac{1}{2N_\mathrm{events}}\frac{\mathrm{d}}{\mathrm{d}x_p}\left[N(q\to \bar \Lambda^0)+N(\bar q\to \Lambda^0)\right]$
XLabel=$x_p$
YLabel=$R^q_{\bar \Lambda^0}$
FullRange=1
LogY=1
# END PLOT
# BEGIN PLOT /SLD_1999_I469925/d35-x01-y01
Title=$D^q_{\Lambda^0}=(R^q_{\Lambda^0}-R^q_{\bar \Lambda^0})/(R^q_{\Lambda^0}+R^q_{\bar \Lambda^0})$
XLabel=$x_p$
YLabel=$D^q_{\Lambda^0}$ 
FullRange=1
LogY=0
# END PLOT

 
BEGIN PLOT /SLD_1999_I469925/d24-x01
YLabel=$\langle N\rangle$
XCustomMajorTicks=1 $\pi^\pm$ 2 $K^\pm$ 3 $K^0,\bar{K}^0$ 4 $K^{*0},\bar{K}^{*0}$ 5 $\phi$ 6 $p,\bar{p}$ 7 $\Lambda^0,\bar\Lambda^0$
LogY=0
END PLOT
BEGIN PLOT /SLD_1999_I469925/d25-x01
XCustomMajorTicks=1 $\pi^\pm$ 2 $K^\pm$ 3 $K^0,\bar{K}^0$ 4 $K^{*0},\bar{K}^{*0}$ 5 $\phi$ 6 $p,\bar{p}$ 7 $\Lambda^0,\bar\Lambda^0$
LogY=0
END PLOT

BEGIN PLOT /SLD_1999_I469925/d24-x01-y01
Title=Hadron Multiplicities (all events)
END PLOT
BEGIN PLOT /SLD_1999_I469925/d24-x01-y02
Title=Hadron Multiplicities ($uds$ events)
END PLOT
BEGIN PLOT /SLD_1999_I469925/d24-x01-y03
Title=Hadron Multiplicities ($c$ events)
END PLOT
BEGIN PLOT /SLD_1999_I469925/d24-x01-y04
Title=Hadron Multiplicities ($b$ events)
END PLOT

BEGIN PLOT /SLD_1999_I469925/d25-x01-y01
Title=Multplicity difference charm-light
YLabel=$\langle N_{\mathrm{charm}}-N_{\mathrm{light}}\rangle$
END PLOT
BEGIN PLOT /SLD_1999_I469925/d25-x01-y02
Title=Multplicity difference bottom-light
YLabel=$\langle N_{\mathrm{charm}}-N_{\mathrm{light}}\rangle$
END PLOT
