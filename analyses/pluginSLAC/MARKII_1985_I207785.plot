BEGIN PLOT /MARKII_1985_I207785/d02-x01-y01
Title=$K^0$ spectrum at 29 GeV
XLabel=$p$
YLabel=$\frac1{\sigma_{\mathrm{hadrons}}\beta} \mathrm{d}\sigma/\mathrm{d}x_p$
END PLOT
BEGIN PLOT /MARKII_1985_I207785/d04-x01-y01
Title=$K^\pm$ spectrum at 29 GeV
XLabel=$p$
YLabel=$\frac1{\sigma_{\mathrm{hadrons}}\beta} \mathrm{d}\sigma/\mathrm{d}x_p$
END PLOT
