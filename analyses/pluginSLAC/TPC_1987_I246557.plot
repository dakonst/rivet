BEGIN PLOT /TPC_1987_I246557/d01-x01-y01
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to p\bar{p})$ [nb]
LogY=1
END PLOT

BEGIN PLOT /TPC_1987_I246557/d03-x01-y01
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.1<\sqrt{s}<2.4$ GeV
END PLOT
BEGIN PLOT /TPC_1987_I246557/d03-x01-y02
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.4<\sqrt{s}<2.8$ GeV
END PLOT
