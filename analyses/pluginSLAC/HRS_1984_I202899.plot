BEGIN PLOT /HRS_1984_I202899/d01-x01-y01
Title=$D^0$ cross section at 29 GeV ($z>0.5$)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
BEGIN PLOT /HRS_1984_I202899/d01-x01-y02
Title=$D^+$ cross section at 29 GeV ($z>0.5$)
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
BEGIN PLOT /HRS_1984_I202899/d04-x01-y01
Title=$D^0$ spectrum at 29 GeV
XLabel=$z$
YLabel=$\frac1N\text{d}N/\mathrm{d}z$ 
LogY=0
END PLOT
BEGIN PLOT /HRS_1984_I202899/d03-x01-y01
Title=$D^0$ spectrum at 29 GeV
XLabel=$z$
YLabel=$\frac{s}\beta\mathrm{d}\sigma/\mathrm{d}z$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /HRS_1984_I202899/d03-x01-y02
Title=$D^+$ spectrum at 29 GeV
XLabel=$z$
YLabel=$\frac{s}\beta\mathrm{d}\sigma/\mathrm{d}z$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
LogY=0
END PLOT
